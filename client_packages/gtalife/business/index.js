const NativeUI = require("nativeui");
const Menu = NativeUI.Menu;
const UIMenuItem = NativeUI.UIMenuItem;
const Point = NativeUI.Point;
const Offset = require("gtalife/util/offsets.js")

let business_text_mode = 0;

let business_purchase_menu = null;
let business_menu = null;
let business_safe_menu = null;

let business_menus = [];

function b_numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function b_hideMenus() {
    for (let i = 0; i < business_menus.length; i++) business_menus[i].Visible = false;
}

mp.events.add('ShowBusinessText', (args) => {
    business_text_mode = args;
    if (business_text_mode == 0) b_hideMenus();
});

mp.events.add('Business_PurchaseMenu', (args) => {
    let data = JSON.parse(args);

    if (business_purchase_menu == null) {
        business_purchase_menu = new Menu("Negocio", "¡Este negocio esta en venta!", new Point(mp.game.resolution.width - Offset.getWidthOffset(mp.game.resolution.width), 50));
        business_menus.push(business_purchase_menu);

        business_purchase_menu.ItemSelect.on((item, index) => {
            if (index == 2) mp.events.callRemote("BusinessPurchase");
        });
    }

    business_purchase_menu.Clear();

    let temp_item = new UIMenuItem("Tipo", "El tipo de lo negocio.");
    temp_item.SetRightLabel(data.Type);
    business_purchase_menu.AddItem(temp_item);

    temp_item = new UIMenuItem("Precio", "Precio del negocio.");
    temp_item.SetRightLabel("$" + b_numberWithCommas(data.Price));
    business_purchase_menu.AddItem(temp_item);

    temp_item = new UIMenuItem("Comprar", "Seleccione esta opción para comprar el negocio.");
    business_purchase_menu.AddItem(temp_item);

    business_purchase_menu.Visible = true;
});

mp.events.add('BusinessMenu', (args) => {
    let data = JSON.parse(args);

    if (business_menu == null) {
        business_menu = new Menu("Negocio", "Controles del negocio", new Point(mp.game.resolution.width - Offset.getWidthOffset(mp.game.resolution.width), 50));
        business_menus.push(business_menu);

        business_menu.CheckboxChange.on((item, checked) => {
            mp.events.callRemote("BusinessSetLock", checked);
        });

        business_menu.ItemSelect.on((item, index) => {
            switch (index) {
                case 0:
                    // let name = API.getUserInput("", 31);
                    // if (name.length > 0) API.triggerServerEvent("BusinessSetName", name);
                    mp.gui.chat.push('WARNING: API.getUserInput not implemented.');
                    break;

                case 2:
                    mp.events.callRemote("BusinessSell");
                    break;
            }
        });
    }

    if (business_safe_menu == null) {
        business_safe_menu = new Menu("Casa", "Caja fuerte", new Point(mp.game.resolution.width - Offset.getWidthOffset(mp.game.resolution.width), 50));
        business_menus.push(business_safe_menu);

        business_safe_menu.ParentMenu = business_menu;

        let temp_safe_item = new UIMenuItem("Depositar dinero", "Depositar dinero en la caja fuerte.");
        business_safe_menu.AddItem(temp_safe_item);

        temp_safe_item = new UIMenuItem("Sacar dinero", "Sacar dinero de la caja fuerte.");
        business_safe_menu.AddItem(temp_safe_item);

        business_safe_menu.ItemSelect.on((item, index) => {
            // let amount = API.getUserInput("", 9);
            // if (parseInt(amount) > 1) API.triggerServerEvent("BusinessSafe", index, amount);
            mp.gui.chat.push('WARNING: API.getUserInput not implemented.');
        });
    }

    // business main menu
    business_menu.Clear();

    let temp_item = new UIMenuItem("Nombre del negocio", "Cambia el nombre de el negocio.");
    business_menu.AddItem(temp_item);

    /*temp_item = API.createCheckboxItem("Lock", "Permite/bloquee lo uso de otros jugadores a su negocio.", data.Locked);
    business_menu.AddItem(temp_item);*/

    temp_item = new UIMenuItem("Caja fuerte", "~g~($" + b_numberWithCommas(data.Money) + ")");
    business_menu.AddItem(temp_item);
    business_menu.BindMenuToItem(business_safe_menu, temp_item);

    temp_item = new UIMenuItem("Vender negocio", "Vende tu negocio.");
    business_menu.AddItem(temp_item);

    business_menu.Visible = true;
});

mp.events.add('BusinessUpdateSafe', (args) => {
    if (business_safe_menu != null) {
        let data = JSON.parse(args);
        // API.setMenuSubtitle(business_safe_menu, "~b~Caja fuerte ~g~($" + b_numberWithCommas(data.Money) + ")");
        mp.gui.chat.push('WARNING: API.setMenuSubtitle not implemented.');
    }
});

mp.events.add('UpdateBusinessBlip', (args) => {
    // API.setBlipColor(args[0], 69);
    // API.setBlipShortRange(args[0], false);
    mp.gui.chat.push('WARNING: API.setBlip not implemented.');
});

mp.events.add('ResetBusinessBlip', (args) => {
    // API.setBlipColor(args[0], 0);
    // API.setBlipShortRange(args[0], true);
    mp.gui.chat.push('WARNING: API.setBlip not implemented.');
});

// E key
mp.keys.bind(0x45, false, () => {
    if (mp.gui.cursor.visible) return;
    
    if (business_text_mode == 1) mp.events.callRemote("BusinessInteract");
    else if (business_text_mode == 2) mp.events.callRemote("BusinessLeave");
});

// M key
mp.keys.bind(0x4D, false, () => {
    b_hideMenus();
    mp.events.callRemote("BusinessMenu");
});
