const NativeUI = require("nativeui");
const Menu = NativeUI.Menu;
const UIMenuItem = NativeUI.UIMenuItem;
const Point = NativeUI.Point;
const Offset = require("gtalife/util/offsets.js")

let business_menu = null;

let products = [
    { name: "Agua", price: "5", description: "Un líquido incoloro, transparente, inodoro e insípido que forma los mares, lagos, ríos y la lluvia y es la base de los fluidos de los organismos vivos." },
    { name: "Cerveza", price: "25", description: "Bebida producida a partir de la fermentación de cereales, principalmente la cebada maltada. Se cree que fue una de las primeras bebidas alcohólicas que fueron creadas por el ser humano." },
    { name: "Whisky", price: "50", description: "El whisky es una bebida alcohólica destilada de granos, a menudo incluyendo malta, que ha sido envejecida en barricas." },
    { name: "Vodka", price: "100", description: "Una popular bebida destilada, incolora, casi sin sabor y con un grado alcohólico." },
];

mp.events.add('BusinessBar', () => {
    if (business_menu == null) {
        business_menu = new Menu("Bebidas", "Seleccione una bebida", new Point(mp.game.resolution.width - Offset.getWidthOffset(mp.game.resolution.width), 50));

        business_menu.ItemSelect.on((item, index) => {
            mp.events.callRemote("BusinessBuyDrink", JSON.stringify(products[index]));
        });
    }

    // business main menu
    business_menu.Clear();

    for (let i = 0, len = products.length; i < len; i++) {
        let temp_item = new UIMenuItem(`${products[i].name}`, `${products[i].description}`);
        temp_item.SetRightLabel(`$${products[i].price}`);
        business_menu.AddItem(temp_item);
    }

    business_menu.Visible = true;
});
