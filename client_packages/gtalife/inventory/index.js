const NativeUI = require("nativeui");
const Menu = NativeUI.Menu;
const UIMenuItem = NativeUI.UIMenuItem;
const UIMenuListItem = NativeUI.UIMenuListItem;
const ItemsCollection = NativeUI.ItemsCollection;
const Point = NativeUI.Point;
const Offset = require("gtalife/util/offsets.js")

let inventoryMenu = null;
let itemActionsMenu = null;
let inventoryData = [];
let selectedIdx = -1;

let dropListItem = null;
let dropAmount = 0;

let tradeMenu = null;
let tradeAmount = 0;

function createInventoryMenu() {
    // main inventory menu
    inventoryMenu = new Menu("Inventario", "Selecciona un artículo.", new Point(mp.game.resolution.width - Offset.getWidthOffset(mp.game.resolution.width), 50));
    inventoryMenu.ItemSelect.on((item, index) => {
        selectedIdx = index;

        // update actions menu
        itemActionsMenu.Text = inventoryData[index].Name;

        itemActionsMenu.Clear();
        itemActionsMenu.AddItem(new UIMenuItem("Usar", ""));

        if (inventoryData[index].IsDroppable) {
            if (inventoryData[index].Quantity > 1) {
                let amountList = [];
                for (let i = 1; i <= inventoryData[index].Quantity; i++) amountList.push(i.toString());

                dropListItem = new UIMenuListItem("Tirar", "", new ItemsCollection(amountList));
                itemActionsMenu.AddItem(dropListItem);
                dropListItem.OnListChanged.on((item, new_index) => {
                    dropAmount = new_index + 1;
                });

                let tradeListItem = new UIMenuListItem("Vender", "", new ItemsCollection(amountList));
                itemActionsMenu.AddItem(tradeListItem);
                tradeListItem.OnListChanged.on((item, new_index) => {
                    tradeAmount = new_index + 1;
                });
            } else {
                itemActionsMenu.AddItem(new UIMenuItem("Tirar", ""));
                itemActionsMenu.AddItem(new UIMenuItem("Vender", ""));
            }
        } else {
            itemActionsMenu.AddItem(new UIMenuItem("Tirar", "No puedes tirar este item."));
            itemActionsMenu.MenuItems[1].Enabled = false;

            itemActionsMenu.AddItem(new UIMenuItem("Vender", "No puedes vender este item."));
            itemActionsMenu.MenuItems[2].Enabled = false;
        }

        dropAmount = 0;
        tradeAmount = 0;
        inventoryMenu.Visible = false;
        setTimeout(() => {
            // hack
            itemActionsMenu.Visible = true;
        }, 25);
    });

    // actions menu
    itemActionsMenu = new Menu("Inventario", "Selecciona una acción.", new Point(mp.game.resolution.width - Offset.getWidthOffset(mp.game.resolution.width), 50));
    itemActionsMenu.ParentMenu = inventoryMenu;

    itemActionsMenu.ItemSelect.on((item, index) => {
        switch (index) {
            case 0:
                mp.events.callRemote("ConsumeItem", selectedIdx);
                break;

            case 1:
                if (!inventoryData[selectedIdx].IsDroppable) return;
                let amount = (inventoryData[selectedIdx].Quantity > 1) ? dropListItem.SelectedValue : 1;
                if (amount < 1) amount = 1;

                // let pos = API.getEntityFrontPosition(API.getLocalPlayer());
                let ground = mp.game.gameplay.getGroundZFor3dCoord(mp.players.local.position.x, mp.players.local.position.y, mp.players.local.position.z, 0.0, false);
                mp.events.callRemote("DropItem", selectedIdx, amount, mp.players.local.position.x, mp.players.local.position.y, ground);
                break;
            case 2:
                if (!inventoryData[selectedIdx].IsDroppable) return;
                let _amount = (inventoryData[selectedIdx].Quantity > 1) ? tradeAmount : 1;
                if (_amount < 1) _amount = 1;

                mp.events.callRemote("RequestNearbyPlayersInventory", selectedIdx, _amount);
                break;
        }
    });
}

mp.events.add('ReceiveInventory', (args) => {
    if (inventoryMenu == null) createInventoryMenu();
    inventoryMenu.Clear();

    // load data
    inventoryData = JSON.parse(args);
    for (let i = 0; i < inventoryData.length; i++) {
        let temp_item = new UIMenuItem(inventoryData[i].Name, inventoryData[i].Description);
        if (inventoryData[i].StackSize > 1) temp_item.SetRightLabel(inventoryData[i].Quantity + "/" + inventoryData[i].StackSize);

        inventoryMenu.AddItem(temp_item);
    }

    selectedIdx = -1;
    itemActionsMenu.Visible = false;
    inventoryMenu.Visible = (inventoryData.length > 0);

    if (inventoryData.length < 1) {
        mp.game.graphics.notify("~r~Tu inventario está vacío.");
    }
});

mp.events.add('CloseInventoryMenus', (args) => {
    inventoryMenu.Visible = false;
    itemActionsMenu.Visible = false;
});

mp.events.add('ReceiveNearbyPlayersInventory', (args0, args1, args2) => {
    if (tradeMenu == null) {
        tradeMenu = new Menu("Inventario", "Selecciona un jugador.", 0, 0, 6);
    }
    tradeMenu.Clear();

    let players = JSON.parse(args2);
    for (let i = 0; i < players.length; i++) {
        let playerItem = new UIMenuItem(players[i], "");
        tradeMenu.AddItem(playerItem);
        playerItem.Activated.connect(function (menu, item) {
            mp.gui.chat.push('API.getUserInput not implemented yet!');
            // let price = API.getUserInput("", 9);
            // if (parseInt(price) > 1) mp.events.callRemote("TradeItem", args[0], args[1], players[i], price);
            // else API.sendNotification("~r~Precio inválido");

            tradeMenu.Visible = false;
            inventoryMenu.Visible = false;
            itemActionsMenu.Visible = false;
        });
    }

    inventoryMenu.Visible = false;
    itemActionsMenu.Visible = false;
    tradeMenu.Visible = true;
});

// I key
mp.keys.bind(0x49, false, () => {
    if (mp.gui.cursor.visible) return;
    mp.events.callRemote("RequestInventory");
});
