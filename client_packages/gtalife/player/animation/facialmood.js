const NativeUI = require("nativeui");
const Menu = NativeUI.Menu;
const UIMenuItem = NativeUI.UIMenuItem;
const BadgeStyle = NativeUI.BadgeStyle;
const Point = NativeUI.Point;
const Offset = require("gtalife/util/offsets.js")

var mood_menu = null;

mp.events.add('ReceiveMoods', (args) => {
    if (mood_menu == null) {
        var data = JSON.parse(args);

        mood_menu = new Menu("Humor", "Seleccione un humor.", new Point(mp.game.resolution.width - Offset.getWidthOffset(mp.game.resolution.width), 50));
        for (var i = 0; i < data.length; i++) mood_menu.AddItem(new UIMenuItem(data[i], ""));
        mood_menu.MenuItems[0].SetLeftBadge(BadgeStyle.Tick);

        mood_menu.ItemSelect.on(function (item, index) {
            mp.events.callRemote("SetMood", index);
        });
    }
    mood_menu.Visible = true;
});

mp.events.add('SetCurrentMoodIndex', (args) => {
    if (mood_menu != null) {
        for (var i = 0; i < mood_menu.MenuItems.length; i++) mood_menu.MenuItems[i].SetLeftBadge(BadgeStyle.None);
        mood_menu.MenuItems[args].SetLeftBadge(BadgeStyle.Tick);
    }
});

mp.events.add('SetPlayerMood', (player, animName) => {
    player.setFacialIdleAnimOverride(animName, "");
});

mp.events.add('ResetPlayerMood', (player) => {
    player.clearFacialIdleAnimOverride();
});

// f4 key
mp.keys.bind(0x73, false, () => {
    if (anim_menu == null) mp.events.callRemote("RequestMoods");
    else mood_menu.Visible = !mood_menu.Visible;
});

mp.events.add('entityStreamIn', (entity) => {
    const animName = entity.getVariable('PlayerMood');
    if (animName) {
        entity.setFacialIdleAnimOverride(animName, "");
    }
});
