var mainBrowser = null;

mainBrowser = mp.browsers.new('package://gtalife/assets/views/playerlist.html');
mainBrowser.active = false;

setInterval(() => {
    if (mp.keys.isDown(0x5A)) {
        if (!mp.gui.cursor.visible && !mainBrowser.active) {
            mp.events.callRemote("get_players_list");
        }
    }
    else if (mp.keys.isUp(0x5A)) {
        if (!mp.gui.cursor.visible && mainBrowser.active) {
            mainBrowser.active = false;
        }
    }
}, 500);

mp.events.add('show_players_list', (args) => {
    mainBrowser.active = true;
    mainBrowser.execute(`update_players_list(${args});`);
});
