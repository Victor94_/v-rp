﻿using GTANetworkAPI;

namespace gtalife.src.Faction.Medic.Extensions
{
    public static class ClientExtensions
    {
        public static bool isInjured(this Client player)
        {
            return Death.PlayersInjured.ContainsKey(player);
        }
    }
}
