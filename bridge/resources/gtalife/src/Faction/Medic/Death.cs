﻿using GTANetworkAPI;
using gtalife.src.Flags;
using gtalife.src.Player.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace gtalife.src.Faction.Medic
{
    public class Death : Script
    {
        public const int ACCEPT_DEATH_MINUTES = 2;

        public static Dictionary<Client, DateTime> PlayersInjured = new Dictionary<Client, DateTime>();

        [ServerEvent(Event.PlayerDeath)]
        public void OnPlayerDeath(Client player, Client killer, uint reason)
        {
            OverrideDeath(player);

            if (player.getJailtime() > 0)
            {
                player.SendChatMessage("~y~INFO: ~w~No puedes morir en la cárcel.");
                return;
            }

            player.Invincible = true;
            player.FreezePosition = true;

            if (!PlayersInjured.ContainsKey(player))
            {
                PlayersInjured.Add(player, DateTime.Now.AddMinutes(ACCEPT_DEATH_MINUTES));
            }
            else
            {
                PlayersInjured[player] = DateTime.Now.AddMinutes(ACCEPT_DEATH_MINUTES);
            }

            player.TriggerEvent("DisableAllControls", true);

            player.PlayAnimation("dead", "dead_d", (int)(AnimationFlags.Loop));
            NAPI.Task.Run(() =>
            {
                player.PlayAnimation("dead", "dead_d", (int)(AnimationFlags.Loop));
            }, delayTime: 1000);

            player.SendChatMessage("~r~INFO: ~w~Sufrió un daño fatal y está herido, espere a un médico o lo use /morir para aceptar la muerte.");

            // Medic blips
            SendInjuredPlayersListToMedics();
        }

        [Command("morir")]
        public void CMD_Die(Client player)
        {
            if (PlayersInjured.ContainsKey(player))
            {
                if (PlayersInjured[player] > DateTime.Now) player.SendChatMessage($"~r~ERROR: ~w~Debes esperar {Math.Round(PlayersInjured[player].Subtract(DateTime.Now).TotalSeconds, 0)} segundos para aceptar la muerte.");
                else
                {                    
                    player.Invincible = false;
                    player.FreezePosition = false;
                    PlayersInjured.Remove(player);

                    player.TriggerEvent("DisableAllControls", false);
                    player.TriggerEvent("StopDeathEffect", false);
                    player.StopAnimation();
                    player.RemoveAllWeapons();

                    player.Position = new Vector3(-247.5816, 6330.996, 32.42617);
                    player.Rotation = new Vector3(0, 0, -45.39548);

                    int money = 200;
                    player.giveMoney(-money);
                    player.SendChatMessage($"~y~INFO: ~w~Pagaste ${money} al hospital.");

                    // Medic blips
                    SendInjuredPlayersListToMedics();
                }
            }
            else
            {
                player.Kill();
                player.sendChatAction("se suicida.");
            }
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (PlayersInjured.ContainsKey(player)) PlayersInjured.Remove(player);

            // Medic blips
            SendInjuredPlayersListToMedics();
        }

        private void SendInjuredPlayersListToMedics()
        {
            List<Client> players = new List<Client>();
            foreach (var injuredPlayer in PlayersInjured) players.Add(injuredPlayer.Key);

            foreach (var medic in NAPI.Pools.GetAllPlayers())
            {
                Faction faction = Main.Factions.FirstOrDefault(f => f.ID == medic.GetFactionID());
                if (faction == null || faction.Type != (int)FactionType.Medic) continue;

                medic.TriggerEvent("ReceiveInjuredPlayers", NAPI.Util.ToJson(players));
            }
        }

        private void OverrideDeath(Client player)
        {
            NAPI.Native.SendNativeToPlayer(player, Hash._RESET_LOCALPLAYER_STATE, player);
            NAPI.Native.SendNativeToPlayer(player, Hash.RESET_PLAYER_ARREST_STATE, player);
            NAPI.Native.SendNativeToPlayer(player, Hash.IGNORE_NEXT_RESTART, true);
            NAPI.Native.SendNativeToPlayer(player, Hash._DISABLE_AUTOMATIC_RESPAWN, true);
            NAPI.Native.SendNativeToPlayer(player, Hash.SET_FADE_IN_AFTER_DEATH_ARREST, true);
            NAPI.Native.SendNativeToPlayer(player, Hash.SET_FADE_OUT_AFTER_DEATH, false);
            NAPI.Native.SendNativeToPlayer(player, Hash.NETWORK_REQUEST_CONTROL_OF_ENTITY, player);
            NAPI.Native.SendNativeToPlayer(player, Hash.FREEZE_ENTITY_POSITION, player, false);
            NAPI.Native.SendNativeToPlayer(player, Hash.NETWORK_RESURRECT_LOCAL_PLAYER, player.Position.X, player.Position.Y, player.Position.Z, player.Rotation.Z, false, false);
            NAPI.Native.SendNativeToPlayer(player, Hash.RESURRECT_PED, player);
            NAPI.Native.SendNativeToPlayer(player, Hash.SET_PED_TO_RAGDOLL, player, true);
            NAPI.Native.SendNativeToPlayer(player, Hash.SET_CLOCK_TIME, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        }
    }
}
