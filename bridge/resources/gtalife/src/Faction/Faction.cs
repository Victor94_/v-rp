﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace gtalife.src.Faction
{
    #region FactionType
    public enum FactionType
    {
        Police = 1,
        Medic,
        Army
    }
    #endregion

    #region Faction Class
    public class Faction
    {
        public Guid ID { get; }
        public string Name { get; set; }
        public int Type { get; set; }
        public List<string> Ranks { get; set; }
        public List<FactionVehicle> Vehicles { get; set; }

        [JsonIgnore]
        private DateTime LastSave;

        public Faction(Guid id, string name, int type, List<string> ranks = null, List<FactionVehicle> vehicles = null)
        {
            ID = id;
            Name = name;
            Type = type;
            Ranks = ranks ?? new List<string>();
            Vehicles = vehicles ?? new List<FactionVehicle>();
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.FACTION_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }
    }
    #endregion
}
