﻿using GTANetworkAPI;
using gtalife.src.Managers;
using Newtonsoft.Json;

namespace gtalife.src.Faction
{
    #region FactionVehicle Class
    public class FactionVehicle
    {
        public int Model { get; }
        public int PrimaryColor { get; }
        public int SecondaryColor { get; }

        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }

        [JsonIgnore]
        public Vehicle Vehicle { get; private set; }

        public FactionVehicle(int model, int color1, int color2, Vector3 position, Vector3 rotation)
        {
            Model = model;
            PrimaryColor = color1;
            SecondaryColor = color2;

            Position = position;
            Rotation = rotation;
        }

        public void Create(uint dimension = 0)
        {
            Vehicle = VehicleManager.CreateVehicle((VehicleHash)Model, Position, Rotation, PrimaryColor, SecondaryColor, dimension);
            Vehicle.EngineStatus = false;
        }

        public void Destroy()
        {
            if (Vehicle != null) Vehicle.Delete();
        }
    }
    #endregion
}
