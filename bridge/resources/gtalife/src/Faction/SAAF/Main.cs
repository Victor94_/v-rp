﻿using GTANetworkAPI;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Extensions;
using gtalife.src.Player.Outfit;
using gtalife.src.Player.Utils;
using System.Linq;

namespace gtalife.src.Faction.SAAF
{
    class Main : Script
    {
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // Blip
            Blip blip = NAPI.Blip.CreateBlip(new Vector3(-2172.735, 3255.833, 32.81029));
            blip.ShortRange = true;
            blip.Sprite = 73;
            blip.Scale = 0.5f;

            // Markers
            NAPI.TextLabel.CreateTextLabel($"~y~SAAF~s~\nUniforme\nPulsa ~y~E ~s~para se interactuar", new Vector3(-2172.735, 3255.833, 32.81029 + 0.5), 15f, 0.5f, 1, new Color(255, 255, 255, 255));
            NAPI.Marker.CreateMarker(1, new Vector3(-2172.735, 3255.833, 32.81029 - 1.0), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1, new Color(255, 255, 22, 22));

            var ColShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(-2172.735, 3255.833, 32.81029), 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.SetData("Military_MarkerID", 1);
                    player.TriggerEvent("OnEnterMilitaryMark", 1);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.ResetData("Military_MarkerID");
                    player.TriggerEvent("OnLeaveMilitaryMark");
                }
            };

            // Weapons
            blip = NAPI.Blip.CreateBlip(new Vector3(-2156.494, 3280.378, 32.8103));
            blip.ShortRange = true;
            blip.Sprite = 110;
            blip.Scale = 0.5f;

            NAPI.TextLabel.CreateTextLabel($"~y~SAAF~s~\nArmas\nPulsa ~y~E ~s~para se interactuar", new Vector3(-2156.494, 3280.378, 32.8103 + 0.5), 15f, 0.5f, 1, new Color(255, 255, 255, 255));
            NAPI.Marker.CreateMarker(1, new Vector3(-2156.494, 3280.378, 32.8103 - 1.0), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1, new Color(255, 255, 22, 22));

            ColShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(-2156.494, 3280.378, 32.8103), 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.SetData("Military_MarkerID", 2);
                    player.TriggerEvent("OnEnterMilitaryMark", 2);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.ResetData("Military_MarkerID");
                    player.TriggerEvent("OnLeaveMilitaryMark");
                }
            };
        }

        [RemoteEvent("MilitaryInteract")]
        public void MilitaryInteract(Client player, object[] arguments)
        {
            if (!player.HasData("Military_MarkerID")) return;

            Faction faction = src.Faction.Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());
            if (faction == null || faction.Type != (int)FactionType.Army)
            {
                player.SendChatMessage("~r~ERROR: ~w~No es de la policía.");
                return;
            }

            var marker = player.GetData("Military_MarkerID");
            switch (marker)
            {
                case 1:
                    {
                        TogglePlayerOnDuty(player);
                        break;
                    }
                case 2:
                    {
                        if (!IsOnDuty(player))
                        {
                            player.SendChatMessage("~r~ERROR: ~w~No estás de servicio.");
                            break;
                        }
                        player.TriggerEvent("Military_ShowWeaponMenu");
                        break;
                    }
            }
        }

        [RemoteEvent("MILITARY_GET_WEAPON")]
        public void MilitaryGetWeapon(Client player, object[] arguments)
        {
            WeaponHash weapon = (WeaponHash)arguments[0];
            switch (weapon)
            {
                case WeaponHash.StunGun:
                    player.giveItem(ItemID.Weapon_StunGun, 1);
                    break;
                case WeaponHash.Pistol:
                    player.giveItem(ItemID.Weapon_Pistol, 1);
                    break;
                case WeaponHash.Pistol50:
                    player.giveItem(ItemID.Weapon_Pistol50, 1);
                    break;
                case WeaponHash.Revolver:
                    player.giveItem(ItemID.Weapon_Revolver, 1);
                    break;
                case WeaponHash.MicroSMG:
                    if (player.GetFactionRank() > 3)
                    {
                        player.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        player.TriggerEvent("Military_ShowWeaponMenu");
                        return;
                    }
                    player.giveItem(ItemID.Weapon_MicroSMG, 1);
                    break;
                case WeaponHash.SMG:
                    if (player.GetFactionRank() > 3)
                    {
                        player.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        player.TriggerEvent("Military_ShowWeaponMenu");
                        return;
                    }
                    player.giveItem(ItemID.Weapon_SMG, 1);
                    break;
                case WeaponHash.Gusenberg:
                    if (player.GetFactionRank() > 3)
                    {
                        player.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        player.TriggerEvent("Military_ShowWeaponMenu");
                        return;
                    }
                    player.giveItem(ItemID.Weapon_Gusenberg, 1);
                    break;
                case WeaponHash.AssaultRifle:
                    if (player.GetFactionRank() > 2)
                    {
                        player.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        player.TriggerEvent("Military_ShowWeaponMenu");
                        return;
                    }
                    player.giveItem(ItemID.Weapon_AssaultRifle, 1);
                    break;
                case WeaponHash.BullpupRifle:
                    if (player.GetFactionRank() > 2)
                    {
                        player.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        player.TriggerEvent("Military_ShowWeaponMenu");
                        return;
                    }
                    player.giveItem(ItemID.Weapon_BullpupRifle, 1);
                    break;
                case WeaponHash.SniperRifle:
                    if (player.GetFactionRank() > 1)
                    {
                        player.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        player.TriggerEvent("Military_ShowWeaponMenu");
                        return;
                    }
                    player.giveItem(ItemID.Weapon_SniperRifle, 1);
                    break;
                case WeaponHash.PumpShotgun:
                    player.giveItem(ItemID.Weapon_PumpShotgun, 1);
                    break;
                case WeaponHash.SawnOffShotgun:
                    if (player.GetFactionRank() > 4)
                    {
                        player.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        player.TriggerEvent("Military_ShowWeaponMenu");
                        return;
                    }
                    player.giveItem(ItemID.Weapon_SawnOffShotgun, 1);
                    break;
                case WeaponHash.Nightstick:
                    player.giveItem(ItemID.Weapon_Nightstick, 1);
                    break;
                case WeaponHash.Flashlight:
                    player.giveItem(ItemID.Weapon_Flashlight, 1);
                    break;
            }
            player.sendChatAction("toma un arma del almacenamiento.");
        }

        public static void TogglePlayerOnDuty(Client player)
        {
            if (!player.HasData("IsUsingPoliceUniform"))
            {
                player.SetOutfit(164);
                player.SetClothes(1, 0, 0);
                player.SetData("IsUsingPoliceUniform", true);
                player.sendChatAction("viste el uniforme.");
            }
            else
            {
                player.loadClothes();
                player.ResetData("IsUsingPoliceUniform");
                player.sendChatAction("desaprovecha el uniforme.");
            }
        }

        public static bool IsOnDuty(Client player)
        {
            return NAPI.Data.HasEntityData(player, "IsUsingPoliceUniform");
        }
    }
}
