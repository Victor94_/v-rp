﻿using GTANetworkAPI;
using gtalife.src.Admin;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Extensions;
using gtalife.src.Player.Outfit;
using gtalife.src.Player.Utils;
using gtalife.src.Managers;
using System.Collections.Generic;
using System.Linq;

namespace gtalife.src.Faction.Police
{
    public class Door
    {
        public int Model { get; }

        public Vector3 Position { get; }

        public ColShape Colshape { get; set; }

        public Door(int model, Vector3 position)
        {
            Model = model;
            Position = position;
        }
    }

    class Police : Script
    {
        public static List<VehicleHash> PoliceVehicles = new List<VehicleHash>()
        {
            VehicleHash.Police, VehicleHash.Police2, VehicleHash.Police3,
            VehicleHash.Police4, VehicleHash.Policeb, VehicleHash.PoliceOld1,
            VehicleHash.PoliceOld2, VehicleHash.PoliceT, VehicleHash.Sheriff,
            VehicleHash.Sheriff2, VehicleHash.Pranger
        };

        public static List<Door> PoliceDoors = new List<Door>()
        {
            new Door(-1320876379, new Vector3(446.5728, -980.0106, 30.8393)),
            new Door(-1320876379, new Vector3(446.5728, -980.0106, 30.8393)),
            new Door(185711165, new Vector3(450.1041, -81.4915, 30.8393)),
            new Door(185711165, new Vector3(450.1041, -984.0915, 30.8393)),
            new Door(749848321, new Vector3(453.0793, -983.1895, 30.83926)),
            new Door(1557126584, new Vector3(450.1041, -985.7384, 30.8393)),
            new Door(-2023754432, new Vector3(452.6248, -987.3626, 30.8393)),
            new Door(749848321, new Vector3(461.2865, -985.3206, 30.83926)),
            new Door(-2023754432, new Vector3(452.6248, -987.3626, 30.8393)),
            new Door(185711165, new Vector3(443.4078, -989.4454, 30.8393)),
            new Door(185711165, new Vector3(446.0079, -989.4454, 30.8393)),
            new Door(-131296141, new Vector3(443.0298, -991.941, 30.8393)),
            new Door(-131296141, new Vector3(443.0298, -994.5412, 30.8393)),
            new Door(-1033001619, new Vector3(463.4782, -1003.538, 25.00599)),
            new Door(631614199, new Vector3(461.8065, -994.4086, 25.06443)),
            new Door(631614199, new Vector3(461.8065, -997.6583, 25.06443)),
            new Door(631614199, new Vector3(461.8065, -1001.302, 25.06443)),
            new Door(631614199, new Vector3(464.5701, -992.6641, 25.06443))
        };

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // Markers
            NAPI.TextLabel.CreateTextLabel($"~y~Polícia~s~\nUniforme\nPulsa ~y~E ~s~para se interactuar", new Vector3(451.0035, -992.9563, 30.68959 + 0.5), 15f, 0.5f, 1, new Color(255, 255, 255, 255));
            NAPI.Marker.CreateMarker(1, new Vector3(451.0035, -992.9563, 30.68959 - 1.0), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1, new Color(255, 255, 22, 22));

            var ColShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(451.0035, -992.9563, 30.68959), 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.SetData("Police_MarkerID", 1);
                    player.TriggerEvent("OnEnterPoliceMark", 1);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.ResetData("Police_MarkerID");
                    player.TriggerEvent("OnLeavePoliceMark");
                }
            };

            // Weapons
            NAPI.TextLabel.CreateTextLabel($"~y~Polícia~s~\nArmas\nPulsa ~y~E ~s~para se interactuar", new Vector3(459.0013, -980.9437, 30.68959 + 0.5), 15f, 0.5f, 1, new Color(255, 255, 255, 255));
            NAPI.Marker.CreateMarker(1, new Vector3(459.0013, -980.9437, 30.68959 - 1.0), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1, new Color(255, 255, 22, 22));

            ColShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(459.0013, -980.9437, 30.68959), 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.SetData("Police_MarkerID", 2);
                    player.TriggerEvent("OnEnterPoliceMark", 2);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.ResetData("Police_MarkerID");
                    player.TriggerEvent("OnLeavePoliceMark");
                }
            };

            // Doors
            foreach (var door in PoliceDoors)
            {
                var i = DoorManager.registerDoor(door.Model, door.Position);
                DoorManager.setDoorState(i, true, 0);

                door.Colshape = NAPI.ColShape.CreateCylinderColShape(door.Position, 1f, 1f);
                door.Colshape.OnEntityEnterColShape += (s, ent) =>
                {
                    Client player;

                    if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                    {
                        Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());
                        if (!player.IsAdmin() && (faction == null || faction.Type != (int)FactionType.Police)) return;

                        DoorManager.setDoorState(i, false, 0);
                        NAPI.ClientEvent.TriggerClientEvent(player, "DisplaySubtitle", $"Puerta desbloqueada", 500);
                    }
                };

                door.Colshape.OnEntityExitColShape += (s, ent) =>
                {
                    Client player;

                    if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                    {
                        Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());
                        if (!player.IsAdmin() && (faction == null || faction.Type != (int)FactionType.Police)) return;

                        DoorManager.setDoorState(i, true, 0);
                        NAPI.ClientEvent.TriggerClientEvent(player, "DisplaySubtitle", $"Puerta bloqueada", 500);
                    }
                };
            }
        }

        [RemoteEvent("PoliceInteract")]
        public void PoliceInteract(Client sender, object[] arguments)
        {
            if (!sender.HasData("Police_MarkerID")) return;

            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == sender.GetFactionID());
            if (faction == null || faction.Type != (int)FactionType.Police)
            {
                sender.SendChatMessage("~r~ERROR: ~w~No es de la policía.");
                return;
            }

            var marker = sender.GetData("Police_MarkerID");
            switch (marker)
            {
                case 1:
                    {
                        TogglePlayerOnDuty(sender);
                        break;
                    }
                case 2:
                    {
                        if (!IsOnDuty(sender))
                        {
                            sender.SendChatMessage("~r~ERROR: ~w~No estás de servicio.");
                            break;
                        }
                        sender.TriggerEvent("Police_ShowWeaponMenu");
                        break;
                    }
            }
        }

        [RemoteEvent("POLICE_GET_WEAPON")]
        public void PoliceGetWeapon(Client sender, object[] arguments)
        {
            WeaponHash weapon = (WeaponHash)arguments[0];
            switch (weapon)
            {
                case WeaponHash.StunGun:
                    sender.giveItem(ItemID.Weapon_StunGun, 1);
                    break;
                case WeaponHash.Pistol:
                    sender.giveItem(ItemID.Weapon_Pistol, 1);
                    break;
                case WeaponHash.Pistol50:
                    sender.giveItem(ItemID.Weapon_Pistol50, 1);
                    break;
                case WeaponHash.Revolver:
                    sender.giveItem(ItemID.Weapon_Revolver, 1);
                    break;
                case WeaponHash.MicroSMG:
                    if (sender.GetFactionRank() > 3)
                    {
                        sender.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        sender.TriggerEvent("Police_ShowWeaponMenu");
                        return;
                    }
                    sender.giveItem(ItemID.Weapon_MicroSMG, 1);
                    break;
                case WeaponHash.SMG:
                    if (sender.GetFactionRank() > 3)
                    {
                        sender.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        sender.TriggerEvent("Police_ShowWeaponMenu");
                        return;
                    }
                    sender.giveItem(ItemID.Weapon_SMG, 1);
                    break;
                case WeaponHash.Gusenberg:
                    if (sender.GetFactionRank() > 3)
                    {
                        sender.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        sender.TriggerEvent("Police_ShowWeaponMenu");
                        return;
                    }
                    sender.giveItem(ItemID.Weapon_Gusenberg, 1);
                    break;
                case WeaponHash.AssaultRifle:
                    if (sender.GetFactionRank() > 2)
                    {
                        sender.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        sender.TriggerEvent("Police_ShowWeaponMenu");
                        return;
                    }
                    sender.giveItem(ItemID.Weapon_AssaultRifle, 1);
                    break;
                case WeaponHash.BullpupRifle:
                    if (sender.GetFactionRank() > 2)
                    {
                        sender.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        sender.TriggerEvent("Police_ShowWeaponMenu");
                        return;
                    }
                    sender.giveItem(ItemID.Weapon_BullpupRifle, 1);
                    break;
                case WeaponHash.SniperRifle:
                    if (sender.GetFactionRank() > 1)
                    {
                        sender.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        sender.TriggerEvent("Police_ShowWeaponMenu");
                        return;
                    }
                    sender.giveItem(ItemID.Weapon_SniperRifle, 1);
                    break;
                case WeaponHash.PumpShotgun:
                    sender.giveItem(ItemID.Weapon_PumpShotgun, 1);
                    break;
                case WeaponHash.SawnOffShotgun:
                    if (sender.GetFactionRank() > 4)
                    {
                        sender.SendChatMessage("~r~ERROR: ~w~No tienes permiso para obtener esta arma.");
                        sender.TriggerEvent("Police_ShowWeaponMenu");
                        return;
                    }
                    sender.giveItem(ItemID.Weapon_SawnOffShotgun, 1);
                    break;
                case WeaponHash.Nightstick:
                    sender.giveItem(ItemID.Weapon_Nightstick, 1);
                    break;
                case WeaponHash.Flashlight:
                    sender.giveItem(ItemID.Weapon_Flashlight, 1);
                    break;
            }
            sender.sendChatAction("toma un arma del almacenamiento.");
        }

        [ServerEvent(Event.PlayerEnterVehicle)]
        public void OnPlayerEnterVehicle(Client player, Vehicle vehicle, sbyte seatID)
        {
            if (PoliceVehicles.Contains((VehicleHash)NAPI.Entity.GetEntityModel(vehicle)) && seatID == -1)
            {
                Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());
                if (!player.IsAdmin() && (faction == null || faction.Type != (int)FactionType.Police))
                {
                    NAPI.Player.WarpPlayerOutOfVehicle(player);
                    player.SendChatMessage("~r~ERROR: ~w~No es de la policía.");
                }
            }
        }

        public static bool IsOnDuty(Client player)
        {
            return NAPI.Data.HasEntityData(player, "IsUsingPoliceUniform");
        }

        public static void TogglePlayerOnDuty(Client player)
        {
            if (!player.HasData("IsUsingPoliceUniform"))
            {
                player.SetOutfit(66);
                player.SetData("IsUsingPoliceUniform", true);
                player.sendChatAction("viste el uniforme.");
            }
            else
            {
                player.loadClothes();
                player.ResetData("IsUsingPoliceUniform");
                player.sendChatAction("desaprovecha el uniforme.");
            }
        }
    }
}
