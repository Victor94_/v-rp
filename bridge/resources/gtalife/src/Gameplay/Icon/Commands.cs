﻿using GTANetworkAPI;
using gtalife.src.Admin;
using System.IO;

namespace gtalife.src.Gameplay.Icon
{
    class Commands : Script
    {
        [Command("blipcmds")]
        public void CMD_BlipCommands(Client player)
        {
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Blips ~~~~~~~~~~~");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /crearblip - /borrarblip");
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Blips ~~~~~~~~~~~");
        }

        [Command("crearblip")]
        public void CMD_CreateBlip(Client player, string name, uint sprite, int color, bool shortRange = true)
        {
            if (player.IsAdmin())
            {
                if (sprite < 1 || sprite > 609)
                {
                    player.SendChatMessage("~r~ERROR: ~w~Sprite es inválido.");
                    return;
                }
                else if (color < 1 || color > 85)
                {
                    player.SendChatMessage("~r~ERROR: ~w~Color es inválida.");
                    return;
                }

                Icon new_icon = new Icon(Main.GetGuid(), name, sprite, color, player.Position, shortRange);
                new_icon.Save();

                Main.Icons.Add(new_icon);

                player.SendChatMessage("~g~ÉXITO: ~w~Creaste un blip en su posicion.");
            }
            else NAPI.Notification.SendNotificationToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("borrarblip")]
        public void CMD_DeleteBlip(Client player)
        {
            if (player.IsAdmin())
            {
                float dist = 15f;
                Icon icon = null;

                foreach (var ic in Main.Icons)
                {
                    if (player.Position.DistanceTo(ic.Position) < dist)
                    {
                        dist = player.Position.DistanceTo(ic.Position);
                        icon = ic;
                    }
                }

                if (icon != null)
                {
                    icon.Destroy();
                    Main.Icons.Remove(icon);

                    string icon_file = Main.BLIP_SAVE_DIR + Path.DirectorySeparatorChar + icon.ID + ".json";
                    if (File.Exists(icon_file)) File.Delete(icon_file);

                    player.SendChatMessage("~g~ÉXITO: ~w~Borraste el blip.");
                }
                else player.SendChatMessage("~r~ERROR: ~w~No estás cerca de un blip dinámico.");

            }
            else NAPI.Notification.SendNotificationToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
        }
    }
}
