﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace gtalife.src.Gameplay.Gang
{
    public class GangZone
    {
        public Guid ID { get; }
        public Guid? GangID { get; private set; }
        public Vector3 Position { get; private set; }

        // entities
        [JsonIgnore]
        private Blip Blip;

        [JsonIgnore]
        private Marker Marker;

        [JsonIgnore]
        private ColShape ColShape;

        [JsonIgnore]
        private TextLabel Label;

        // misc
        [JsonIgnore]
        public List<Client> PlayersInside = new List<Client>();

        [JsonIgnore]
        private DateTime LastSave;

        // gang attack
        [JsonIgnore]
        public bool IsUnderAttack { get; private set; }

        [JsonIgnore]
        public Guid? Attacker { get; private set; }

        [JsonIgnore]
        public DateTime AttackStartedAt { get; private set; }

        public GangZone(Guid id, Guid? gangid, Vector3 position)
        {
            ID = id;
            GangID = gangid;
            Position = position;

            // create blip
            Blip = NAPI.Blip.CreateBlip(position);

            Blip.Name = "Barrio";
            Blip.Sprite = 491;
            Blip.Color = 70;
            Blip.Scale = 1f;
            Blip.ShortRange = true;
            UpdateBlip();

            // create marker
            Marker = NAPI.Marker.CreateMarker(1, position - new Vector3(0.0, 0.0, 1.0), new Vector3(), new Vector3(), 6, new Color(150, 255, 255, 12));

            // create colshape
            ColShape = NAPI.ColShape.CreateCylinderColShape(position, 3.0f, 4.0f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.SetData("GangZoneMarker_ID", ID);
                    player.TriggerEvent("ShowGangZoneText", 1);
                    PlayersInside.Add(player);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.ResetData("GangZoneMarker_ID");
                    player.TriggerEvent("ShowGangZoneText", 0);
                    PlayersInside.Remove(player);
                }
            };

            // create text label
            Label = NAPI.TextLabel.CreateTextLabel($"~y~Barrio~n~~s~Controlado por: Nadie", position, 15f, 0.65f, 1, new Color(255, 255, 255, 255));
            UpdateLabel();
        }

        public void SetOwner(Guid? gang_id)
        {
            GangID = gang_id;
            UpdateBlip();
            UpdateLabel();
            Save();
        }

        private void UpdateLabel()
        {
            if (GangID != null)
            {
                Gang gang = Main.Gangs.FirstOrDefault(g => g.ID == GangID);
                if (gang != null)
                {
                    Label.Text = string.Format("~y~Barrio~n~~s~Controlado por: {0}", gang.Name);
                }
                else
                {
                    GangID = null;
                    Label.Text = string.Format("~y~Barrio~n~~s~Controlado por: Nadie");
                }
            }
            else
            {
                Label.Text = string.Format("~y~Barrio~n~~s~Controlado por: Nadie");
            }
        }

        private void UpdateBlip()
        {
            if (GangID != null)
            {
                Gang gang = Main.Gangs.FirstOrDefault(g => g.ID == GangID);
                if (gang != null)
                {
                    Blip.Name = $"Barrio de {gang.Name}";
                }
                else
                {
                    GangID = null;
                    Blip.Name = $"Barrio";
                }
            }
            else
            {
                Blip.Name = $"Barrio";
            }
        }

        public void SetUnderAttack(bool isunderattack, Guid? attacker = null)
        {
            Attacker = attacker;
            IsUnderAttack = isunderattack;

            if (IsUnderAttack)
            {
                // Blip.Flashing = true; // deprecated
                AttackStartedAt = DateTime.Now;
            }
            // else Blip.Flashing = false;  // deprecated
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.GANG_ZONE_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }

        public void Destroy()
        {
            Blip.Delete();
            Marker.Delete();
            NAPI.ColShape.DeleteColShape(ColShape);
            Label.Delete();
        }
    }
}
