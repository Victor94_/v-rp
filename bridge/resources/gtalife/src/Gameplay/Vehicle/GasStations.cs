﻿using GTANetworkAPI;
using gtalife.src.Player.Utils;

namespace gtalife.src.Gameplay
{
    class GasStations : Script
    {
        [Command("llenar")]
        public void FillCommand(Client player)
        {
            if (player.HasData("IsFillingVehicle"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Ya estás llenando el vehículo");
                return;
            }

            foreach (var business in Business.Main.Businesses)
            {
                if (business.Type == 1 && business.Position.DistanceTo(player.Position) < 25f)
                {
                    if (!player.IsInVehicle)
                    {
                        player.SendChatMessage("~r~ERROR: ~w~Debes estar en un vehículo.");
                        return;
                    }

                    if (player.VehicleSeat != -1)
                    {
                        player.SendChatMessage("~r~ERROR: ~w~Tienes que ser el conductor.");
                        return;
                    }

                    if (player.Vehicle.EngineStatus)
                    {
                        player.SendChatMessage("~r~ERROR: ~w~Apaga el motor primero.");
                        return;
                    }

                    /* deprecated
                     * if (player.Vehicle.FuelLevel >= 100)
                    {
                        player.SendChatMessage("~r~ERROR: ~w~Su vehículo ya está lleno.");
                        return;
                    }

                    int price = (100 - (int)player.Vehicle.fuelLevel) * 6;
                    if (player.getMoney() < price)
                    {
                        player.SendChatMessage("~r~ERROR: ~w~No tienes suficiente dinero.");
                        return;
                    }*/

                    player.SendChatMessage("~y~INFO: ~w~Llenando vehículo.");
                    player.sendChatAction("está llenando el vehículo.");

                    var vehicle = player.Vehicle;
                    vehicle.FreezePosition = true;

                    System.Threading.Tasks.Task.Run(() =>
                    {
                        NAPI.Task.Run(() =>
                        {
                            int price = 100;

                            if (vehicle == null) return;
                            vehicle.FreezePosition = false;
                            // vehicle.fuelLevel = 100f;

                            if (player == null) return;
                            player.SendChatMessage($"~g~ÉXITO: ~w~Vehículo lleno por ~g~${price}~w~.");
                            player.giveMoney(-price);
                            player.ResetData("IsFillingVehicle");

                            business.GiveMoney(price);
                            player.TriggerEvent("BusinessUpdateSafe", NAPI.Util.ToJson(new { business.Money }));
                        }, delayTime: 10000);
                        // 250 * (100 - (int)vehicle.fuelLevel)
                    });

                    return;
                }
            }
            player.SendChatMessage("~r~ERROR: ~w~No estás en una gasolinera.");
        }
    }
}
