"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var menu = null;
API.onServerEventTrigger.connect((eventName, args) => {
    if (eventName === "open_vehicle_menu") {
        menu = API.createMenu("Vehículo", "Selecciona una opción", 0, 0, 6);
        let capo = API.createMenuItem("Capó", "");
        let maletero = API.createMenuItem("Maletero", "");
        let doors = API.createMenuItem("Puertas", "");
        let engine = API.createMenuItem("Motor", "");
        let lock = API.createMenuItem(API.getVehicleLocked(API.getPlayerVehicle(API.getLocalPlayer())) ? "Desbloquear" : "Bloquear", "");
        menu.AddItem(capo);
        menu.AddItem(maletero);
        menu.AddItem(doors);
        menu.AddItem(engine);
        menu.AddItem(lock);
        menu.OnItemSelect.connect(function (sender, item, index) {
            if (item == capo) {
                API.triggerServerEvent("open_vehicle_hood");
            }
            else if (item == maletero) {
                API.triggerServerEvent("open_vehicle_trunk");
            }
            else if (item == doors) {
                API.triggerServerEvent("open_vehicle_doors");
            }
            else if (item == engine) {
                API.triggerServerEvent("on_player_toggle_engine");
            }
            else if (item == lock) {
                API.triggerServerEvent("on_player_toggle_vehicle_lock");
            }
            menu.Visible = false;
        });
        menu.Visible = true;
    }
});
