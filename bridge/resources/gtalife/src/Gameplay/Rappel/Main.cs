﻿using GTANetworkAPI;

namespace gtalife.src.Gameplay.Rappel
{
    public class Main : Script
    {
        [RemoteEvent("RappelFromHelicopter")]
        public void RappelFromHelicopter(Client player, object[] arguments)
        {
            NAPI.Native.SendNativeToPlayersInRangeInDimension(player.Position, 150f, player.Dimension, Hash.CLEAR_PED_TASKS, player.Handle);
            NAPI.Native.SendNativeToPlayersInRangeInDimension(player.Position, 150f, player.Dimension, Hash.TASK_RAPPEL_FROM_HELI, player.Handle, 1092616192);
        }

        [Command("rappel")]
        public void CMD_Rappel(Client player)
        {
            player.TriggerEvent("Rappel");
        }
    }
}
