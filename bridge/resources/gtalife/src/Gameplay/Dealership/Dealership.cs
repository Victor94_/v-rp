﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using GTANetworkAPI;
using Newtonsoft.Json;
using gtalife.src.Database.Models;
using gtalife.src.Player.Utils;

namespace gtalife.src.Gameplay.Dealership
{
    #region Dealership Class
    public class Dealership
    {
        public Guid ID { get; }
        public string Name { get; private set; }
        public Vector3 Position { get; }
        public Vector3 VehicleSpawn { get; private set; }

        // storage
        public long Money { get; private set; }
        public List<DealershipVehicle> Vehicles { get; }

        public DateTime CreatedAt { get; }

        // entities
        [JsonIgnore]
        private Blip Blip;

        [JsonIgnore]
        private Marker Marker;

        [JsonIgnore]
        private ColShape ColShape;

        [JsonIgnore]
        private TextLabel Label;

        [JsonIgnore]
        private DateTime LastSave;

        public Dealership(Guid id, Vector3 position, string name = "", Vector3 vehiclespawn = null, long money = 0, List<DealershipVehicle> vehicles = null)
        {
            ID = id;
            Position = position;
            VehicleSpawn = vehiclespawn ?? new Vector3(0f, 0f, 0f);
            CreatedAt = DateTime.Now;

            Name = name;

            Money = money;
            Vehicles = (vehicles == null) ? new List<DealershipVehicle>() : vehicles;

            // create blip
            Blip = NAPI.Blip.CreateBlip(position);
            Blip.Name = "Concesión";
            Blip.Scale = 1f;
            Blip.ShortRange = true;
            UpdateBlip();

            // create colshape
            ColShape = NAPI.ColShape.CreateCylinderColShape(position, 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.SetData("DealershipMarker_ID", ID);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.ResetData("DealershipMarker_ID");
                }
            };

            // create marker
            Marker = NAPI.Marker.CreateMarker(1, position - new Vector3(0.0, 0.0, 1.0), new Vector3(), new Vector3(), 1, new Color(150, 252, 210, 34));

            // create text label
            Label = NAPI.TextLabel.CreateTextLabel("Concesión", position, 15f, 0.65f, 1, new Color(255, 255, 255, 255));
            UpdateLabel();
        }

        private void UpdateLabel()
        {
            Label.Text = string.Format("~y~Concesión~n~~w~{0}", Name);
        }

        private void UpdateBlip()
        {
            Blip.Sprite = 225;
            Blip.Color = 5;
        }

        public void SetSpawn(Vector3 spawn)
        {
            VehicleSpawn = spawn;

            Save();
        }

        public void SetName(string new_name)
        {
            Name = new_name;

            UpdateLabel();
            Save();
        }

        public void GiveMoney(int amount)
        {
            Money += amount;

            Save();
        }

        public void SetMoney(int amount)
        {
            Money = amount;

            Save();
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.DEALERSHIP_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }

        public void Destroy(bool exit = false)
        {
            Blip.Delete();
            Marker.Delete();
            NAPI.ColShape.DeleteColShape(ColShape);
            Label.Delete();
        }
    }
    #endregion

    public class Main : Script
    {
        // settings
        public static string DEALERSHIP_SAVE_DIR = "data/DealershipData";
        public static int PLAYER_DEALERSHIP_LIMIT = 0;
        public static int DEALERSHIP_MONEY_LIMIT = 5000000;
        public static int SAVE_INTERVAL = 120;

        public static List<Dealership> Dealerships = new List<Dealership>();

        #region Methods
        public static Guid GetGuid()
        {
            Guid new_guid;

            do
            {
                new_guid = Guid.NewGuid();
            } while (Dealerships.Count(h => h.ID == new_guid) > 0);

            return new_guid;
        }
        #endregion

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // load settings
            if (NAPI.Resource.HasSetting(this, "dealershipDirName")) DEALERSHIP_SAVE_DIR = NAPI.Resource.GetSetting<string>(this, "dealershipDirName");

            DEALERSHIP_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + DEALERSHIP_SAVE_DIR;
            if (!Directory.Exists(DEALERSHIP_SAVE_DIR)) Directory.CreateDirectory(DEALERSHIP_SAVE_DIR);

            if (NAPI.Resource.HasSetting(this, "playerDealershipLimit")) PLAYER_DEALERSHIP_LIMIT = NAPI.Resource.GetSetting<int>(this, "playerDealershipLimit");
            if (NAPI.Resource.HasSetting(this, "dealershipMoneyLimit")) DEALERSHIP_MONEY_LIMIT = NAPI.Resource.GetSetting<int>(this, "dealershipMoneyLimit");
            if (NAPI.Resource.HasSetting(this, "saveInterval")) SAVE_INTERVAL = NAPI.Resource.GetSetting<int>(this, "saveInterval");

            NAPI.Util.ConsoleOutput("-> Player Dealership Limit: {0}", ((PLAYER_DEALERSHIP_LIMIT == 0) ? "Disabled" : PLAYER_DEALERSHIP_LIMIT.ToString()));
            NAPI.Util.ConsoleOutput("-> Dealership Safe Limit: ${0:n0}", DEALERSHIP_MONEY_LIMIT);

            // load dealerships
            foreach (string file in Directory.EnumerateFiles(DEALERSHIP_SAVE_DIR, "*.json"))
            {
                Dealership dealership = JsonConvert.DeserializeObject<Dealership>(File.ReadAllText(file));
                Dealerships.Add(dealership);

                // vehicles
                foreach (DealershipVehicle vehicle in dealership.Vehicles) vehicle.Create();
            }

            NAPI.Util.ConsoleOutput("Loaded {0} dealerships.", Dealerships.Count);
        }

        [RemoteEvent("RequestDealershipBuyMenu")]
        public void RequestDealershipBuyMenu(Client player, object[] arguments)
        {
            if (!player.HasData("DealershipVehicle_ID")) return;

            DealershipVehicle vehicle = player.GetData("DealershipVehicle_ID");
            player.TriggerEvent("Dealership_PurchaseMenu", NAPI.Util.ToJson(new { vehicle.Price }));
        }

        [RemoteEvent("VehiclePurchase")]
        public void VehiclePurchase(Client player, object[] arguments)
        {
            if (!player.HasData("DealershipVehicle_ID")) return;

            int color1 = (int)arguments[0];
            int color2 = (int)arguments[1];

            Dealership dealership = player.GetData("Dealership_ID");
            DealershipVehicle vehicle = player.GetData("DealershipVehicle_ID");

            if (Player.Data.Character[player].Vehicles.Count >= 3) player.SendChatMessage("~r~ERROR: ~s~Solo puedes tener hasta tres vehículos.");
            else if (vehicle.Price > Player.Data.Character[player].Money) player.SendChatMessage("~r~ERROR: ~s~No tienes suficiente dinero.");
            else
            {
                var veh = NAPI.Vehicle.CreateVehicle(vehicle.Model, new Vector3(dealership.VehicleSpawn.X, dealership.VehicleSpawn.Y, dealership.VehicleSpawn.Z), 0f, color1, color2);
                veh.EngineStatus = false;

                player.Position = new Vector3(player.Position.X, player.Position.Y, player.Position.Z + 0.5f);
                System.Threading.Tasks.Task.Run(() =>
                {
                    NAPI.Task.Run(() =>
                    {
                        NAPI.Player.SetPlayerIntoVehicle(player, veh, -1);
                    }, delayTime: 500);
                });

                player.SetIntoVehicle(veh, -1);

                player.ResetData("Dealership_ID");
                player.ResetData("DealershipVehicle_ID");
                player.TriggerEvent("OnExitDealershipVehicle");

                player.giveMoney(-vehicle.Price);
                player.SendNotification($"Usted compró un ~g~{veh.DisplayName}~s~.");

                if (!Player.Award.Main.IsAwardUnlocked(player, "new_car")) Player.Award.Main.UnlockAward(player, "new_car");

                CharacterVehicle characterVehicle = new CharacterVehicle
                {
                    Character = Player.Data.Character[player],
                    Handle = veh,
                    Price = vehicle.Price,
                    Model = vehicle.Model,
                    Color1 = color1,
                    Color2 = color2,
                    Fuel = 100,
                    PositionX = veh.Position.X,
                    PositionY = veh.Position.Y,
                    PositionZ = veh.Position.Z,
                    RotationX = 0.0f,
                    RotationY = 0.0f,
                    RotationZ = 0.0f,
                    CreatedAt = DateTime.Now
                };

                Player.Data.Character[player].Vehicles.Add(characterVehicle);
            }
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (Dealership dealership in Dealerships)
            {
                dealership.Save(true);
                dealership.Destroy(true);
            }

            Dealerships.Clear();
        }

        [ServerEvent(Event.PlayerEnterVehicle)]
        public void OnPlayerEnterVehicle(Client player, Vehicle vehicle, sbyte seatID)
        {
            foreach (Dealership dealership in Dealerships)
            {
                foreach (DealershipVehicle v in dealership.Vehicles)
                {
                    if (vehicle == v.Vehicle)
                    {
                        NAPI.Chat.SendChatMessageToPlayer(player, "~y~INFO: ~s~Si quieres comprar este vehículo, pulsa ~y~E~w~.");
                        player.SetData("Dealership_ID", dealership);
                        player.SetData("DealershipVehicle_ID", v);
                        player.TriggerEvent("OnEnterDealershipVehicle");
                    }
                }
            }
        }

        [ServerEvent(Event.PlayerExitVehicle)]
        public void OnPlayerExitVehicle(Client player, Vehicle vehicle)
        {
            if (player.HasData("DealershipVehicle_ID"))
            {
                player.ResetData("Dealership_ID");
                player.ResetData("DealershipVehicle_ID");
                player.TriggerEvent("OnExitDealershipVehicle");
            }
        }

        public static bool IsADealershipVehicle(NetHandle vehicle)
        {
            foreach (Dealership dealership in Dealerships)
            {
                foreach (DealershipVehicle v in dealership.Vehicles)
                {
                    if (vehicle == v.Vehicle)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion
    }    
}
