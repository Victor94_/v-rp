"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var school_text_mode = 0;
var school_purchase_menu = null;
API.onServerEventTrigger.connect((eventName, args) => {
    switch (eventName) {
        case "ShowSchoolText":
            school_text_mode = args[0];
            break;
        case "School_PurchaseMenu":
            var data = JSON.parse(args[0]);
            if (school_purchase_menu == null) {
                school_purchase_menu = API.createMenu("Autoescuela", "Seleccione una opcion.", 0, 0, 6);
                school_purchase_menu.OnItemSelect.connect(function (menu, item, index) {
                    if (index == 0)
                        API.triggerServerEvent("School_StartTest");
                    school_purchase_menu.Visible = false;
                });
            }
            school_purchase_menu.Clear();
            var temp_item = API.createMenuItem("Vehículos", "Seleccionar para comenzar la prueba.");
            temp_item.SetRightLabel("$" + data.Price);
            school_purchase_menu.AddItem(temp_item);
            school_purchase_menu.Visible = true;
            break;
    }
});
API.onKeyDown.connect((sender, e) => {
    if (API.isChatOpen())
        return;
    switch (e.KeyCode) {
        case Keys.E:
            if (school_text_mode == 1 && !API.isAnyMenuOpen()) {
                API.triggerServerEvent("SchoolInteract");
            }
            break;
    }
});
API.onUpdate.connect(() => {
    if (school_text_mode > 0)
        API.displaySubtitle("Presione ~y~E ~w~para interactuar", 100);
});
