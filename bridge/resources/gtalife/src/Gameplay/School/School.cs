﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.IO;

namespace gtalife.src.Gameplay.School
{
    #region School Class
    public class School
    {
        public Guid ID { get; }
        public Vector3 Position { get; }

        // storage
        public long Money { get; private set; }

        // entities
        [JsonIgnore]
        private Blip Blip;

        [JsonIgnore]
        private Marker Marker;

        [JsonIgnore]
        private ColShape ColShape;

        [JsonIgnore]
        private TextLabel Label;

        [JsonIgnore]
        private DateTime LastSave;

        public School(Guid id, Vector3 position, long money = 0)
        {
            ID = id;
            Position = position;
            Money = money;

            // create blip
            Blip = NAPI.Blip.CreateBlip(position);
            Blip.Name = "Autoescuela";
            Blip.Scale = 0f;
            Blip.ShortRange = true;
            UpdateBlip();

            // create colshape
            ColShape = NAPI.ColShape.CreateCylinderColShape(position, 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.SetData("SchoolMarker_ID", ID);
                    player.TriggerEvent("ShowSchoolText", 1);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.ResetData("SchoolMarker_ID");
                    player.TriggerEvent("ShowSchoolText", 0);
                }
            };

            // create marker
            Marker = NAPI.Marker.CreateMarker(1, position - new Vector3(0.0, 0.0, 1.0), new Vector3(), new Vector3(), 1, new Color(34, 252, 210, 150));

            // create text label
            Label = NAPI.TextLabel.CreateTextLabel("Autoescuela", position, 15f, 0.65f, 1, new Color(255, 255, 255, 255));
            UpdateLabel();
        }

        private void UpdateLabel()
        {
            Label.Text = string.Format("~y~Autoescuela");
        }

        private void UpdateBlip()
        {
            Blip.Sprite = 545;
            Blip.Color = 9;
        }

        public void GiveMoney(int amount)
        {
            Money += amount;

            Save();
        }

        public void SetMoney(int amount)
        {
            Money = amount;

            Save();
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.SCHOOL_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }

        public void Destroy(bool exit = false)
        {
            Blip.Delete();
            Marker.Delete();
            NAPI.ColShape.DeleteColShape(ColShape);
            Label.Delete();
        }
    }
    #endregion
}
