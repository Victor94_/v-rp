﻿using System;
using System.Linq;
using System.Collections.Generic;
using GTANetworkAPI;
using Newtonsoft.Json;
using gtalife.src.Player.Utils;

namespace gtalife.src.Gameplay.House
{
    #region HouseFurniture Class
    public class HouseFurniture
    {
        public uint Model { get; }
        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }

        public string Name { get; }
        public int Price { get; }

        [JsonIgnore]
        public GTANetworkAPI.Object Object { get; private set; }

        public HouseFurniture(uint model, Vector3 position, Vector3 rotation, string name, int price)
        {
            Model = model;
            Position = position;
            Rotation = rotation;

            Name = name;
            Price = price;
        }

        public void Create(byte dimension)
        {
            Object = NAPI.Object.CreateObject(Model, Position, Rotation, dimension);
        }

        public void Destroy()
        {
            if (Object != null) Object.Delete();
        }
    }
    #endregion

    #region Furniture Class
    public class Furniture
    {
        public string Name { get; }
        public string ModelName { get; }
        public int Price { get; }

        public Furniture(string name, string model_name, int price)
        {
            Name = name;
            ModelName = model_name;
            Price = price;
        }
    }
    #endregion

    public class HouseFurnitures : Script
    {
        public static List<Furniture> FurnitureList = new List<Furniture>
        {
            // name, model name, price
            new Furniture("Office Chair", "hei_prop_heist_off_chair", 250),
            new Furniture("Director's Chair", "prop_direct_chair_01", 400),
            new Furniture("Safe", "p_v_43_safe_s", 1000),
            new Furniture("Snowman", "prop_prlg_snowpile", 50),
            new Furniture("Cash Pile", "prop_cash_crate_01", 5000),
            new Furniture("Gold Bar", "prop_gold_bar", 6500),
            new Furniture("Gold Pile", "v_ilev_gold", 500000)
        };

        #region Events
        [RemoteEvent("HouseFurnitureCatalogue")]
        public void HouseFurnitureCatalogue(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID")) return;

            House house = Main.Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el dueño puede hacer esto.");
                return;
            }

            player.TriggerEvent("HouseFurnitureCatalogue", NAPI.Util.ToJson(FurnitureList));
        }

        [RemoteEvent("HouseBuyFurniture")]
        public void HouseBuyFurniture(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID") || arguments.Length < 1) return;

            House house = Main.Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el dueño puede hacer esto.");
                return;
            }

            if (Main.HOUSE_FURNITURE_LIMIT > 0 && house.Furnitures.Count >= Main.HOUSE_FURNITURE_LIMIT)
            {
                player.SendNotification("~r~Límite de muebles de la casa alcanzado.");
                return;
            }

            int idx = Convert.ToInt32(arguments[0]);
            if (idx < 0 || idx >= FurnitureList.Count) return;

            if (player.getMoney() < FurnitureList[idx].Price)
            {
                player.SendNotification("~r~No puedes pagar estos muebles.");
                return;
            }

            player.giveMoney(-FurnitureList[idx].Price);

            HouseFurniture new_furniture = new HouseFurniture(NAPI.Util.GetHashKey(FurnitureList[idx].ModelName), player.Position - new Vector3(0.0, 0.0, 0.5), new Vector3(), FurnitureList[idx].Name, FurnitureList[idx].Price);
            house.Furnitures.Add(new_furniture);
            new_furniture.Create((byte)house.Dimension);
            house.Save();

            player.SendNotification(string.Format("~g~Compró un {0} a ${1:n0}.", FurnitureList[idx].Name, FurnitureList[idx].Price));

            player.TriggerEvent("HouseUpdateFurnitures", NAPI.Util.ToJson(house.Furnitures));
        }

        [RemoteEvent("HouseEditFurniture")]
        public void HouseEditFurniture(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID") || arguments.Length < 1) return;

            House house = Main.Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el dueño puede hacer esto.");
                return;
            }

            int idx = Convert.ToInt32(arguments[0]);
            if (idx < 0 || idx >= house.Furnitures.Count) return;

            player.SetData("EditingFurniture_ID", idx);
            player.TriggerEvent("SetEditingHandle", house.Furnitures[idx].Object.Handle);
        }

        [RemoteEvent("HouseSaveFurniture")]
        public void HouseSaveFurniture(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID") || !player.HasData("EditingFurniture_ID") || arguments.Length < 6) return;

            House house = Main.Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el dueño puede hacer esto.");
                return;
            }

            int idx = player.GetData("EditingFurniture_ID");
            if (idx < 0 || idx >= house.Furnitures.Count) return;

            Vector3 new_pos = new Vector3(Convert.ToDouble(arguments[0]), Convert.ToDouble(arguments[1]), Convert.ToDouble(arguments[2]));
            Vector3 new_rot = new Vector3(Convert.ToDouble(arguments[3]), Convert.ToDouble(arguments[4]), Convert.ToDouble(arguments[5]));

            house.Furnitures[idx].Position = new_pos;
            house.Furnitures[idx].Rotation = new_rot;

            house.Furnitures[idx].Object.Position = new_pos;
            house.Furnitures[idx].Object.Rotation = new_rot;
            house.Save();

            player.SendNotification(string.Format("~g~Edited {0}.", house.Furnitures[idx].Name));
            player.ResetData("EditingFurniture_ID");
        }

        [RemoteEvent("HouseResetFurniture")]
        public void HouseResetFurniture(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID") || !player.HasData("EditingFurniture_ID")) return;

            House house = Main.Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el dueño puede hacer esto.");
                return;
            }

            int idx = player.GetData("EditingFurniture_ID");
            if (idx < 0 || idx >= house.Furnitures.Count) return;

            player.TriggerEvent("ResetEntityPosition", house.Furnitures[idx].Position, house.Furnitures[idx].Rotation);
            player.ResetData("EditingFurniture_ID");
        }

        [RemoteEvent("HouseSellFurniture")]
        public void HouseSellFurniture(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID") || arguments.Length < 1) return;

            House house = Main.Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el dueño puede hacer esto.");
                return;
            }

            int idx = Convert.ToInt32(arguments[0]);
            if (idx < 0 || idx >= house.Furnitures.Count) return;

            HouseFurniture furniture = house.Furnitures[idx];
            int price = (int)Math.Round(furniture.Price * 0.8);
            player.giveMoney(price);

            house.Furnitures[idx].Destroy();
            house.Furnitures.RemoveAt(idx);
            house.Save();

            player.SendNotification(string.Format("~g~Vendió un {0} a ${1:n0}.", furniture.Name, price));

            player.TriggerEvent("HouseUpdateFurnitures", NAPI.Util.ToJson(house.Furnitures));
        }
        #endregion
    }
}
