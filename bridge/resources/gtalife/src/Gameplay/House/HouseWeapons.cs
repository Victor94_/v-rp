﻿using System;
using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;

namespace gtalife.src.Gameplay.House
{
    #region HouseWeapon Class
    public class HouseWeapon
    {
        public WeaponHash Hash { get; }
        public int Ammo { get; }

        // customization
        public WeaponTint Tint { get; }
        public WeaponComponent[] Components { get; }

        public HouseWeapon(WeaponHash hash, int ammo, WeaponTint tint, WeaponComponent[] components)
        {
            Hash = hash;
            Ammo = ammo;

            Tint = tint;
            Components = components;
        }
    }
    #endregion

    public class HouseWeapons : Script
    {
        public static List<WeaponHash> WeaponBlacklist = new List<WeaponHash>
        {
            WeaponHash.Unarmed,
            WeaponHash.Snowball
        };

        #region Events
        [RemoteEvent("HousePutGun")]
        public void HousePutGun(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID")) return;

            House house = Main.Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el dueño puede hacer esto.");
                return;
            }

            WeaponHash weapon = player.CurrentWeapon;
            if (WeaponBlacklist.Contains(weapon))
            {
                player.SendNotification("~r~No puedes almacenar esta arma.");
                return;
            }

            if (Main.HOUSE_WEAPON_LIMIT > 0 && house.Weapons.Count >= Main.HOUSE_WEAPON_LIMIT)
            {
                player.SendNotification("~r~Se alcanzó el límite de casillero de la casa.");
                return;
            }

            house.Weapons.Add(new HouseWeapon(weapon, player.GetWeaponAmmo(weapon), player.GetWeaponTint(weapon), player.GetAllWeaponComponents(weapon)));
            house.Save();

            player.SendNotification(string.Format("~g~Almacenado un {0} con {1:n0} munición.", weapon, player.GetWeaponAmmo(weapon)));
            player.RemoveWeapon(weapon);

            player.TriggerEvent("HouseUpdateWeapons", NAPI.Util.ToJson(house.Weapons));
        }

        [RemoteEvent("HouseTakeGun")]
        public void HouseTakeGun(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID") || arguments.Length < 1) return;

            House house = Main.Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el dueño puede hacer esto.");
                return;
            }

            int idx = Convert.ToInt32(arguments[0]);
            if (idx < 0 || idx >= house.Weapons.Count) return;

            HouseWeapon weapon = house.Weapons[idx];
            house.Weapons.RemoveAt(idx);
            house.Save();

            player.GiveWeapon(weapon.Hash, weapon.Ammo);
            foreach (WeaponComponent comp in weapon.Components) player.SetWeaponComponent(weapon.Hash, comp);
            player.SetWeaponTint(weapon.Hash, weapon.Tint);

            player.SendNotification(string.Format("~g~Tomó un {0} con {1:n0} munición.", weapon.Hash, weapon.Ammo));

            player.TriggerEvent("HouseUpdateWeapons", NAPI.Util.ToJson(house.Weapons));
        }
        #endregion
    }
}
