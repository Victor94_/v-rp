﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var menu = null;

var deposit = null;
var withdraw = null;
var balance = null;
var transfer = null;

var bank_text_mode = 0;

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) => {
    if (eventName == "ShowBankText") {
        bank_text_mode = args[0];
    }
});

API.onKeyDown.connect((sender: any, e: System.Windows.Forms.KeyEventArgs) => {
    if (API.isChatOpen()) return;

    if (e.KeyCode === Keys.E) {
        if (bank_text_mode > 0) {
            if (menu == null) {
                menu = API.createMenu("Banco", "Selecciona una opción", 0, 0, 6);

                deposit = API.createMenuItem("Depositar", "Deposita su dinero en el banco");
                withdraw = API.createMenuItem("Sacar", "Retira tu dinero del banco");
                balance = API.createMenuItem("Saldo", "Verifica tu saldo");
                transfer = API.createMenuItem("Transferir", "Transfiere tu dinero a otros jugadores");

                menu.AddItem(deposit);
                menu.AddItem(withdraw);
                menu.AddItem(balance);
                menu.AddItem(transfer);

                menu.OnItemSelect.connect(function (sender, item, index) {
                    menu.Visible = false;

                    if (item == deposit) {
                        var amount = API.getUserInput("", 9);
                        if (parseInt(amount) > 1) API.triggerServerEvent("bank_deposit", amount);
                        else menu.Visible = true;
                    }
                    else if (item == withdraw) {
                        var amount = API.getUserInput("", 9);
                        if (parseInt(amount) > 1) API.triggerServerEvent("bank_withdraw", amount);
                        else menu.Visible = true;
                    }
                    else if (item == balance) {
                        API.triggerServerEvent("bank_balance", amount);
                    }
                    else if (item == transfer) {
                        var amount = API.getUserInput("Cantidad", 9);
                        if (parseInt(amount) > 1) {
                            var playerName = API.getUserInput("Nombre del jugador", 24);
                            API.triggerServerEvent("bank_transfer", amount, playerName);
                        }
                        else menu.Visible = true;

                    }
                });
            }

            menu.Visible = !menu.Visible;
            API.playSoundFrontEnd("CONFIRM_BEEP", "HUD_MINI_GAME_SOUNDSET");
        }
    }
});

API.onUpdate.connect(() => {
    if (bank_text_mode > 0) API.displaySubtitle("Pulsa ~g~E ~w~para interactuar", 100);
});
