﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.IO;

namespace gtalife.src.Gameplay.Bank
{
    #region Bank Class
    public class Bank
    {
        public Guid ID { get; }
        public Vector3 Position { get; }
        public int Type { get; private set; }

        // entities
        [JsonIgnore]
        private Blip Blip;

        [JsonIgnore]
        private Marker Marker;

        [JsonIgnore]
        private ColShape ColShape;

        [JsonIgnore]
        private DateTime LastSave;

        public Bank(Guid id, Vector3 position, int type)
        {
            ID = id;
            Position = position;
            Type = type;

            // create blip
            Blip = NAPI.Blip.CreateBlip(position);
            Blip.ShortRange = true;
            UpdateBlip();

            // create colshape
            ColShape = NAPI.ColShape.CreateCylinderColShape(position, 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.SetData("BankMarker_ID", ID);
                    player.TriggerEvent("ShowBankText", 1);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.ResetData("BankMarker_ID");
                    player.TriggerEvent("ShowBankText", 0);
                }
            };

            // create marker
            Marker = NAPI.Marker.CreateMarker(1, position - new Vector3(0.0, 0.0, 1.0), new Vector3(), new Vector3(), 0.5f, new Color(150, 25, 255, 25));
        }

        private void UpdateBlip()
        {
            Blip.Sprite = 108;
            Blip.Color = 69;
            Blip.Name = (Type == 0) ? "Banco" : "Cajero automático";
            Blip.Scale = (Type == 0) ? 1f : 0.5f;
        }

        public void SetType(int new_type)
        {
            Type = new_type;

            UpdateBlip();
            Save();
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.BANK_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }

        public void Destroy()
        {
            Blip.Delete();
            Marker.Delete();
            NAPI.ColShape.DeleteColShape(ColShape);
        }
    }
    #endregion
}
