﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace gtalife.src.Gameplay.Buildings
{
    #region Building Class
    public class Building
    {
        public Guid ID { get; }
        public string Name { get; private set; }
        public uint BlipSprite { get; private set; }
        public int BlipColor { get; private set; }
        public Vector3 InsidePosition { get; private set; }
        public Vector3 OutsidePosition { get; private set; }
        public uint Dimension { get; set; }

        // entities
        [JsonIgnore]
        private Blip Blip;

        [JsonIgnore]
        private Marker MarkerIn;

        [JsonIgnore]
        private Marker MarkerOut;

        [JsonIgnore]
        private ColShape ColShapeIn;

        [JsonIgnore]
        private ColShape ColShapeOut;

        [JsonIgnore]
        private TextLabel LabelIn;

        [JsonIgnore]
        private TextLabel LabelOut;

        // misc
        [JsonIgnore]
        private List<NetHandle> PlayersInside = new List<NetHandle>();

        [JsonIgnore]
        private DateTime LastSave;

        public Building(Guid id, string name, uint blipsprite, int blipcolor, Vector3 outsideposition, Vector3 insideposition, uint dimension = 0)
        {
            ID = id;
            Name = name;
            BlipSprite = blipsprite;
            BlipColor = blipcolor;
            OutsidePosition = outsideposition;
            InsidePosition = insideposition;
            Dimension = dimension;

            // create blip
            Blip = NAPI.Blip.CreateBlip(OutsidePosition);
            Blip.Sprite = BlipSprite;
            Blip.Name = name;
            Blip.Color = BlipColor;
            Blip.Scale = 1f;
            Blip.ShortRange = true;
            Blip.SetSharedData("PlayersInside", 0);

            // create marker
            MarkerIn = NAPI.Marker.CreateMarker(1, InsidePosition - new Vector3(0.0, 0.0, 1.0), new Vector3(), new Vector3(), 1, new Color(150, 64, 196, 255));
            MarkerOut = NAPI.Marker.CreateMarker(1, OutsidePosition - new Vector3(0.0, 0.0, 1.0), new Vector3(), new Vector3(), 1, new Color(150, 64, 196, 255));

            // create colshape
            ColShapeIn = NAPI.ColShape.CreateCylinderColShape(InsidePosition, 0.85f, 0.85f);
            ColShapeIn.OnEntityEnterColShape += (s, player) =>
            {
                if (player.HasData("BuildingEnterTimer"))
                {
                    DateTime time = player.GetData("BuildingEnterTimer");
                    if (DateTime.Now.Subtract(time).Seconds < 3)
                        return;
                }

                RemovePlayer(player);
                player.SetData("BuildingEnterTimer", DateTime.Now);
            };

            ColShapeOut = NAPI.ColShape.CreateCylinderColShape(OutsidePosition, 0.85f, 0.85f);
            ColShapeOut.OnEntityEnterColShape += (s, player) =>
            {
                if (InsidePosition == new Vector3())
                {
                    player.SendChatMessage("~r~ERROR: ~w~Esta puerta esta cerrada.");
                    return;
                }
                else if (player.HasData("BuildingEnterTimer"))
                {
                    DateTime time = player.GetData("BuildingEnterTimer");
                    if (DateTime.Now.Subtract(time).Seconds < 3)
                        return;
                }

                SendPlayer(player);
                player.SetData("BuildingEnterTimer", DateTime.Now);
            };

            // create text label
            LabelIn = NAPI.TextLabel.CreateTextLabel(Name, InsidePosition, 15f, 0.65f, 1, new Color(255, 255, 255, 255));
            LabelOut = NAPI.TextLabel.CreateTextLabel(Name, OutsidePosition, 15f, 0.65f, 1, new Color(255, 255, 255, 255));

            SetDimension(dimension);
        }

        private void UpdateBlip()
        {
            int count = PlayersInside.Count;
            Blip.SetSharedData("PlayersInside", count);

            /*
             * CRASHING SERVER
             * 
             * if (count < 1) NAPI.Native.SendNativeToAllPlayers(Hash.HIDE_NUMBER_ON_BLIP, Blip.Handle);
             * else NAPI.Native.SendNativeToAllPlayers(Hash.SHOW_NUMBER_ON_BLIP, Blip.Handle, count);
             */
        }

        public void SetName(string new_name)
        {
            Name = new_name;
            LabelIn.Text = new_name;
            LabelOut.Text = new_name;

            Save();
        }

        public void SetBlipSprite(uint new_sprite)
        {
            BlipSprite = new_sprite;
            Blip.Sprite = new_sprite;

            Save();
        }

        public void SetInsideMarker(Vector3 position)
        {
            InsidePosition = position;
            LabelIn.Position = position;
            MarkerIn.Position = position - new Vector3(0.0, 0.0, 1.0);

            NAPI.ColShape.DeleteColShape(ColShapeIn);
            ColShapeIn = NAPI.ColShape.CreateCylinderColShape(InsidePosition, 0.85f, 0.85f);
            ColShapeIn.Dimension = Dimension;
            ColShapeIn.OnEntityEnterColShape += (s, player) =>
            {
                if (player.HasData("BuildingEnterTimer"))
                {
                    DateTime time = player.GetData("BuildingEnterTimer");
                    if (DateTime.Now.Subtract(time).Seconds < 3)
                        return;
                }

                RemovePlayer(player);
                player.SetData("BuildingEnterTimer", DateTime.Now);
            };
        }

        public void SetOutsideMarker(Vector3 position)
        {
            OutsidePosition = position;
            LabelOut.Position = position;
            Blip.Position = position;
            MarkerOut.Position = position - new Vector3(0.0, 0.0, 1.0);

            NAPI.ColShape.DeleteColShape(ColShapeOut);
            ColShapeOut = NAPI.ColShape.CreateCylinderColShape(OutsidePosition, 0.85f, 0.85f);
            ColShapeOut.OnEntityEnterColShape += (s, player) =>
            {
                if (player.HasData("BuildingEnterTimer"))
                {
                    DateTime time = player.GetData("BuildingEnterTimer");
                    if (DateTime.Now.Subtract(time).Seconds < 3)
                        return;
                }

                SendPlayer(player);
                player.SetData("BuildingEnterTimer", DateTime.Now);
            };
        }

        public void SendPlayer(Client player)
        {
            player.Dimension = Dimension;
            player.Position = InsidePosition;
            player.SetData("InsideBuilding_ID", ID);

            if (!PlayersInside.Contains(player.Handle)) PlayersInside.Add(player.Handle);
            UpdateBlip();
        }

        public void RemovePlayer(Client player, bool set_pos = true)
        {
            if (set_pos)
            {
                player.Position = OutsidePosition;
                player.Dimension = 0;
            }

            player.ResetData("InsideBuilding_ID");

            if (PlayersInside.Contains(player.Handle)) PlayersInside.Remove(player.Handle);
            UpdateBlip();
        }

        public void RemoveAllPlayers(bool exit = false)
        {
            for (int i = PlayersInside.Count - 1; i >= 0; i--)
            {
                Client player = NAPI.Entity.GetEntityFromHandle<Client>(PlayersInside[i]);

                if (player != null)
                {
                    player.Position = OutsidePosition;
                    player.Dimension = 0;

                    player.ResetData("InsideBuilding_ID");
                }

                PlayersInside.RemoveAt(i);
            }

            if (!exit) UpdateBlip();
        }

        public void SetDimension(uint dimension)
        {
            Dimension = dimension;
            MarkerIn.Dimension = dimension;
            ColShapeIn.Dimension = dimension;
            LabelIn.Dimension = dimension;

            Save();
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.BUILDING_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }

        public void Destroy(bool exit = false)
        {
            RemoveAllPlayers(exit);

            Blip.Delete();
            MarkerIn.Delete();
            MarkerOut.Delete();
            NAPI.ColShape.DeleteColShape(ColShapeIn);
            NAPI.ColShape.DeleteColShape(ColShapeOut);
            LabelIn.Delete();
            LabelOut.Delete();
        }
    }
    #endregion
}
