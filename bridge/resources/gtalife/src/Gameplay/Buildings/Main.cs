﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace gtalife.src.Gameplay.Buildings
{
    public class Main : Script
    {
        // settings
        public static string BUILDING_SAVE_DIR = "data/Building";
        public static int SAVE_INTERVAL = 120;

        public static List<Building> Buildings = new List<Building>();

        #region Methods
        public static Guid GetGuid()
        {
            Guid new_guid;

            do
            {
                new_guid = Guid.NewGuid();
            } while (Buildings.Count(h => h.ID == new_guid) > 0);

            return new_guid;
        }

        public void RemovePlayerFromBuildingList(Client player)
        {
            if (player.HasData("InsideBuilding_ID"))
            {
                Building building = Buildings.FirstOrDefault(h => h.ID == player.GetData("InsideBuilding_ID"));
                if (building == null) return;

                building.RemovePlayer(player, false);
            }
        }
        #endregion

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // load settings
            if (NAPI.Resource.HasSetting(this, "buildingDirName")) BUILDING_SAVE_DIR = NAPI.Resource.GetSetting<string>(this, "buildingDirName");

            BUILDING_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + BUILDING_SAVE_DIR;
            if (!Directory.Exists(BUILDING_SAVE_DIR)) Directory.CreateDirectory(BUILDING_SAVE_DIR);

            if (NAPI.Resource.HasSetting(this, "saveInterval")) SAVE_INTERVAL = NAPI.Resource.GetSetting<int>(this, "saveInterval");

            // load buildings
            foreach (string file in Directory.EnumerateFiles(BUILDING_SAVE_DIR, "*.json"))
            {
                Building building = JsonConvert.DeserializeObject<Building>(File.ReadAllText(file));
                Buildings.Add(building);
            }

            NAPI.Util.ConsoleOutput("Loaded {0} buildings.", Buildings.Count);
        }

        [ServerEvent(Event.PlayerDeath)]
        public void OnPlayerDeath(Client player, Client killer, uint reason)
        {
            player.Dimension = 0;
            RemovePlayerFromBuildingList(player);
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            RemovePlayerFromBuildingList(player);
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (Building building in Buildings)
            {
                building.Save(true);
                building.Destroy(true);
            }

            Buildings.Clear();
        }
        #endregion
    }
}
