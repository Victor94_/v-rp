﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var business_text_mode = 0;

var business_purchase_menu = null;
var business_menu = null;
var business_safe_menu = null;

var business_menus = [];

function b_numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function b_hideMenus() {
    for (var i = 0; i < business_menus.length; i++) business_menus[i].Visible = false;
}

API.onServerEventTrigger.connect(function (event_name, args) {
    switch (event_name) {
        case "ShowBusinessText":
            business_text_mode = args[0];
            if (business_text_mode == 0) b_hideMenus();
            break;

        case "Business_PurchaseMenu":
            var data = JSON.parse(args[0]);

            if (business_purchase_menu == null) {
                business_purchase_menu = API.createMenu("Negocio", "~g~¡Este negocio esta en venta!", 0, 0, 6);
                business_menus.push(business_purchase_menu);

                business_purchase_menu.OnItemSelect.connect(function (menu, item, index) {
                    if (index == 2) API.triggerServerEvent("BusinessPurchase");
                });
            }

            business_purchase_menu.Clear();

            var temp_item = API.createMenuItem("Tipo", "El tipo de lo negocio.");
            temp_item.SetRightLabel(data.Type);
            business_purchase_menu.AddItem(temp_item);

            temp_item = API.createMenuItem("Precio", "Precio del negocio.");
            temp_item.SetRightLabel("$" + b_numberWithCommas(data.Price));
            business_purchase_menu.AddItem(temp_item);

            temp_item = API.createColoredItem("Comprar", "Seleccione esta opción para comprar el negocio.", "#4caf50", "#ffffff");
            business_purchase_menu.AddItem(temp_item);

            business_purchase_menu.Visible = true;
            break;

        case "BusinessMenu":
            var data = JSON.parse(args[0]);

            if (business_menu == null) {
                business_menu = API.createMenu("Negocio", "~b~Controles del negocio", 0, 0, 6);
                business_menus.push(business_menu);

                business_menu.OnCheckboxChange.connect(function (menu, item, checked) {
                    API.triggerServerEvent("BusinessSetLock", checked);
                });

                business_menu.OnItemSelect.connect(function (menu, item, index) {
                    switch (index) {
                        case 0:
                            var name = API.getUserInput("", 31);
                            if (name.length > 0) API.triggerServerEvent("BusinessSetName", name);
                            break;

                        case 2:
                            API.triggerServerEvent("BusinessSell");
                            break;
                    }
                });
            }

            if (business_safe_menu == null) {
                business_safe_menu = API.createMenu("Casa", "~b~Caja fuerte", 0, 0, 6);
                business_menus.push(business_safe_menu);

                business_safe_menu.ParentMenu = business_menu;

                var temp_safe_item = API.createMenuItem("Depositar dinero", "Depositar dinero en la caja fuerte.");
                business_safe_menu.AddItem(temp_safe_item);

                temp_safe_item = API.createMenuItem("Sacar dinero", "Sacar dinero de la caja fuerte.");
                business_safe_menu.AddItem(temp_safe_item);

                business_safe_menu.OnItemSelect.connect(function (menu, item, index) {
                    var amount = API.getUserInput("", 9);
                    if (parseInt(amount) > 1) API.triggerServerEvent("BusinessSafe", index, amount);
                });
            }

            // business main menu
            business_menu.Clear();

            var temp_item = API.createMenuItem("Nombre del negocio", "Cambia el nombre de el negocio.");
            business_menu.AddItem(temp_item);

            /*temp_item = API.createCheckboxItem("Lock", "Permite/bloquee lo uso de otros jugadores a su negocio.", data.Locked);
            business_menu.AddItem(temp_item);*/

            temp_item = API.createMenuItem("Caja fuerte", "Opciones para la caja fuerta.");
            business_menu.AddItem(temp_item);
            business_menu.BindMenuToItem(business_safe_menu, temp_item);

            temp_item = API.createColoredItem("Vender negocio", "Vende tu negocio.", "#4caf50", "#ffffff");
            business_menu.AddItem(temp_item);

            // business safe menu
            API.setMenuSubtitle(business_safe_menu, "~b~Caja fuerte ~g~($" + b_numberWithCommas(data.Money) + ")");

            business_menu.Visible = true;
            break;

        case "BusinessUpdateSafe":
            if (business_safe_menu != null) {
                var data = JSON.parse(args[0]);
                API.setMenuSubtitle(business_safe_menu, "~b~Caja fuerte ~g~($" + b_numberWithCommas(data.Money) + ")");
            }
            break;

        case "UpdateBusinessBlip":
            API.setBlipColor(args[0], 69);
            API.setBlipShortRange(args[0], false);
            break;

        case "ResetBusinessBlip":
            API.setBlipColor(args[0], 0);
            API.setBlipShortRange(args[0], true);
            break;
    }
});

API.onKeyDown.connect(function (e, key) {
    if (API.isChatOpen()) return;

    switch (key.KeyCode) {
        case Keys.E:
            if (business_text_mode == 1) {
                API.triggerServerEvent("BusinessInteract");
            } else if (business_text_mode == 2) {
                API.triggerServerEvent("BusinessLeave");
            }
            break;

        case Keys.M:
            b_hideMenus();
            API.triggerServerEvent("BusinessMenu");
            break;
    }
});

API.onUpdate.connect(function () {
    if (business_text_mode > 0) API.displaySubtitle(((business_text_mode == 1) ? "Presione ~y~E ~w~para interactuar con el negocio." : "Presione ~y~E ~w~para interactuar."), 100);
});