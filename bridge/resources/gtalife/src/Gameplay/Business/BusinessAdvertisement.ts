﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var business_menu = null;

API.onServerEventTrigger.connect(function (event_name, args) {
    switch (event_name) {
        case "BusinessAd":
            var message = API.getUserInput("", 128);
            if (message.length > 0) API.triggerServerEvent("BussinessSendAd", message);
            break;
    }
});
