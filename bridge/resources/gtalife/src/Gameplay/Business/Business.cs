﻿using GTANetworkAPI;
using gtalife.src.Player.Utils;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using gtalife.src.Managers;
using DSharpPlus.Entities;
using System.Timers;

namespace gtalife.src.Gameplay.Business
{
    #region Business Class
    public class Business
    {
        public Guid ID { get; }
        public string Owner { get; private set; }
        public int Type { get; private set; }
        public Vector3 Position { get; }
        public int Price { get; private set; }
        public bool Locked { get; private set; }
        public Vector3 VehicleSpawn { get; private set; }
        public uint Dimension { get; set; }

        // customization
        public string Name { get; private set; }

        // storage
        public long Money { get; private set; }
        public List<BusinessVehicle> Vehicles { get; }

        // entities
        [JsonIgnore]
        private Blip Blip;

        [JsonIgnore]
        private Marker Marker;

        [JsonIgnore]
        private ColShape ColShape;

        [JsonIgnore]
        private TextLabel Label;

        [JsonIgnore]
        private DateTime LastSave;

        public Business(Guid id, string owner, int type, Vector3 position, int price, bool locked, uint dimension = 0, string name = "", long money = 0, Vector3 vehiclespawn = null, List < BusinessVehicle> vehicles = null)
        {
            ID = id;
            Owner = owner;
            Type = type;
            Position = position;
            Price = price;
            Locked = locked;
            Dimension = dimension;

            Name = name;

            Money = money;
            Vehicles = vehicles ?? new List<BusinessVehicle>();
            VehicleSpawn = vehiclespawn ?? new Vector3(0f, 0f, 0f);

            // create blip
            Blip = NAPI.Blip.CreateBlip(position);            
            Blip.Scale = 1f;
            Blip.ShortRange = true;
            UpdateBlip();

            // create marker
            Marker = NAPI.Marker.CreateMarker(1, position - new Vector3(0.0, 0.0, 1.0), new Vector3(), new Vector3(), 1, new Color(150, 64, 196, 255), dimension: dimension);

            // create colshape
            ColShape = NAPI.ColShape.CreateCylinderColShape(position, 0.85f, 0.85f);
            ColShape.Dimension = dimension;
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.SetData("BusinessMarker_ID", ID);
                    player.TriggerEvent("ShowBusinessText", 1);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.ResetData("BusinessMarker_ID");
                    player.TriggerEvent("ShowBusinessText", 0);
                }
            };

            // create text label
            Label = NAPI.TextLabel.CreateTextLabel("Negocio", position, 15f, 0.65f, 1, new Color(255, 255, 255, 255));
            UpdateLabel();
        }

        private void UpdateLabel()
        {
            if (string.IsNullOrEmpty(Owner))
            {
                Label.Text = string.Format("~b~Negocio en venta~n~~n~~w~Tipo: ~b~{0}~n~~w~Precio: ~b~${1:n0}", BusinessTypes.BusinessTypeList[Type].Name, Price);
            }
            else
            {
                Label.Text = string.Format("~b~Negocio~n~~n~~w~{0}~n~~w~Propietario: ~b~{1}~n~~w~Tipo: ~b~{2}", Name, Owner, BusinessTypes.BusinessTypeList[Type].Name);
            }
            Label.Dimension = Dimension;
        }

        private void UpdateBlip()
        {
            switch (Type)
            {
                case 0:
                    Blip.Sprite = 52;
                    break;
                case 1:
                    Blip.Sprite = 361;
                    break;
                case 2:
                    Blip.Sprite = 73;
                    break;
                case 3:
                    Blip.Sprite = 100;
                    break;
                case 4:
                    Blip.Sprite = 93;
                    break;
                case 5:
                    Blip.Sprite = 133;
                    break;
                case 6:
                    Blip.Sprite = 71;
                    break;
                case 7:
                    Blip.Sprite = 362;
                    break;
                default:
                    Blip.Sprite = 475;
                    break;
            }
            Blip.Color = string.IsNullOrEmpty(Owner) ? 69 : 26;
            Blip.Name = string.IsNullOrEmpty(Owner) ? "Negocio en venta" : "Negocio del jugador";
            Blip.Dimension = Dimension;
        }

        public void SetSpawn(Vector3 spawn)
        {
            VehicleSpawn = spawn;

            Save();
        }

        public void SetOwner(Client player)
        {
            Owner = (player == null) ? string.Empty : player.Name;

            Blip.Color = (player == null) ? 69 : 26;
            Blip.ShortRange = true;

            UpdateLabel();
            Save();
        }

        public void SetName(string new_name)
        {
            Name = new_name;

            UpdateLabel();
            Save();
        }

        public void SetLock(bool locked)
        {
            Locked = locked;

            UpdateLabel();
            Save();
        }

        public void SetType(int new_type)
        {
            Type = new_type;

            UpdateLabel();
            UpdateBlip();
            Save();
        }

        public void SetPrice(int new_price)
        {
            Price = new_price;

            UpdateLabel();
            Save();
        }

        public void GiveMoney(int amount)
        {
            Money += amount;

            Save();
        }

        public void SetMoney(int amount)
        {
            Money = amount;

            Save();
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.BUSINESS_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }

        public void Destroy(bool exit = false)
        {
            Blip.Delete();
            Marker.Delete();
            NAPI.ColShape.DeleteColShape(ColShape);
            Label.Delete();
        }

        internal void SetSpawn(object position)
        {
            throw new NotImplementedException();
        }
    }
    #endregion

    public class Main : Script
    {
        // settings
        public static string BUSINESS_SAVE_DIR = "data/BusinessData";
        public static int PLAYER_BUSINESS_LIMIT = 3;
        public static int BUSINESS_MONEY_LIMIT = 5000000;
        public static int SAVE_INTERVAL = 120;

        public static List<Business> Businesses = new List<Business>();

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (player.HasData("RENTED_VEHICLE"))
            {
                if (NAPI.Entity.DoesEntityExist(player.GetData("RENTED_VEHICLE")))
                    NAPI.Entity.DeleteEntity(player.GetData("RENTED_VEHICLE"));
            }

            if (player.HasData("RENTED_TIMER"))
            {
                player.GetData("RENTED_TIMER").Dispose();
                player.ResetData("RENTED_TIMER");
            }
        }

        [ServerEvent(Event.PlayerEnterVehicle)]
        public void OnPlayerEnterVehicle(Client player, Vehicle vehicle, sbyte seatID)
        {
            foreach (var business in Businesses)
            {
                foreach (var businessVehicle in business.Vehicles)
                {
                    if (vehicle.Handle == businessVehicle.Vehicle.Handle)
                    {
                        player.TriggerEvent("BusinessRent_ShowText", 1);
                        player.SetData("Business_Vehicle", businessVehicle);
                    }
                }
            }
        }

        [ServerEvent(Event.PlayerExitVehicle)]
        public void OnPlayerExitVehicle(Client player, Vehicle vehicle)
        {
            if (player.HasData("Business_Vehicle"))
            {
                player.TriggerEvent("BusinessRent_ShowText", 0);
                player.ResetData("Business_Vehicle");
            }
        }

        #region Methods
        public static Guid GetGuid()
        {
            Guid new_guid;

            do
            {
                new_guid = Guid.NewGuid();
            } while (Businesses.Count(h => h.ID == new_guid) > 0);

            return new_guid;
        }

        public static bool IsABusinessVehicle(NetHandle vehicle)
        {
            foreach (Business business in Businesses)
            {
                foreach (BusinessVehicle v in business.Vehicles)
                {
                    if (vehicle == v.Vehicle)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {            
            // load settings
            if (NAPI.Resource.HasSetting(this, "businessDirName")) BUSINESS_SAVE_DIR = NAPI.Resource.GetSetting<string>(this, "businessDirName");

            BUSINESS_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + BUSINESS_SAVE_DIR;
            if (!Directory.Exists(BUSINESS_SAVE_DIR)) Directory.CreateDirectory(BUSINESS_SAVE_DIR);

            if (NAPI.Resource.HasSetting(this, "playerBusinessLimit")) PLAYER_BUSINESS_LIMIT = NAPI.Resource.GetSetting<int>(this, "playerBusinessLimit");
            if (NAPI.Resource.HasSetting(this, "businessMoneyLimit")) BUSINESS_MONEY_LIMIT = NAPI.Resource.GetSetting<int>(this, "businessMoneyLimit");
            if (NAPI.Resource.HasSetting(this, "saveInterval")) SAVE_INTERVAL = NAPI.Resource.GetSetting<int>(this, "saveInterval");

            NAPI.Util.ConsoleOutput("-> Player Business Limit: {0}", ((PLAYER_BUSINESS_LIMIT == 0) ? "Disabled" : PLAYER_BUSINESS_LIMIT.ToString()));
            NAPI.Util.ConsoleOutput("-> Business Safe Limit: ${0:n0}", BUSINESS_MONEY_LIMIT);

            // load businesses
            foreach (string file in Directory.EnumerateFiles(BUSINESS_SAVE_DIR, "*.json"))
            {
                Business business = JsonConvert.DeserializeObject<Business>(File.ReadAllText(file));

                Businesses.Add(business);

                // vehicles
                foreach (BusinessVehicle vehicle in business.Vehicles) vehicle.Create();
            }

            NAPI.Util.ConsoleOutput("Loaded {0} businesses.", Businesses.Count);
        }

        [RemoteEvent("BusinessInteract")]
        public void BusinessInteract(Client player, object[] arguments)
        {
            if (!player.HasData("BusinessMarker_ID")) return;

            Business business = Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
            if (business == null) return;

            if (business.Type == 0)
            {
                // 24-7 business
                if (string.IsNullOrEmpty(business.Owner)) player.SendChatMessage("~y~INFO: ~w~Este negocio está a la venta, presione ~y~M~w~ para interactuar.");
                player.TriggerEvent("BusinessStore", NAPI.Util.ToJson(business));
            }
            else if (business.Type == 2)
            {
                // Clothing business
                if (string.IsNullOrEmpty(business.Owner)) player.SendChatMessage("~y~INFO: ~w~Este negocio está a la venta, presione ~y~M~w~ para interactuar.");

                player.TriggerEvent("BusinessClothes", player.getPlayerGenderName());
            }
            else if (business.Type == 4)
            {
                // Bar business
                if (string.IsNullOrEmpty(business.Owner)) player.SendChatMessage("~y~INFO: ~w~Este negocio está a la venta, presione ~y~M~w~ para interactuar.");

                player.TriggerEvent("BusinessBar", NAPI.Util.ToJson(business));
            }
            else if (business.Type == 5)
            {
                // Ad business
                if (player.getPhoneNumber() == null)
                {
                    player.SendChatMessage("~r~ERROR: ~w~Necesita un teléfono para enviar un anuncio.");
                    return;
                }

                if (string.IsNullOrEmpty(business.Owner)) player.SendChatMessage("~y~INFO: ~w~Este negocio está a la venta, presione ~y~M~w~ para interactuar.");

                player.TriggerEvent("BusinessAd", NAPI.Util.ToJson(business));
            }
            else if (business.Type == 6)
            {
                // Hairdresser business
                if (string.IsNullOrEmpty(business.Owner)) player.SendChatMessage("~y~INFO: ~w~Este negocio está a la venta, presione ~y~M~w~ para interactuar.");

                player.TriggerEvent("BusinessHair", NAPI.Util.ToJson(business));
            }
            else if (business.Type == 7)
            {
                // Surgeon business
                if (string.IsNullOrEmpty(business.Owner)) player.SendChatMessage("~y~INFO: ~w~Este negocio está a la venta, presione ~y~M~w~ para interactuar.");

                // ???
                uint dimension = DimensionManager.RequestPrivateDimension(player);
                player.Dimension = dimension;

                System.Threading.Tasks.Task.Run(() =>
                {
                    NAPI.Task.Run(() =>
                    {
                        player.SetData("BusinessMarker_ID", business.ID);
                    }, delayTime: 1000);
                });

                player.TriggerEvent("BusinessSurgeon", NAPI.Util.ToJson(business));
            }
            else if (string.IsNullOrEmpty(business.Owner))
            {
                // not owned business
                player.TriggerEvent("Business_PurchaseMenu", NAPI.Util.ToJson(new { Type = BusinessTypes.BusinessTypeList[business.Type].Name, business.Price }));
            }
            else
            {
                // owned business
                if (business.Owner == player.Name)
                {
                    player.TriggerEvent("BusinessMenu", NAPI.Util.ToJson(business));
                }
                else
                {
                    player.SendNotification("~r~Solo el propietario puede acceder a este negocio.");
                }
            }
        }

        [RemoteEvent("BusinessPurchase")]
        public void BusinessPurchase(Client player, object[] arguments)
        {
            if (!player.HasData("BusinessMarker_ID")) return;

            Business business = Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));

            if (business == null) return;
            else if (!string.IsNullOrEmpty(business.Owner)) player.SendNotification("~r~Esta negocio tiene dueño.");
            else if (business.Price > player.getMoney()) player.SendNotification("~r~No puedes pagar por este negocio.");
            else if (PLAYER_BUSINESS_LIMIT > 0 && Businesses.Count(h => h.Owner == player.Name) >= PLAYER_BUSINESS_LIMIT) player.SendNotification("~r~No puedes tener más negocios.");
            else
            {
                business.SetLock(true);
                business.SetOwner(player);

                player.giveMoney(-business.Price);
                player.SendNotification("~g~Felicitaciones, compraste este negocio!");
            }
        }

        [RemoteEvent("BusinessMenu")]
        public void BusinessMenu(Client player, object[] arguments)
        {
            if (!player.HasData("BusinessMarker_ID")) return;

            Business business = Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
            if (business == null) return;

            // if it is not owned, show purchase menu
            else if (string.IsNullOrEmpty(business.Owner)) player.TriggerEvent("Business_PurchaseMenu", NAPI.Util.ToJson(new { Type = BusinessTypes.BusinessTypeList[business.Type].Name, business.Price }));
            else if (business.Owner != player.Name) player.SendNotification("~r~Solo el propietario puede acceder al menú de lo negocio.");
            else player.TriggerEvent("BusinessMenu", NAPI.Util.ToJson(business));
        }

        [RemoteEvent("BusinessSetName")]
        public void BusinessSetName(Client player, object[] arguments)
        {
            if (!player.HasData("BusinessMarker_ID") || arguments.Length < 1) return;
            string new_name = arguments[0].ToString();

            Business business = Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
            if (business == null) return;
            else if (business.Owner != player.Name) player.SendNotification("~r~Solo el dueño puede hacer esto.");
            else if (new_name.Length > 32) player.SendNotification("~r~El nombre no puede tener más de 32 caracteres.");
            else
            {
                business.SetName(new_name);
                player.SendNotification(string.Format("~g~El nombre de lo negocio cambió a: ~w~\"{0}\"", new_name));
            }
        }

        [RemoteEvent("BusinessSafe")]
        public void BusinessSafe(Client player, object[] arguments)
        {
            if (!player.HasData("BusinessMarker_ID") || arguments.Length < 2) return;

            int type = Convert.ToInt32(arguments[0]);
            int amount = 0;

            Business business = Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));

            if (business == null) return;
            else if (business.Owner != player.Name) player.SendNotification("~r~Solo el dueño puede hacer esto.");
            else if (!int.TryParse(arguments[1].ToString(), out amount)) player.SendNotification("~r~Monto invalido.");
            else if (amount < 1) player.SendNotification("~r~Monto invalido.");
            else if (type == 0)
            {
                if (player.getMoney() < amount) player.SendNotification("~r~No tienes tanto dinero.");
                else if (business.Money + amount > BUSINESS_MONEY_LIMIT) player.SendNotification("~r~Límite de dinero de lo negocio alcanzado.");
                else
                {
                    business.GiveMoney(amount);
                    player.giveMoney(-amount);

                    player.SendNotification(string.Format("~g~Colocaste ${0:n0} en el cofre.", amount));
                    player.TriggerEvent("BusinessUpdateSafe", NAPI.Util.ToJson(new { business.Money }));
                }
            }
            else
            {
                if (business.Money < amount) player.SendNotification("~r~La caja fuerte no tiene tanto dinero.");
                else
                {
                    player.giveMoney(amount);
                    business.GiveMoney(-amount);

                    player.SendNotification(string.Format("~g~Tomó ${0:n0} de la caja fuerte.", amount));
                    player.TriggerEvent("BusinessUpdateSafe", NAPI.Util.ToJson(new { business.Money }));
                }
            }
        }

        [RemoteEvent("BusinessSell")]
        public void BusinessSell(Client player, object[] arguments)
        {
            if (!player.HasData("BusinessMarker_ID")) return;

            Business business = Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));

            if (business == null) return;
            else if (business.Owner != player.Name) player.SendNotification("~r~Solo el dueño puede hacer esto.");
            else if (business.Money > 0) player.SendNotification("~r~Vacíe lo negocio antes de vender.");
            else
            {
                business.SetOwner(null);

                int price = (int)Math.Round(business.Price * 0.6);
                player.giveMoney(price);
                player.SendNotification(string.Format("~g~Vendió tu negocio por ${0:n0}.", price));
            }
        }

        [RemoteEvent("BusinessBuyItem")]
        public void BusinessBuyItem(Client player, object[] arguments)
        {
            if (!player.HasData("BusinessMarker_ID") || arguments.Length < 1) return;

            var item = NAPI.Util.FromJson((string)arguments[0]);
            int price = (int)item.price;

            Business business = Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
            if (business == null) return;
            else if (player.getMoney() < price) player.SendNotification("~r~No tienes tanto dinero.");
            else
            {
                if (item.name == "Botiquín") player.giveItem(ItemID.FirstAidKit_Tier1, 1);
                else if (item.name == "Bidón") player.giveItem(ItemID.Jerrycan, 1);
                else if (item.name == "Radio") player.giveItem(ItemID.Boombox, 1);
                else if (item.name == "Teléfono")
                {
                    Random rand = new Random();
                    int phone = rand.Next(90000) + 10000;

                    player.setPhoneNumber(phone);
                    player.SendChatMessage($"~y~INFO: ~w~Su número de teléfono es ~y~{phone}~w~. (/ayudatelefono)");
                }

                player.SendChatMessage($"~g~ÉXITO: ~w~Compraste un ~g~{item.name} ~w~por ~g~${price}~w~.");

                business.GiveMoney(price);
                player.giveMoney(-price);

                player.TriggerEvent("BusinessUpdateSafe", NAPI.Util.ToJson(new { business.Money }));
            }
        }

        [RemoteEvent("BusinessBuyDrink")]
        public void BusinessBuyDrink(Client player, object[] arguments)
        {
            if (!player.HasData("BusinessMarker_ID") || arguments.Length < 1) return;

            var item = NAPI.Util.FromJson((string)arguments[0]);
            int price = (int)item.price;

            Business business = Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
            if (business == null) return;
            else if (player.getMoney() < price) player.SendNotification("~r~No tienes tanto dinero.");
            else
            {
                if (item.name == "Agua") player.giveItem(ItemID.Water, 1);
                else if (item.name == "Cerveza") player.giveItem(ItemID.Beer, 1);
                else if (item.name == "Whisky") player.giveItem(ItemID.Whisky, 1);
                else if (item.name == "Vodka") player.giveItem(ItemID.Vodka, 1);

                player.SendChatMessage($"~g~ÉXITO: ~w~Compraste un ~g~{item.name} ~w~por ~g~${price}~w~.");

                business.GiveMoney(price);
                player.giveMoney(-price);

                player.TriggerEvent("BusinessUpdateSafe", NAPI.Util.ToJson(new { business.Money }));
            }
        }

        [RemoteEvent("BusinessRentVehicle")]
        public void BusinessRentVehicle(Client player, object[] arguments)
        {
            if (!player.HasData("Business_Vehicle")) return;

            BusinessVehicle vehicle = player.GetData("Business_Vehicle");
            Business business = Businesses.FirstOrDefault(x => x.Vehicles.Contains(vehicle));

            if (business == null) return;

            if (vehicle.Price > player.getMoney())
            {
                player.SendNotification("~r~No tienes tanto dinero.");
                return;
            }
            else if (business.VehicleSpawn.X == 0.0 && business.VehicleSpawn.Y == 0.0)
            {
                player.SendNotification("~r~Estamos cerrados.");
                return;
            }

            if (player.HasData("RENTED_VEHICLE"))
            {
                if (NAPI.Entity.DoesEntityExist(player.GetData("RENTED_VEHICLE")))
                    NAPI.Entity.DeleteEntity(player.GetData("RENTED_VEHICLE"));
            }

            if (player.HasData("RENTED_TIMER"))
            {
                player.GetData("RENTED_TIMER").Dispose();
                player.ResetData("RENTED_TIMER");
            }

            player.SendChatMessage("~y~INFO: ~w~Usted ha alquilado un vehículo, puede usarlo durante 20 minutos.");

            var veh = VehicleManager.CreateVehicle((VehicleHash)vehicle.Model, new Vector3(business.VehicleSpawn.X, business.VehicleSpawn.Y, business.VehicleSpawn.Z), new Vector3(0.0, 0.0, 0.0), vehicle.PrimaryColor, vehicle.SecondaryColor);
            veh.EngineStatus = false;

            player.Position = new Vector3(player.Position.X, player.Position.Y, player.Position.Z + 0.5f);
            System.Threading.Tasks.Task.Run(() =>
            {
                NAPI.Task.Run(() =>
                {
                    NAPI.Player.SetPlayerIntoVehicle(player, veh, -1);
                }, delayTime: 500);
            });

            player.SetData("RENTED_VEHICLE", veh);
            player.TriggerEvent("BusinessRent_ShowText", 0);

            var timer = new Timer();
            timer.Elapsed += (sender, e) => OnRentTimerUpdate(sender, e, player);
            timer.Interval = 1200000;
            timer.Enabled = true;
            player.SetData("RENTED_TIMER", timer);

            player.giveMoney(-vehicle.Price);
            business.GiveMoney(vehicle.Price);
        }

        [RemoteEvent("BussinessSendAd")]
        public void BussinessSendAd(Client player, object[] arguments)
        {
            if (!player.HasData("BusinessMarker_ID") || arguments.Length < 1) return;

            Business business = Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
            if (business == null) return;

            string message = (string)arguments[0];
            int price = message.Length * 4;

            if (price > player.getMoney()) player.SendChatMessage($"~r~ERROR: ~w~No tienes suficiente dinero. (( ${price} ))");
            else if (message.Length < 8) player.SendChatMessage("~r~ERROR: ~w~Mensaje demasiado corto.");
            else if (!player.HasData("ADVERTISEMENT_COOLDOWN") || (DateTime)player.GetData("ADVERTISEMENT_COOLDOWN") < DateTime.Now)
            {
                NAPI.Chat.SendChatMessageToAll($"~g~ANUNCIO: {message} - Teléfono: {player.getPhoneNumber()}");

                player.giveMoney(-price);
                business.GiveMoney(price);

                player.SetData("ADVERTISEMENT_COOLDOWN", DateTime.Now.AddMinutes(2));

                // Discord bot message
                var embed = new DiscordEmbedBuilder
                {
                    Title = "Anuncio",
                    Description = message,
                    ThumbnailUrl = "https://cdn.discordapp.com/attachments/389490689265893377/448200319604883467/megaphone.png",
                    Author = new DiscordEmbedBuilder.EmbedAuthor { Name = business.Name, IconUrl = "https://cdn.discordapp.com/attachments/390881942649962505/448191766030712844/vv.png" },
                    Footer = new DiscordEmbedBuilder.EmbedFooter { Text = $"Enviado por {player.Name} - Teléfono: {player.getPhoneNumber()}" },
                    Color = new DiscordColor(0xFFFF00)
                };
                Discord.Main.SendDiscordBotMessage(Discord.Main.AdvertisementChannelId, "", embed: embed);
            }
            else
            {
                DateTime cooldown = (DateTime)player.GetData("ADVERTISEMENT_COOLDOWN");
                player.SendChatMessage($"~r~ERROR: ~w~Debes esperar {(cooldown - DateTime.Now).Minutes} minutos y {(cooldown - DateTime.Now).Seconds} segundos para anunciar de nuevo.");
            }
        }

        public void Business_ClientEvent(Client player, string event_name, params object[] args)
        {
            switch (event_name)
            {
                case "BusinessBuyClothes":
                    {
                        if (player.getMoney() < 100)
                        {
                            player.loadClothes();
                            player.SendChatMessage("~r~ERROR: ~w~No tienes suficiente dinero.");
                            return;
                        }

                        var torso = NAPI.Util.FromJson((string)args[0]);
                        var legs = NAPI.Util.FromJson((string)args[1]);
                        var feet = NAPI.Util.FromJson((string)args[2]);
                        var accessories = NAPI.Util.FromJson((string)args[3]);
                        var undershirt = NAPI.Util.FromJson((string)args[4]);
                        var tops = NAPI.Util.FromJson((string)args[5]);

                        foreach (var clothes in Player.Data.Character[player].Clothes)
                        {
                            if (clothes.Slot == 3 && !clothes.IsProp && torso.variation != null)
                            {
                                clothes.Drawable = torso.variation;
                            }
                            else if (clothes.Slot == 4 && !clothes.IsProp && legs.variation != null)
                            {
                                clothes.Drawable = legs.variation;
                            }
                            else if (clothes.Slot == 6 && !clothes.IsProp && feet.variation != null)
                            {
                                clothes.Drawable = feet.variation;
                            }
                            else if (clothes.Slot == 7 && !clothes.IsProp && accessories.variation != null)
                            {
                                clothes.Drawable = accessories.variation;
                            }
                            else if (clothes.Slot == 8 && !clothes.IsProp && undershirt.variation != null)
                            {
                                clothes.Drawable = undershirt.variation;
                            }
                            else if (clothes.Slot == 11 && !clothes.IsProp && tops.variation != null)
                            {
                                clothes.Drawable = tops.variation;
                                clothes.Texture = tops.texture;
                            }
                        }

                        player.loadClothes();
                        player.giveMoney(-100);
                        player.SendChatMessage("~g~ÉXITO: ~w~Has comprado nueva ropa.");
                    }
                    break;
                case "CloseBusinessClothesMenu":
                    {
                        player.loadClothes();
                        break;
                    }
                case "BusinessPurchaseHaircut":
                    {
                        if (!player.HasData("BusinessMarker_ID") || args.Length < 2) return;

                        Business business = Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
                        if (business == null) return;

                        int hairstyle = (int)args[0];
                        int haircolor = (int)args[1];
                        int price = 100;

                        if (player.getMoney() < price) player.SendChatMessage($"~r~ERROR: ~w~No tienes suficiente dinero. (( ${price} ))");
                        else
                        {
                            if (hairstyle > -1) Player.Data.Character[player].Trait.HairType = hairstyle;
                            if (haircolor > -1) Player.Data.Character[player].Trait.HairColor = haircolor;

                            Player.Customization.CustomizationModel Customization = new Player.Customization.CustomizationModel();
                            Customization.InitializePedFace(player);
                            Customization.UpdatePlayerFace(player);

                            player.giveMoney(-price);
                            business.GiveMoney(price);
                        }
                        break;
                    }
                case "BusinessCloseHaircutMenu":
                    {
                        Player.Customization.CustomizationModel Customization = new Player.Customization.CustomizationModel();
                        Customization.InitializePedFace(player);
                        Customization.UpdatePlayerFace(player);
                        break;
                    }
                case "BusinessPurchaseSurgery":
                    {
                        if (!player.HasData("BusinessMarker_ID") || args.Length < 2) return;

                        Business business = Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
                        if (business == null) return;

                        int face_1 = (int)args[0];
                        int face_2 = (int)args[1];
                        float face_mix = (float)args[2];
                        int price = 10000;

                        if (player.getMoney() < price) player.SendChatMessage($"~r~ERROR: ~w~No tienes suficiente dinero. (( ${price} ))");
                        else
                        {
                            Player.Data.Character[player].Trait.FaceFirst = face_1;
                            Player.Data.Character[player].Trait.FaceSecond = face_2;
                            Player.Data.Character[player].Trait.FaceMix = face_mix;

                            Player.Customization.CustomizationModel Customization = new Player.Customization.CustomizationModel();
                            Customization.InitializePedFace(player);
                            Customization.UpdatePlayerFace(player);

                            player.giveMoney(-price);
                            business.GiveMoney(price);

                            player.Position = business.Position;

                            DimensionManager.DismissPrivateDimension(player);
                            player.Dimension = business.Dimension;
                        }
                        break;
                    }
                case "BusinessCloseSurgeonMenu":
                    {
                        Player.Customization.CustomizationModel Customization = new Player.Customization.CustomizationModel();
                        Customization.InitializePedFace(player);
                        Customization.UpdatePlayerFace(player);

                        Business business = Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
                        if (business == null) return;

                        DimensionManager.DismissPrivateDimension(player);
                        player.Dimension = business.Dimension;
                        player.Position = business.Position;
                        break;
                    }
            }
        }

        private void OnRentTimerUpdate(object sender, ElapsedEventArgs e, Client player)
        {
            if (player.HasData("RENTED_VEHICLE"))
            {
                if (NAPI.Entity.DoesEntityExist(player.GetData("RENTED_VEHICLE")))
                    NAPI.Entity.DeleteEntity(player.GetData("RENTED_VEHICLE"));
            }

            player.GetData("RENTED_TIMER").Dispose();
            player.ResetData("RENTED_TIMER");
            player.SendChatMessage("~y~INFO: ~w~Su tiempo de alquiler ha terminado.");
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (Business business in Businesses)
            {
                business.Save(true);
                business.Destroy(true);
            }

            Businesses.Clear();
        }
        #endregion
    }
}
