"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var male_menu = null;
var female_menu = null;
var components = [];
function initializeComponentsArray() {
    for (let i = 0; i < 12; i++) {
        components[i] = {};
    }
}
API.onServerEventTrigger.connect((event_name, args) => {
    switch (event_name) {
        case "BusinessClothes":
            if (args[0] == "male") {
                if (male_menu == null) {
                    male_menu = API.createMenu(" ", "Seleccione una categoría", 0, 0, 6);
                    API.setMenuBannerSprite(male_menu, "shopui_title_lowendfashion", "shopui_title_lowendfashion");
                    for (let i = 0; i < resource.Clothes.types.length; i++) {
                        var male_category_menu = createClothesCategory(resource.Clothes.types[i], male_menu);
                        for (let j = 0; j < resource.Clothes.components[0][i].length; j++) {
                            createSelectClothesItem(`${resource.Clothes.components[0][i][j].name}`, resource.Clothes.components[0][i][j], male_category_menu);
                        }
                    }
                    var temp_item = API.createColoredItem("Comprar", "Seleccione esta opción para terminar.", "#4caf50", "#ffffff");
                    temp_item.Activated.connect(function (menu, item) {
                        API.triggerServerEvent("BusinessBuyClothes", JSON.stringify(components[3]), JSON.stringify(components[4]), JSON.stringify(components[6]), JSON.stringify(components[7]), JSON.stringify(components[8]), JSON.stringify(components[11]));
                        male_menu.Visible = false;
                    });
                    male_menu.AddItem(temp_item);
                    male_menu.OnMenuClose.connect(function (sender) {
                        API.triggerServerEvent("CloseBusinessClothesMenu");
                    });
                }
                male_menu.Visible = true;
            }
            else {
                if (female_menu == null) {
                    female_menu = API.createMenu(" ", "Seleccione una categoría", 0, 0, 6);
                    API.setMenuBannerSprite(female_menu, "shopui_title_lowendfashion", "shopui_title_lowendfashion");
                    for (let i = 0; i < resource.Clothes.types.length; i++) {
                        var female_category_menu = createClothesCategory(resource.Clothes.types[i], female_menu);
                        for (let j = 0; j < resource.Clothes.components[1][i].length; j++) {
                            createSelectClothesItem(`${j + 1}`, resource.Clothes.components[1][i][j], female_category_menu);
                        }
                    }
                    var temp_item = API.createColoredItem("Comprar", "Seleccione esta opción para terminar.", "#4caf50", "#ffffff");
                    temp_item.Activated.connect(function (menu, item) {
                        API.triggerServerEvent("BusinessBuyClothes", JSON.stringify(components[3]), JSON.stringify(components[4]), JSON.stringify(components[6]), JSON.stringify(components[7]), JSON.stringify(components[8]), JSON.stringify(components[11]));
                        female_menu.Visible = false;
                    });
                    female_menu.AddItem(temp_item);
                    female_menu.OnMenuClose.connect(function (sender) {
                        API.triggerServerEvent("CloseBusinessClothesMenu");
                    });
                }
                female_menu.Visible = true;
            }
            initializeComponentsArray();
            break;
    }
});
function createClothesCategory(name, menu) {
    var clothesCategoryMenu = API.createMenu(name, 0, 0, 6);
    var clothesCategoryItem = API.createMenuItem(name, "");
    API.setMenuBannerSprite(clothesCategoryMenu, "shopui_title_lowendfashion", "shopui_title_lowendfashion");
    menu.AddItem(clothesCategoryItem);
    menu.BindMenuToItem(clothesCategoryMenu, clothesCategoryItem);
    return clothesCategoryMenu;
}
function createSelectClothesItem(name, data, parentMenu) {
    var menuItem = API.createMenuItem(name, "");
    menuItem.Activated.connect(function (menu, item) {
        components[data.slot] = { slot: data.slot, variation: data.variation, torso: data.torso, undershirt: data.undershirt, texture: data.texture };
        if (data.torso != undefined) {
            components[3] = { variation: data.torso };
            API.setPlayerClothes(API.getLocalPlayer(), 3, data.torso, 0);
        }
        if (data.undershirt != undefined) {
            if (components[8].variation == undefined) {
                components[8] = { variation: data.undershirt };
                API.setPlayerClothes(API.getLocalPlayer(), 8, data.undershirt, 0);
            }
        }
        API.setPlayerClothes(API.getLocalPlayer(), data.slot, data.variation, data.texture);
    });
    parentMenu.AddItem(menuItem);
}
