﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var main_menu = null;
var hair_menu = null;
var color_menu = null;

var male_hair_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36];
var female_hair_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38];

var selected_hair = -1;
var selected_color = -1;

API.onServerEventTrigger.connect(function (event_name, args) {
    switch (event_name) {
        case "BusinessHair":
            var player = API.getLocalPlayer();
            if (API.getEntityModel(player) == 1885233650 || API.getEntityModel(player) == -1667301416) {
                if (main_menu == null) {
                    main_menu = API.createMenu("Peluquero", "Seleccione una opcion", 0, 0, 6);
                    main_menu.OnItemSelect.connect(function (menu, item, index) {
                        if (index == 3) {
                            API.triggerServerEvent("BusinessPurchaseHaircut", selected_hair, selected_color);
                            main_menu.Visible = false;
                        }
                    });

                    main_menu.OnMenuClose.connect(function (sender) {
                        API.triggerServerEvent("BusinessCloseHaircutMenu");
                    });

                    // Hairstyle
                    var item = API.createMenuItem("Peinado", "Selecciona un estilo para tu cabello");
                    main_menu.AddItem(item);

                    hair_menu = API.createMenu("Peluquero", "Seleccione un estilo", 0, 0, 6);
                    main_menu.BindMenuToItem(hair_menu, item);

                    hair_menu.OnItemSelect.connect(function (menu, item, index) {
                        selected_hair = (API.getEntityModel(player) == 1885233650 ? male_hair_list[index] : female_hair_list[index]);
                        for (let i = 0; i < (API.getEntityModel(player) == 1885233650 ? male_hair_list.length : female_hair_list.length); i++) hair_menu.MenuItems[i].SetLeftBadge(BadgeStyle.None);
                        hair_menu.MenuItems[index].SetLeftBadge(BadgeStyle.Tick);

                        // Aplly visual
                        API.setPlayerClothes(API.getLocalPlayer(), 2, selected_hair, 0);
                        if (selected_color > -1) API.callNative("_SET_PED_HAIR_COLOR", API.getLocalPlayer(), selected_color, API.getEntitySyncedData(API.getLocalPlayer(), "GTAO_HAIR_HIGHLIGHT_COLOR"));
                    });

                    // Color
                    var item = API.createMenuItem("Color", "Selecciona una color para tu cabello");
                    main_menu.AddItem(item);

                    color_menu = API.createMenu("Peluquero", "Seleccione una color", 0, 0, 6);
                    for (let i = 0; i < 64; i++) {
                        color_menu.AddItem(API.createMenuItem(`${i}`, ""));
                    }
                    main_menu.BindMenuToItem(color_menu, item);

                    color_menu.OnItemSelect.connect(function (menu, item, index) {
                        selected_color = index;
                        for (let i = 0; i < 64; i++) color_menu.MenuItems[i].SetLeftBadge(BadgeStyle.None);
                        color_menu.MenuItems[index].SetLeftBadge(BadgeStyle.Tick);

                        // Aplly visual
                        API.callNative("_SET_PED_HAIR_COLOR", API.getLocalPlayer(), selected_color, API.getEntitySyncedData(API.getLocalPlayer(), "GTAO_HAIR_HIGHLIGHT_COLOR"));
                    });

                    // Price
                    var item = API.createMenuItem("Precio", "Precio del corte");
                    item.SetRightLabel("$100");
                    main_menu.AddItem(item);

                    // Purchase
                    var item_c = API.createColoredItem("Comprar", "Seleccione esta opción para comprar.", "#4caf50", "#ffffff");
                    main_menu.AddItem(item_c);
                }
                hair_menu.Clear();
                for (let i = 0; i < (API.getEntityModel(player) == 1885233650 ? male_hair_list.length : female_hair_list.length); i++) {
                    hair_menu.AddItem(API.createMenuItem(`${i}`, ""));
                }

                selected_hair = -1;
                selected_color = -1;

                main_menu.Visible = !main_menu.Visible;
            }
            else {
                API.SendChatMessage("~r~ERROR: ~w~No puedes cambiar tu cabello.");
            }
            break;
    }
});
