"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var business_menu = null;
var products = [
    { name: "Botiquín", price: "50", description: "Medicamentos para recuperar tu salud." },
    { name: "Teléfono", price: "750", description: "Utilizado para llamar a otros jugadores." },
    { name: "Bidón", price: "125", description: "Un recipiente hermético utilizado para contener, transportar y almacenar líquidos." },
    { name: "Radio", price: "800", description: "Un dispositivo electrónico que recibe ondas de radio y convierte la información que llevan en una forma utilizable." },
];
API.onServerEventTrigger.connect(function (event_name, args) {
    switch (event_name) {
        case "BusinessStore":
            var data = JSON.parse(args[0]);
            if (business_menu == null) {
                business_menu = API.createMenu(" ", "Seleccione un item", 0, 0, 6);
                API.setMenuBannerSprite(business_menu, "shopui_title_conveniencestore", "shopui_title_conveniencestore");
                business_menu.OnItemSelect.connect(function (menu, item, index) {
                    API.triggerServerEvent("BusinessBuyItem", JSON.stringify(products[index]));
                });
            }
            // business main menu
            business_menu.Clear();
            for (var i = 0, len = products.length; i < len; i++) {
                var temp_item = API.createMenuItem(`${products[i].name}`, `${products[i].description}`);
                temp_item.SetRightLabel(`$${products[i].price}`);
                business_menu.AddItem(temp_item);
            }
            business_menu.Visible = true;
            break;
    }
});
