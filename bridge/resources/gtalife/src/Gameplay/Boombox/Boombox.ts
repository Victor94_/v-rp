﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var menu = null;
var boombox = null;

var stations = [
    { genre: "Tech House", url: "https://www.youtube.com/watch?v=sPts9LvPvYg" },
    { genre: "Reggaeton", url: "https://www.youtube.com/watch?v=BtrdX-fxVzA" },
    { genre: "EDM", url: "https://www.youtube.com/watch?v=kcQc1kAjKhA" },
    { genre: "Bassline", url: "https://www.youtube.com/watch?v=39XpEzRu5R0" },
    { genre: "Rock", url: "https://www.youtube.com/watch?v=_uJKeS2rurg" },
    { genre: "Hip-Hop", url: "https://www.youtube.com/watch?v=wpqTrO0TK5U" },
    { genre: "Trap", url: "https://www.youtube.com/watch?v=b_px_Q2Q4_8" }
];

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) => {
    switch (eventName) {
        case "BoomBox:Enter":
            boombox = JSON.parse(args[0]);

            if (boombox.Enabled) resource.Audio.PlayAudioStream(boombox.Url);
            else resource.Audio.StopAudioStream();
            break;
        case "BoomBox:Exit":
            boombox = null;

            resource.Audio.StopAudioStream();
            break;
    }
});

API.onKeyDown.connect((sender: any, e: System.Windows.Forms.KeyEventArgs) => {
    if (API.isChatOpen() || API.isAnyMenuOpen() || boombox == null) return;

    var player = API.getLocalPlayer();

    if (e.KeyCode === Keys.E) {
        if (API.getEntityPosition(player).DistanceTo(new Vector3(boombox.Position.X, boombox.Position.Y, boombox.Position.Z)) < 1) {
            if (menu == null) {
                menu = API.createMenu("Estaciones", "Selecciona una estación", 0, 0, 6);
            }
            menu.Clear();

            for (let i = 0; i < stations.length; i++) {
                var temp_item = API.createMenuItem(stations[i].genre, "");
                menu.AddItem(temp_item);

                temp_item.Activated.connect((menu: NativeUI.UIMenu, selectedItem: NativeUI.UIMenuItem) => {
                    API.triggerServerEvent("BoomBox:Select", stations[i].genre, stations[i].url);
                    menu.Visible = false;
                });
            }

            var temp_item = API.createMenuItem("Link", "");
            menu.AddItem(temp_item);

            temp_item.Activated.connect((menu: NativeUI.UIMenu, selectedItem: NativeUI.UIMenuItem) => {
                var url = API.getUserInput("https://www.youtube.com/watch?v=", 128);
                if (url.length > 0) {
                    API.triggerServerEvent("BoomBox:Select", "Personalizado", url);
                    menu.Visible = false;
                }
            });

            if (boombox.Enabled) {
                var temp_item = API.createMenuItem("Apagar", "");
                menu.AddItem(temp_item);

                temp_item.Activated.connect((menu: NativeUI.UIMenu, selectedItem: NativeUI.UIMenuItem) => {
                    API.triggerServerEvent("BoomBox:TurnOff", boombox.ID);
                    menu.Visible = false;
                });
            }

            if (API.getPlayerName(API.getLocalPlayer()) == boombox.Owner) {
                var temp_item = API.createMenuItem("Tomar", "");
                menu.AddItem(temp_item);

                temp_item.Activated.connect((menu: NativeUI.UIMenu, selectedItem: NativeUI.UIMenuItem) => {
                    API.triggerServerEvent("BoomBox:Take", boombox.ID);
                    menu.Visible = false;
                });
            }

            menu.Visible = true;
        }
    }
});
