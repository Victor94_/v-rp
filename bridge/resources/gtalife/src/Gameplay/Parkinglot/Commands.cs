﻿using GTANetworkAPI;
using gtalife.src.Admin;
using System.IO;
using System.Linq;

namespace gtalife.src.Gameplay.Parkinglot
{
    class Commands : Script
    {
        [Command("parkinglotcmds", Alias = "pcmds")]
        public void CommandsCommand(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no tiene permiso.");
                return;
            }

            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos estacionamiento ~~~~~~~~~~~");
            player.SendChatMessage("* /createparkinglot - /deleteparkinglot - /addparkinglotspawn - /deleteparkinglotspawn");
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos estacionamiento ~~~~~~~~~~~");
        }

        [Command("createparkinglot")]
        public void CMD_CreateParkinglot(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no tiene permiso.");
                return;
            }

            Parkinglot new_parkinglot = new Parkinglot(Main.GetGuid(), player.Position);
            new_parkinglot.Save(true);

            Main.Parkinglots.Add(new_parkinglot);
            NAPI.Chat.SendChatMessageToPlayer(player, $"~g~ÉXITO: ~s~Usted ha creado un estacionamiento.");
        }

        [Command("deleteparkinglot")]
        public void CMD_RemoveParkinglot(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no tiene permiso.");
                return;
            }

            if (!player.HasData("ParkinglotMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Permanezca sobre el checkpoint del estacionamiento que desea excluir.");
                return;
            }

            Parkinglot parkinglot = Main.Parkinglots.FirstOrDefault(h => h.ID == player.GetData("ParkinglotMarker_ID"));
            if (parkinglot == null) return;

            parkinglot.Destroy();
            Main.Parkinglots.Remove(parkinglot);

            string parkinglot_file = Main.PARKINGLOT_SAVE_DIR + Path.DirectorySeparatorChar + parkinglot.ID + ".json";
            if (File.Exists(parkinglot_file)) File.Delete(parkinglot_file);

            NAPI.Chat.SendChatMessageToPlayer(player, $"~g~ÉXITO: ~s~Usted ha borrado el estacionamiento.");
        }

        [Command("addparkinglotspawn")]
        public void CMD_AddParkinglotSpawn(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no tiene permiso.");
                return;
            }

            if (!player.IsInVehicle)
                NAPI.Chat.SendChatMessageToPlayer(player, $"~r~ERROR: ~s~Usted no está en un vehículo.");
            else
            {
                foreach (Parkinglot parkinglot in Main.Parkinglots)
                {
                    if (parkinglot.Position.DistanceTo(player.Position) < 40f)
                    {
                        parkinglot.Spawns.Add(new ParkingSpace { Position = player.Vehicle.Position, Rotation = player.Vehicle.Rotation });
                        parkinglot.Save(true);
                        NAPI.Chat.SendChatMessageToPlayer(player, $"~g~ÉXITO: ~s~Usted ha agregado un spawn en su posición.");
                        return;
                    }
                }

                player.SendNotification($"Usted no está cerca de un estacionamiento.");
            }
        }

        [Command("deleteparkinglotspawn")]
        public void CMD_DeleteParkinglotSpawn(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no tiene permiso.");
                return;
            }

            foreach (Parkinglot parkinglot in Main.Parkinglots)
            {
                if (parkinglot.Position.DistanceTo(player.Position) < 40f)
                {
                    foreach (ParkingSpace space in parkinglot.Spawns)
                    {
                        if (parkinglot.Position.DistanceTo(player.Position) < 2f)
                        {
                            parkinglot.Spawns.Remove(space);
                            NAPI.Chat.SendChatMessageToPlayer(player, $"~g~ÉXITO: ~s~Usted ha quitado lo spawn de su posición.");
                            parkinglot.Save(true);
                            return;
                        }
                    }
                }
            }

            player.SendNotification($"Usted no está cerca de una plaza de estacionamiento.");
        }
    }
}
