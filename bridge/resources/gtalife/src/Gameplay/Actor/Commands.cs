﻿using GTANetworkAPI;
using gtalife.src.Admin;
using System.IO;

namespace gtalife.src.Gameplay.Actor
{
    class Commands : Script
    {
        [Command("npccmds", Alias = "ncmds")]
        public void CommandsCommand(Client player)
        {
            if (player.IsAdmin())
            {
                NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos NPC ~~~~~~~~~~~");
                NAPI.Chat.SendChatMessageToPlayer(player, "* /crearnpc - /borrarnpc - /setnpcname - /playnpcanim - /stopnpcanim");
                NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos NPC  ~~~~~~~~~~~");
                return;
            }
            player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
        }

        [Command("crearnpc", GreedyArg = true)]
        public void CMD_CreateNpc(Client player, PedHash skin, string name)
        {
            if (player.IsAdmin())
            {
                Actor new_actor = new Actor(Main.GetGuid(), name, skin, player.Position, player.Rotation.Z);
                new_actor.Save(true);

                Main.Actors.Add(new_actor);
                return;
            }
            player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
        }

        [Command("borrarnpc")]
        public void CMD_DeleteNpc(Client player)
        {
            if (player.IsAdmin())
            {
                Actor actor = null;
                float distance = 5f;
                foreach (var a in Main.Actors)
                {
                    if (a.Position.DistanceTo(player.Position) < distance)
                    {
                        actor = a;
                        distance = a.Position.DistanceTo(player.Position);
                    }
                }

                if (actor == null) player.SendChatMessage("~r~ERROR: ~w~No estás a cerca de un npc.");
                else
                {
                    actor.Destroy();
                    Main.Actors.Remove(actor);

                    string actor_file = Main.ACTOR_SAVE_DIR + Path.DirectorySeparatorChar + actor.ID + ".json";
                    if (File.Exists(actor_file)) File.Delete(actor_file);
                }
                return;
            }
            player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
        }

        [Command("setnpcname", GreedyArg = true)]
        public void CMD_SetNpcName(Client player, string name)
        {
            if (player.IsAdmin())
            {
                Actor actor = null;
                float distance = 5f;
                foreach (var a in Main.Actors)
                {
                    if (a.Position.DistanceTo(player.Position) < distance)
                    {
                        actor = a;
                        distance = a.Position.DistanceTo(player.Position);
                    }
                }

                if (actor == null) player.SendChatMessage("~r~ERROR: ~w~No estás a cerca de un npc.");
                else
                {
                    actor.SetName(name);
                    actor.Save(true);
                }
                return;
            }
            player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
        }

        [Command("playnpcanim")]
        public void CMD_SetNpcAnim(Client player, string dict, string name)
        {
            if (player.IsAdmin())
            {
                Actor actor = null;
                float distance = 5f;
                foreach (var a in Main.Actors)
                {
                    if (a.Position.DistanceTo(player.Position) < distance)
                    {
                        actor = a;
                        distance = a.Position.DistanceTo(player.Position);
                    }
                }

                if (actor == null) player.SendChatMessage("~r~ERROR: ~w~No estás a cerca de un npc.");
                else
                {
                    actor.PlayAnimation(dict, name);
                    actor.Save(true);
                }
                return;
            }
            player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
        }

        [Command("stopnpcanim")]
        public void CMD_StopNpcAnim(Client player)
        {
            if (player.IsAdmin())
            {
                Actor actor = null;
                float distance = 5f;
                foreach (var a in Main.Actors)
                {
                    if (a.Position.DistanceTo(player.Position) < distance)
                    {
                        actor = a;
                        distance = a.Position.DistanceTo(player.Position);
                    }
                }

                if (actor == null) player.SendChatMessage("~r~ERROR: ~w~No estás a cerca de un npc.");
                else
                {
                    actor.StopAnimation();
                    actor.Save(true);
                }
                return;
            }
            player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
        }
    }
}
