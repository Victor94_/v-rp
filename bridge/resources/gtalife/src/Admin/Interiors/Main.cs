﻿using GTANetworkAPI;
using System;
using System.Linq;

namespace gtalife.src.Admin.Interiors
{
    class Main : Script
    {
        [RemoteEvent("TeleportToInterior")]
        public void TeleportToInterior(Client player, object[] arguments)
        {
            int i = (int)arguments[0];
            player.Position = InteriorDefinitions.Interiors[i].Position;
            // API.requestIplForPlayer(sender, InteriorDefinitions.Interiors[i].Ipl); // deprecated
        }

        [Command("interiors")]
        public void CMD_Interiors(Client player)
        {
            if (player.IsAdmin())
            {
                var categories = Enum.GetNames(typeof(Category)).Cast<string>().ToList();
                player.TriggerEvent("ShowInteriorBrowser", NAPI.Util.ToJson(categories), NAPI.Util.ToJson(InteriorDefinitions.Interiors));
            }
            else NAPI.Notification.SendNotificationToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
        }
    }
}
