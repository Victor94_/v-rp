﻿using GTANetworkAPI;

namespace gtalife.src.Admin.Interiors
{
    enum Category
    {
        Bunkers,
        Apartments,
        Business,
        MazeBank,
        LomBank,
        Warehouse,
        Special
    }

    class Interior
    {
        public string Name { get; set; }
        public Category Category { get; set; }
        public string Ipl { get; set; }
        public Vector3 Position { get; set; }

        public Interior(string name, Category category, string ipl, Vector3 position)
        {
            Name = name;
            Category = category;
            Ipl = ipl;
            Position = position;
        }
    }
}
