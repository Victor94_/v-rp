﻿using System;
using System.Linq;
using GTANetworkAPI;
using gtalife.src.Database.Models;
using System.Timers;
using Weather = GTANetworkAPI.Weather;

namespace gtalife.src.Server
{
    class Main : Script
    {
        public static Database.Models.Weather Weather;

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            NAPI.Task.Run(() =>
            {
                using (var ctx = new DefaultDbContext())
                {
                    Weather = ctx.Weather.OrderByDescending(x => x.Id).FirstOrDefault();
                    if (Weather == null)
                    {
                        Weather = new Database.Models.Weather { Current = 2, Next = 3, Duration = new TimeSpan(5, 0, 0), CreatedAt = DateTime.Now };
                        ctx.Weather.Add(Weather);
                        ctx.SaveChanges();
                    }

                    UpdateWeatherForAll();
                }
            }, delayTime: 2500);

            Timer weatherTimer = new Timer();
            weatherTimer.Elapsed += new ElapsedEventHandler(OnUpdateWeather);
            weatherTimer.Interval = 60000;
            weatherTimer.Enabled = true;
        }

        private void OnUpdateWeather(object sender, ElapsedEventArgs e)
        {
            if (DateTime.Now > Weather.CreatedAt.Add(Weather.Duration))
            {
                Random rand = new Random();
                int currWeather = Weather.Next;
                int nextWeather = rand.Next(14);
                TimeSpan duration = new TimeSpan(rand.Next(5) + 2, 0, 0);

                Weather = new Database.Models.Weather { Current = currWeather, Next = nextWeather, Duration = duration, CreatedAt = DateTime.Now };
                using (var ctx = new DefaultDbContext())
                {
                    ctx.Weather.Add(Weather);
                    ctx.SaveChangesAsync();
                }
            }

            UpdateWeatherForAll();
        }

        [ServerEvent(Event.PlayerConnected)]
        public void OnPlayerConnected(Client player)
        {
            UpdateWeatherForAll();
        }

        public static void UpdateWeatherForAll()
        {
            if (Weather == null) return;

            float transition = (DateTime.Now.Ticks - Weather.CreatedAt.Ticks) / (Weather.CreatedAt.AddSeconds(Weather.Duration.TotalSeconds).Ticks - Weather.CreatedAt.Ticks);
            if (transition > 1.0) transition = 1.0f;

            // Use setWeatherTypeTransition(current, next, transition) on the client-side for smooth transition
            NAPI.World.SetWeather((Weather)Weather.Current);
        }
    }
}
