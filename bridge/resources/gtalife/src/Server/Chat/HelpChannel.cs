﻿using GTANetworkAPI;
using gtalife.src.Admin;
using gtalife.src.Database.Models;
using gtalife.src.Managers;
using gtalife.src.Player.Utils;
using System;

namespace gtalife.src.Server.Chat
{
    class HelpChannel : Script
    {
        bool isHelpChannelActive = true;

        [Command("n", "~y~USE: ~w~/n(ayuda) [mensaje]", GreedyArg = true, Alias = "ayuda")]
        public void HelpChannelCommand(Client player, string message)
        {
            if (!isHelpChannelActive) player.SendChatMessage("~r~ERROR: ~s~El canal de ayuda está bloqueado.");
            else if (player.HasData("HelpChannelDisabled")) player.SendChatMessage("~r~ERROR: ~w~Has deshabilitado el canal de ayuda. (( /canaldudas ))");
            else if (NAPI.Data.HasEntityData(player, "IsMutedFromHelpChannel")) player.SendChatMessage("~r~ERROR: ~s~Estás silenciado de este canal.");
            else
            {
                if (NAPI.Data.HasEntityData(player, "HelpChannelCooldown"))
                {
                    DateTime cooldown = NAPI.Data.GetEntityData(player, "HelpChannelCooldown");
                    if (cooldown > DateTime.Now)
                    {
                        NAPI.Chat.SendChatMessageToPlayer(player, $"~r~ERROR: ~s~Necesita esperar más ~r~{Math.Round((cooldown - DateTime.Now).TotalSeconds)} ~s~segundos para usar este canal nuevamente.");
                        return;
                    }
                }

                var rank = "";
                User user = NAPI.Data.GetEntityData(player, "User");

                if (user.Admin > 0) rank = $"({player.getRank()}) ";
                else player.SetData("HelpChannelCooldown", DateTime.Now.AddMinutes(1));

                foreach (var p in NAPI.Pools.GetAllPlayers())
                {
                    if (p.HasData("HelpChannelDisabled")) continue;

                    p.SendChatMessage($"~g~[Dudas] ~b~{rank}{player.Name} ({PlayeridManager.GetPlayerId(player)}): {message}");
                }
            }
        }

        [Command("alternarcanaldudas")]
        public void ToggleHelpChannelCommand(Client player)
        {
            User user = NAPI.Data.GetEntityData(player, "User");
            if (user.Admin < 1)
                NAPI.Notification.SendNotificationToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
            else
            {
                isHelpChannelActive = !isHelpChannelActive;
                string newState = isHelpChannelActive ? "desbloqueado" : "bloqueado";
                NAPI.Chat.SendChatMessageToAll($"~y~INFO: ~s~El ~y~{player.getRank()} ~s~{player.Name} ha {newState} el canal de ayuda.");
            }
        }

        [Command("canaldudas")]
        public void ToggleHelpChannelUserCommand(Client player)
        {
            bool state = NAPI.Data.HasEntityData(player, "HelpChannelDisabled");
            if (state) NAPI.Data.ResetEntityData(player, "HelpChannelDisabled"); else NAPI.Data.SetEntityData(player, "HelpChannelDisabled", true);

            string newState = state ? "Habilitaste" : "Deshabilitaste";
            NAPI.Chat.SendChatMessageToPlayer(player, $"~y~INFO: ~s~{newState} el canal de ayuda.");
        }

        [Command("silenciar", "~y~USO: ~w~/silenciar [jugador]", GreedyArg = true)]
        public void Mute(Client sender, string idOrName)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) sender.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
                else if (sender == target) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~Usted no puede silenciar usted mismo.");
                else
                {
                    if (NAPI.Data.HasEntityData(target, "IsMutedFromHelpChannel")) NAPI.Data.ResetEntityData(target, "IsMutedFromHelpChannel");
                    else NAPI.Data.SetEntityData(target, "IsMutedFromHelpChannel", true);

                    string toggleText = NAPI.Data.HasEntityData(target, "IsMutedFromHelpChannel") ? "bloqueado" : "desbloqueado";
                    NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~{sender.Name} te ha {toggleText} del canal de ayuda.");
                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Usted ha {toggleText} {target.Name} del canal de ayuda.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }
    }
}
