﻿using GTANetworkAPI;

namespace gtalife.src.Job
{    
    public class Job
    {
        public string Name { get; private set; }
        public JobType Type { get; private set; }
        public int Sprite { get; private set; }
        public int Color { get; private set; }
        public Vector3 Position { get; private set; }
        public bool RequireLicense { get; private set; }

        public Job(string name, JobType type, int sprite, int color, Vector3 position = null, bool requirelicense = false)
        {
            Name = name;
            Type = type;
            Sprite = sprite;
            Color = color;
            Position = position ?? new Vector3();
            RequireLicense = requirelicense;
        }
    }
}
