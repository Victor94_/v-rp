﻿using GTANetworkAPI;
using gtalife.src.Player.Utils;
using System;
using System.Collections.Generic;
using System.Timers;

namespace gtalife.src.Job
{
    class JobVehicle
    {
        public NetHandle NetHandle { get; set; }

        public DateTime? DeleteAt { get; set; }
    }

    class Main : Script
    {
        public static Dictionary<Client, JobVehicle> PlayerJobVehicle = new Dictionary<Client, JobVehicle>();

        [ServerEvent(Event.PlayerExitVehicle)]
        public void OnPlayerExitVehicle(Client player, Vehicle vehicle)
        {
            if (PlayerJobVehicle.ContainsKey(player) && PlayerJobVehicle[player].NetHandle == vehicle)
            {
                PlayerJobVehicle[player].DeleteAt = DateTime.Now.AddMinutes(3);
            }
        }

        [ServerEvent(Event.PlayerEnterVehicle)]
        public void OnPlayerEnterVehicle(Client player, Vehicle vehicle, sbyte seatID)
        {
            if (PlayerJobVehicle.ContainsKey(player) && PlayerJobVehicle[player].NetHandle == vehicle)
            {
                PlayerJobVehicle[player].DeleteAt = null;
            }
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (PlayerJobVehicle.ContainsKey(player))
            {
                if (NAPI.Entity.DoesEntityExist(PlayerJobVehicle[player].NetHandle)) NAPI.Entity.DeleteEntity(PlayerJobVehicle[player].NetHandle);
                PlayerJobVehicle.Remove(player);
            }
        }

        [RemoteEvent("JobInteract")]
        public void JobInteract(Client sender, object[] arguments)
        {
            if (!sender.HasData("JobMarker_ID")) return;
            int id = sender.GetData("JobMarker_ID");

            if (Player.Data.Character[sender].Job != null) sender.SendChatMessage("~r~ERROR: ~w~Ya tienes un trabajo.");
            else if (JobDefinitions.Jobs[id].RequireLicense && !Player.Data.Character[sender].DrivingLicense) sender.SendChatMessage($"~r~ERROR: ~w~Tienes que tener la licencia de conducción para poder trabajar de {JobDefinitions.Jobs[id].Name.ToLower()}.");
            else
            {
                Player.Data.Character[sender].Job = (int)JobDefinitions.Jobs[id].Type;
                sender.SendChatMessage($"~g~ÉXITO: ~w~¡Ahora eres {JobDefinitions.Jobs[id].Name.ToLower()}!");
                sender.SendChatMessage("~w~Usa ~g~/ayudatrabajo ~w~para conocer las funciones del trabajo.");
            }
        }

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            foreach (var job in JobDefinitions.Jobs)
            {
                Blip blip = NAPI.Blip.CreateBlip(job.Position);
                NAPI.Blip.SetBlipSprite(blip, job.Sprite);
                NAPI.Blip.SetBlipShortRange(blip, true);
                NAPI.Blip.SetBlipTransparency(blip, 255);
                NAPI.Blip.SetBlipColor(blip, job.Color);
                NAPI.Blip.SetBlipName(blip, job.Name);

                NAPI.TextLabel.CreateTextLabel($"~y~Trabajo~s~\n{job.Name}\nPulsa ~y~E ~s~para trabajar", job.Position + new Vector3(0.0, 0.0, 0.5), 15f, 0.5f, 1, new Color(255, 255, 255, 255));
                NAPI.Marker.CreateMarker(1, job.Position + new Vector3(0.0, 0.0, - 1.0), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1, new Color(255, 255, 22, 22));

                var ColShape = NAPI.ColShape.CreateCylinderColShape(job.Position, 0.85f, 0.85f);
                ColShape.OnEntityEnterColShape += (s, player) =>
                {
                    player.SetData("JobMarker_ID", JobDefinitions.Jobs.IndexOf(job));
                    player.TriggerEvent("OnEnterJobMark");
                };

                ColShape.OnEntityExitColShape += (s, player) =>
                {
                    player.ResetData("JobMarker_ID");
                    player.TriggerEvent("OnLeaveJobMark");
                };
            }

            Timer timer = new Timer();
            timer.Elapsed += new ElapsedEventHandler(OnUpdate);
            timer.Interval = 60000;
            timer.Enabled = true;
        }

        private void OnUpdate(object sender, ElapsedEventArgs e)
        {
            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                if (!PlayerJobVehicle.ContainsKey(player)) continue;
                if (PlayerJobVehicle[player].DeleteAt == null) continue;

                if (PlayerJobVehicle[player].DeleteAt < DateTime.Now)
                {
                    if (player.GetJob() == (int)JobType.Trucker) Trucker.TruckerService.FinishService(player);
                    else if (player.GetJob() == (int)JobType.Garbageman) Garbageman.MultiPlayerService.FinishService(player);
                    else if (player.GetJob() == (int)JobType.Courier) Courier.Courier.FinishService(player);
                    else if (player.GetJob() == (int)JobType.Guard) Guard.Service.FinishService(player);
                    else
                    {
                        if (NAPI.Entity.DoesEntityExist(PlayerJobVehicle[player].NetHandle)) NAPI.Entity.DeleteEntity(PlayerJobVehicle[player].NetHandle);
                        PlayerJobVehicle.Remove(player);
                    }
                    player.SendChatMessage("~y~INFO: ~w~Tu trabajo ha sido terminado por inactividad.");
                }
            }
        }

        public static void SendChatMessage(JobType type, string message)
        {
            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                if (player.GetJob() == (int)type)
                {
                    player.SendChatMessage(message);
                }
            }
        }

        [Command("terminartrabajo")]
        public void CMD_FinishService(Client sender)
        {
            if (sender.GetJob() == (int)JobType.Trucker)
            {
                Trucker.TruckerService.FinishService(sender);
                sender.SendChatMessage("~y~INFO: ~w~Terminaste tu trabajo.");
            }
            else if (sender.GetJob() == (int)JobType.Garbageman)
            {
                Garbageman.MultiPlayerService.FinishService(sender);
                Garbageman.SinglePlayerService.FinishService(sender);
                sender.SendChatMessage("~y~INFO: ~w~Terminaste tu trabajo.");
            }
            else if (sender.GetJob() == (int)JobType.ArmsDealer)
            {
                ArmsDealer.ArmsDealerService.FinishService(sender);
                sender.SendChatMessage("~y~INFO: ~w~Terminaste tu trabajo.");
            }
            else if (sender.GetJob() == (int)JobType.Courier)
            {
                Courier.Courier.FinishService(sender);
                sender.SendChatMessage("~y~INFO: ~w~Terminaste tu trabajo.");
            }
            else sender.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
        }
    }
}
