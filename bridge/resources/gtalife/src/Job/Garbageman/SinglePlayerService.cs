﻿using GTANetworkAPI;
using gtalife.src.Flags;
using gtalife.src.Player.Outfit;
using gtalife.src.Player.Utils;
using System;
using System.Collections.Generic;

namespace gtalife.src.Job.Garbageman
{
    class SinglePlayerService : Script
    {
        public static Dictionary<Client, int> PlayerProgress = new Dictionary<Client, int>();

        List<Vector3> DirtPositions = new List<Vector3>()
        {
            { new Vector3(46.68713, 6448.884, 31.299140) },
            { new Vector3(10.69051, 6425.666, 31.425160) },
            { new Vector3(-33.89549, 6491.469, 31.45774) },
            { new Vector3(-51.34518, 6493.114, 31.45677) },
            { new Vector3(-81.99129, 6464.466, 31.33829) },
            { new Vector3(-73.74306, 6446.977, 31.43696) },
            { new Vector3(-95.63476, 6425.026, 31.39787) },
            { new Vector3(-126.6888, 6419.938, 31.53631) },
            { new Vector3(-127.1967, 6395.262, 31.45777) },
            { new Vector3(-170.8267, 6378.484, 31.47275) },
            { new Vector3(-144.5085, 6338.975, 31.59609) },
            { new Vector3(-110.2584, 6305.732, 31.45004) },
            { new Vector3(-76.54299, 6280.375, 31.46341) }
        };

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // marker and label
            NAPI.TextLabel.CreateTextLabel($"~y~Servicio: Barrer Calles~s~\nBasurero", new Vector3(145.553, 6367.352, 31.52922 + 0.5), 15f, 0.5f, 1, new Color(255, 255, 255, 255));
            NAPI.Marker.CreateMarker(1, new Vector3(145.553, 6367.352, 31.52922 - 1.0), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1, new Color(255, 255, 22, 22));

            // col shape
            var ColShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(145.553, 6367.352, 31.52922), 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    if (player.GetJob() == (int)JobType.Garbageman)
                        player.TriggerEvent("job@basurero:singleplayer_show", PlayerProgress.ContainsKey(player));
                    else
                        player.SendChatMessage("~r~ERROR: ~w~Usted no es un basurero.");
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.TriggerEvent("job@basurero:singleplayer_hide");
                }
            };
        }

        public static void FinishService(Client player)
        {
            if (PlayerProgress.ContainsKey(player))
            {
                player.loadClothes();
                PlayerProgress.Remove(player);
                player.TriggerEvent("job@basurero:singleplayer_reset");
            }
        }

        [RemoteEvent("job@basurero:singleplayer_interact")]
        public void InteractSinglePlayerJobGarbage(Client player, object[] arguments)
        {
            if (!PlayerProgress.ContainsKey(player))
            {
                Random rand = new Random();
                int i = rand.Next(DirtPositions.Count);

                player.SetOutfit(64);
                PlayerProgress.Add(player, 0);
                player.SendNotification("Vaya a limpiar la ciudad.");
                player.TriggerEvent("job@basurero:singleplayer_setcheckpoint", DirtPositions[i]);
            }
            else
            {
                player.loadClothes();
                player.TriggerEvent("job@basurero:singleplayer_reset");
                player.SendNotification("Usted ha terminado el trabajo.");
                PlayerProgress.Remove(player);
            }
        }

        [RemoteEvent("job@basurero:singleplayer_sweep")]
        public void SweepSinglePlayerJobGarbage(Client player, object[] arguments)
        {
            player.PlayAnimation("amb@world_human_janitor@male@idle_a", "idle_a", (int)AnimationFlags.Loop);

            var broom = NAPI.Object.CreateObject(1689385044, new Vector3(), new Vector3());
            NAPI.Entity.AttachEntityToEntity(broom, player, "PH_R_HAND", new Vector3(0, 0.1, -0.65), new Vector3(-30, 0, 0));
            player.SetData("BROOM_ATTACHED_OBJECT", broom);

            System.Threading.Tasks.Task.Run(() =>
            {
                NAPI.Task.Run(() =>
                {
                    if (!PlayerProgress.ContainsKey(player)) return;

                    NAPI.Entity.DeleteEntity(player.GetData("BROOM_ATTACHED_OBJECT"));
                    player.ResetData("BROOM_ATTACHED_OBJECT");
                    player.StopAnimation();

                    Random rand = new Random();
                    PlayerProgress[player]++;

                    if (PlayerProgress[player] < 5)
                    {
                        int i = rand.Next(DirtPositions.Count);
                        player.SendNotification("Vaya a la siguiente ubicación.");
                        player.TriggerEvent("job@basurero:singleplayer_setcheckpoint", DirtPositions[i]);
                    }
                    else
                    {
                        int money = 700 + rand.Next(100);
                        player.SendChatMessage($"~g~ÉXITO: ~w~Terminaste el servicio y recibiste tu pago de ~g~${money}~w~.");
                        player.loadClothes();
                        player.giveMoney(money);
                        PlayerProgress.Remove(player);
                    }
                }, delayTime: 11530);
            });
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (PlayerProgress.ContainsKey(player))
            {
                PlayerProgress.Remove(player);
            }

            if (player.HasData("BROOM_ATTACHED_OBJECT"))
            {
                if (NAPI.Entity.DoesEntityExist(player.GetData("BROOM_ATTACHED_OBJECT")))
                    NAPI.Entity.DeleteEntity(player.GetData("BROOM_ATTACHED_OBJECT"));
            }
        }
    }
}
