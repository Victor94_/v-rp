﻿using GTANetworkAPI;
using gtalife.src.Flags;
using gtalife.src.Managers;
using gtalife.src.Player.Outfit;
using gtalife.src.Player.Utils;
using System;
using System.Collections.Generic;

namespace gtalife.src.Job.Garbageman
{
    public class Service
    {
        public int Progress { get; set; }

        public List<Client> Partners { get; set; }
    }

    public class MultiPlayerService : Script
    {
        public static Dictionary<Client, Service> PlayerService = new Dictionary<Client, Service>();
        public static VehicleHash GARBAGE_VEHICLE = (VehicleHash)1917016601;
        readonly float[,] GARBAGE_SPAWN_POSITIONS = new float[,]
        {
            { 145.2359f, 6349.255f, 31.10011f, 0.06692585f, -0.3925306f, 27.06384f },
            { 137.117f, 6345.386f, 30.97857f, 0.4492061f, -0.78734f, 27.06137f },
            { 130.1463f, 6358.872f, 30.97888f, 0.5421301f, -0.7641388f, 27.50065f },
            { 138.0094f, 6362.728f, 31.08304f, 0.5075806f, -0.8040473f, 28.96666f }
        };

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // marker and label
            NAPI.TextLabel.CreateTextLabel($"~y~Servicio: Recoger Basura~s~\nBasurero", new Vector3(146.6389, 6365.428, 31.52922 + 0.5), 15f, 0.5f, 1, new Color(255, 255, 255, 255));
            NAPI.Marker.CreateMarker(1, new Vector3(146.6389, 6365.428, 31.52922 - 1.0), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1, new Color(255, 255, 22, 22));

            // col shape
            var ColShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(146.6389, 6365.428, 31.52922), 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    if (player.GetJob() == (int)JobType.Garbageman)
                    {
                        var players = new List<string>();
                        foreach (var p in NAPI.Pools.GetAllPlayers())
                        {
                            if (p.GetJob() != (int)JobType.Garbageman || PlayerService.ContainsKey(p) || p.Position.DistanceTo(player.Position) > 2f || p == player) continue;

                            players.Add(p.Name);
                        }
                        player.TriggerEvent("ShowGarbageMenu", players);
                    }
                    else
                    {
                        player.SendChatMessage("~r~ERROR: ~w~Usted no es un basurero.");
                    }
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.TriggerEvent("HideGarbageMenu");
                }
            };
        }

        #region methods
        public static void FinishService(Client player)
        {
            if (player.HasData("GARBAGE_JOB_BAG"))
            {
                if (NAPI.Entity.DoesEntityExist(player.GetData("GARBAGE_JOB_BAG")))
                {
                    NAPI.Entity.DeleteEntity(player.GetData("GARBAGE_JOB_BAG"));
                }
            }

            if (Main.PlayerJobVehicle.ContainsKey(player))
            {
                if (NAPI.Entity.DoesEntityExist(Main.PlayerJobVehicle[player].NetHandle)) NAPI.Entity.DeleteEntity(Main.PlayerJobVehicle[player].NetHandle);
                Main.PlayerJobVehicle.Remove(player);
            }

            player.loadClothes();
            player.ResetData("GARBAGE_JOB_OPTION");
            player.TriggerEvent("ResetGarbageJob");
        }
        #endregion

        #region events
        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (player.HasData("GARBAGE_JOB_BAG"))
            {
                if (NAPI.Entity.DoesEntityExist(player.GetData("GARBAGE_JOB_BAG")))
                {
                    NAPI.Entity.DeleteEntity(player.GetData("GARBAGE_JOB_BAG"));
                }
            }
        }

        [ServerEvent(Event.PlayerEnterVehicle)]
        public void OnPlayerEnterVehicle(Client player, Vehicle vehicle, sbyte seatID)
        {
            if (Main.PlayerJobVehicle.ContainsKey(player))
            {
                if (vehicle == Main.PlayerJobVehicle[player].NetHandle && player.GetJob() == (int)JobType.Garbageman)
                {
                    player.TriggerEvent("OnPlayerEnterGarbageJobVehicle");
                }
            }
        }

        private void OnClientEvent(Client sender, string eventName, params object[] arguments)
        {
            switch (eventName)
            {
                case "OnFinishGarbageJob":
                    int money = 250;
                    switch (sender.GetData("GARBAGE_JOB_OPTION"))
                    {
                        case 0:
                            break;
                        case 1:
                            money += 300;
                            break;
                    }

                    sender.SendChatMessage($"~g~ÉXITO: ~w~Terminaste el trabajo, has ganado: ~g~${money}");
                    sender.giveMoney(money);

                    if (Main.PlayerJobVehicle.ContainsKey(sender))
                    {
                        if (NAPI.Entity.DoesEntityExist(Main.PlayerJobVehicle[sender].NetHandle)) NAPI.Entity.DeleteEntity(Main.PlayerJobVehicle[sender].NetHandle);
                        Main.PlayerJobVehicle.Remove(sender);
                    }

                    sender.ResetData("GARBAGE_JOB_OPTION");
                    break;
                case "GarbageCarryBagAnimPlay":
                    var box = NAPI.Object.CreateObject(-1998455445, sender.Position, new Vector3(0, 0, 0));
                    NAPI.Entity.AttachEntityToEntity(box, sender, "SKEL_L_Hand", new Vector3(0.20, 0.1, 0.25), new Vector3(0, 90, 90));
                    sender.SetData("GARBAGE_JOB_BAG", box);
                    sender.PlayAnimation("anim@heists@box_carry@", "idle", (int)(AnimationFlags.Loop | AnimationFlags.OnlyAnimateUpperBody | AnimationFlags.AllowPlayerControl));
                    break;
                case "GarbageCarryBagAnimStop":
                    var _box = sender.GetData("GARBAGE_JOB_BAG");
                    NAPI.Entity.DeleteEntity(_box);
                    sender.ResetData("GARBAGE_JOB_BAG");
                    sender.StopAnimation();
                    break;
                case "GarbageSelectPlayer":
                    Client target = NAPI.Player.GetPlayerFromName((string)arguments[0]);
                    if (target == null || target.Position.DistanceTo(sender.Position) > 2f) sender.SendChatMessage("~r~ERROR: ~w~No estás cerca del jugador.");
                    else if (PlayerService.ContainsKey(target)) sender.SendChatMessage("~r~ERROR: ~w~El jugador ya no está disponible.");
                    else if (PlayerService.ContainsKey(sender)) sender.SendChatMessage("~r~ERROR: ~w~Usted ya está trabajando.");
                    else
                    {
                        sender.SendChatMessage($"~y~INFO: ~w~Estás trabajando con {target.Name}, ve a recoger la basura.");
                        target.SendChatMessage($"~y~INFO: ~w~Estás trabajando con {sender.Name}, ve a recoger la basura.");

                        sender.SetOutfit(64);
                        target.SetOutfit(64);

                        PlayerService.Add(sender, new Service { Progress = 0, Partners = new List<Client>() { target } });
                        PlayerService.Add(target, new Service { Progress = 0, Partners = new List<Client>() { sender } });

                        // Starting job
                        Random random = new Random();
                        int i = random.Next(0, GARBAGE_SPAWN_POSITIONS.GetLength(0));

                        var truck = VehicleManager.CreateVehicle(GARBAGE_VEHICLE, new Vector3(GARBAGE_SPAWN_POSITIONS[i, 0], GARBAGE_SPAWN_POSITIONS[i, 1], GARBAGE_SPAWN_POSITIONS[i, 2]), new Vector3(GARBAGE_SPAWN_POSITIONS[i, 3], GARBAGE_SPAWN_POSITIONS[i, 4], GARBAGE_SPAWN_POSITIONS[i, 5]), 0, 1);
                        truck.EngineStatus = false;

                        Main.PlayerJobVehicle.Add(sender, new JobVehicle { NetHandle = truck.Handle, DeleteAt = DateTime.Now.AddMinutes(3) });
                        Main.PlayerJobVehicle.Add(target, new JobVehicle { NetHandle = truck.Handle, DeleteAt = DateTime.Now.AddMinutes(3) });

                        sender.TriggerEvent("StartGarbageJob", truck.Position.X, truck.Position.Y, truck.Position.Z);
                        target.TriggerEvent("StartGarbageJob", truck.Position.X, truck.Position.Y, truck.Position.Z);
                    }
                    break;
            }
        }
        #endregion
    }
}
