﻿using System;
using System.IO;
using System.Timers;
using GTANetworkAPI;
using Newtonsoft.Json;

namespace gtalife.src.Job.Courier
{
    public class Buyer
    {
        public Guid ID { get; set; }
        public Vector3 Position { get; set; }
        public ProductType Type { get; set; }

        [JsonIgnore]
        private string LabelText => $"~g~Repartidora de {ProductDefinitions.Products[Type].Name}~n~~n~~w~Precio unitario: ~g~${Methods.GetFinalPrice(Type):n0}~n~~w~Capacidad: {Methods.GetBuyerFullnessColor(Stock, MaxStock)}{Stock:n0}/{MaxStock:n0}";

        [JsonIgnore]
        public string FilePath => Main.ResourceFolder + Path.DirectorySeparatorChar + "data/BuyerData" + Path.DirectorySeparatorChar + ID + ".json";

        [JsonIgnore]
        private int _stock;

        public int Stock
        {
            get { return _stock; }

            set
            {
                _stock = value;
                if (Label != null) Label.Text = LabelText;

                // Start "selling" product when stock reaches full (will stop when buyer has less than X% stock)
                if (_stock >= MaxStock && SaleTimer == null)
                {
                    SaleTimer = new Timer();
                    SaleTimer.Elapsed += new ElapsedEventHandler(OnSaleTimerUpdate);
                    SaleTimer.Interval = Main.BuyerSaleInterval * 1000;
                    SaleTimer.Enabled = true;
                }
            }
        }

        private void OnSaleTimerUpdate(object sender, ElapsedEventArgs e)
        {
            Stock--;
            if (Methods.GetBuyerFullness(Stock, MaxStock) < Main.BuyerMinStockPercentage) KillTimer();
        }

        [JsonIgnore]
        private int _maxStock;

        public int MaxStock
        {
            get { return _maxStock; }

            set
            {
                _maxStock = value;
                if (Label != null) Label.Text = LabelText;
            }
        }

        [JsonIgnore]
        private DateTime _lastSave;

        [JsonIgnore]
        public Blip Blip { get; set; }

        [JsonIgnore]
        public Marker Marker { get; set; }

        [JsonIgnore]
        public TextLabel Label { get; set; }

        [JsonIgnore]
        public ColShape ColShape { get; set; }

        [JsonIgnore]
        private Timer SaleTimer { get; set; }

        public Buyer(Guid id, Vector3 position, ProductType type, int stock = 0, int maxStock = 100)
        {
            ID = id;
            Position = position;
            Type = type;
            MaxStock = maxStock;
            Stock = stock;
        }

        public void CreateEntities()
        {
            // blip
            Blip = NAPI.Blip.CreateBlip(Position);
            Blip.Name = $"Repartidora de {ProductDefinitions.Products[Type].Name}";
            Blip.Sprite = 85;
            Blip.Color = 11;
            Blip.ShortRange = true;
            Blip.Scale = 1f;

            // marker
            Marker = NAPI.Marker.CreateMarker(1, Position - new Vector3(0.0, 0.0, 1.0), new Vector3(), new Vector3(), 3, new Color(150, 146, 208, 171));

            // label
            Label = NAPI.TextLabel.CreateTextLabel(LabelText, Position + new Vector3(0.0, 0.0, 0.5), 20f, 0.5f, 1, new Color(255, 255, 255, 255));

            // colshape
            ColShape = NAPI.ColShape.CreateCylinderColShape(Position, 1.5f, 2f);
            ColShape.OnEntityEnterColShape += (shape, entityHandle) =>
            {
                Client player = null;

                if ((player = NAPI.Player.GetPlayerFromHandle(entityHandle)) != null)
                {
                    player.SetData("Courier_MarkerID", ID);
                    player.TriggerEvent("Courier_SetMarkerType", (int)MarkerType.Buyer);
                }
            };

            ColShape.OnEntityExitColShape += (shape, entityHandle) =>
            {
                Client player = null;

                if ((player = NAPI.Player.GetPlayerFromHandle(entityHandle)) != null)
                {
                    player.ResetData("Courier_MarkerID");
                    player.TriggerEvent("Courier_SetMarkerType", (int)MarkerType.None);
                }
            };
        }

        private void KillTimer()
        {
            if (SaleTimer != null) SaleTimer.Dispose();
            SaleTimer = null;
        }

        public void DeleteEntities()
        {
            Blip?.Delete();
            Marker?.Delete();
            Label?.Delete();
            if (ColShape != null) NAPI.ColShape.DeleteColShape(ColShape);
            KillTimer();
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(_lastSave).TotalSeconds < Main.SaveInterval) return;

            File.WriteAllText(FilePath, JsonConvert.SerializeObject(this, Formatting.Indented));
            _lastSave = DateTime.Now;
        }
    }
}
