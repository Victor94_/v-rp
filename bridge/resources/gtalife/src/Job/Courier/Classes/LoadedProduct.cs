﻿using GTANetworkAPI;
using Newtonsoft.Json;

namespace gtalife.src.Job.Courier
{
    public class LoadedProduct
    {
        public string Name { get; set; }

        [JsonIgnore]
        public ProductType Type { get; set; }

        [JsonIgnore]
        public Object Object => NAPI.Entity.GetEntityFromHandle<Object>(_objHandle);

        [JsonIgnore]
        private NetHandle _objHandle;

        public LoadedProduct(ProductType type, NetHandle objHandle)
        {
            Name = type.ToString();
            Type = type;
            _objHandle = objHandle;
        }
    }
}
