"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var menu = null;
var text_mode = 0;
var vehicles = [
    { name: "Burrito", hash: -1743316013 },
    { name: "Rumpo", hash: 1162065741 },
    { name: "Speedo", hash: -810318068 },
    { name: "Youga", hash: 65402552 }
];
API.onServerEventTrigger.connect((eventName, args) => {
    switch (eventName) {
        case "ShowCourierMenuText":
            text_mode = args[0];
            break;
        case "ShowCourierMenu":
            if (menu == null) {
                menu = API.createMenu("Vehículos", "Selecione un vehículo", 0, 0, 6);
                for (let i = 0; i < vehicles.length; i++) {
                    var item = API.createMenuItem(vehicles[i].name, " ");
                    menu.AddItem(item);
                }
                menu.OnItemSelect.connect(function (menu, item, index) {
                    menu.Visible = false;
                    API.triggerServerEvent("RequestCourierVehicle", vehicles[index].hash);
                });
            }
            menu.Visible = true;
            break;
    }
});
API.onKeyDown.connect((sender, e) => {
    if (API.isChatOpen())
        return;
    if (e.KeyCode === Keys.E) {
        if (text_mode > 0) {
            API.triggerServerEvent("CourierMenuInteract");
        }
    }
});
API.onUpdate.connect(() => {
    if (text_mode > 0)
        API.displaySubtitle("Presione ~y~E ~w~para interactuar.", 100);
});
