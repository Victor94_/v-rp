﻿using GTANetworkAPI;
using gtalife.src.Player.Utils;
using System;

namespace gtalife.src.Job.Courier
{
    class Courier : Script
    {
        public static void FinishService(Client player)
        {
            if (src.Job.Main.PlayerJobVehicle.ContainsKey(player))
            {
                if (NAPI.Entity.DoesEntityExist(src.Job.Main.PlayerJobVehicle[player].NetHandle)) NAPI.Entity.DeleteEntity(src.Job.Main.PlayerJobVehicle[player].NetHandle);
                src.Job.Main.PlayerJobVehicle.Remove(player);
            }
        }

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // marker and label
            NAPI.TextLabel.CreateTextLabel($"~y~Trabajo~s~\nRepartidor\nPulsa ~y~E ~s~para interactuar", new Vector3(-2.933412, 6276.231, 31.35756 + 0.5), 15f, 0.5f, 1, new Color(255, 255, 255));
            NAPI.Marker.CreateMarker(1, new Vector3(-2.933412, 6276.231, 31.35756 - 1.0), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1, new Color(255, 22, 22));

            // col shape
            var ColShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(-2.933412, 6276.231, 31.35756), 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.TriggerEvent("ShowCourierMenuText", 1);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.TriggerEvent("ShowCourierMenuText", 0);
                }
            };
        }

        [RemoteEvent("CourierMenuInteract")]
        public void CourierMenuInteract(Client sender, object[] arguments)
        {
            if (sender.GetJob() != (int)JobType.Courier) sender.SendChatMessage("~r~ERROR: ~w~No eres un repartidor.");
            else sender.TriggerEvent("ShowCourierMenu");
        }

        [RemoteEvent("RequestCourierVehicle")]
        public void RequestCourierVehicle(Client sender, object[] arguments)
        {
            if (src.Job.Main.PlayerJobVehicle.ContainsKey(sender))
            {
                if (NAPI.Entity.DoesEntityExist(src.Job.Main.PlayerJobVehicle[sender].NetHandle)) NAPI.Entity.DeleteEntity(src.Job.Main.PlayerJobVehicle[sender].NetHandle);
                src.Job.Main.PlayerJobVehicle.Remove(sender);
            }

            var vehicle = NAPI.Vehicle.CreateVehicle((VehicleHash)arguments[0], new Vector3(4.398468, 6276.474, 30.69729), new Vector3(0.221746, 0.1623477, 121.5682), new Random().Next(160), new Random().Next(160));
            vehicle.EngineStatus = false;
            //vehicle.FuelLevel = 100f;

            src.Job.Main.PlayerJobVehicle.Add(sender, new JobVehicle { NetHandle = vehicle.Handle, DeleteAt = DateTime.Now.AddMinutes(3) });
        }
    }
}
