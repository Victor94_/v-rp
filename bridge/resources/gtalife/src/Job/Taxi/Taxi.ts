﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var taxi_menu = null;
var fare_cost = 0;

var res_X = API.getScreenResolutionMaintainRatio().Width;
var res_Y = API.getScreenResolutionMaintainRatio().Height;

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) => {
    if (eventName == "ShowTaxiMenu") {
        if (taxi_menu == null) {
            taxi_menu = API.createMenu("Taxímetro", "Ajuste su taxón", 0, 0, 6);

            taxi_menu.OnCheckboxChange.connect(function (menu, item, checked) {
                API.triggerServerEvent("ToggleTaximeter", checked);
            });

            var temp_item = API.createCheckboxItem("Activado", "Enciende y apaga el taxímetro.", false);
            taxi_menu.AddItem(temp_item);

            var list = new List(String);
            for (let i = 10; i <= 20; i++) list.Add(i.toString());
            var list_item = API.createListItem("Precio", "Valor cobrado cada 15 segundos.", list, 0);
            taxi_menu.AddItem(list_item);

            list_item.OnListChanged.connect(function (sender, new_index) {
                API.triggerServerEvent("TaximeterValue", list[new_index]);
            });
        }

        taxi_menu.Visible = true;
    }
    else if (eventName == "ClearTaxiMenu") {
        taxi_menu.Clear();

        var temp_item = API.createCheckboxItem("Activado", "Enciende y apaga el taxímetro.", false);
        taxi_menu.AddItem(temp_item);

        var list = new List(String);
        for (let i = 10; i <= 20; i++) list.Add(i.toString());
        var list_item = API.createListItem("Precio", "Valor cobrado cada 15 segundos.", list, 0);
        taxi_menu.AddItem(list_item);

        list_item.OnListChanged.connect(function (sender, new_index) {
            API.triggerServerEvent("TaximeterValue", list[new_index]);
        });
    }
    else if (eventName == "UpdateTaximeter") {
        fare_cost = args[0];
    }
});

API.onKeyUp.connect((sender: any, key: System.Windows.Forms.KeyEventArgs) => {
    switch (key.KeyCode) {
        case Keys.B:
            if (taxi_menu != null) {
                if (taxi_menu.Visible == true) {
                    taxi_menu.Visible = false;
                    return;
                }
            }

            if (!API.isPlayerInAnyVehicle(API.getLocalPlayer())) return;

            API.triggerServerEvent("RequestTaxiMenu");
            break;
    }
});

API.onUpdate.connect(() => {
    if (fare_cost > 0) {
        API.drawText("~y~TAXI: ~g~$~w~" + fare_cost, res_X - 15, res_Y - 100, 0.8, 255, 255, 255, 255, 6, 2, true, true, 0);
    }
});
