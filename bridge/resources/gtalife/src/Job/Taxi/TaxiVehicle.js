"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var menu = null;
var taxi_list = 0;
API.onKeyDown.connect((sender, e) => {
    if (taxi_list == 0 || API.isAnyMenuOpen())
        return;
    if (e.KeyCode === Keys.E) {
        API.triggerServerEvent("RequestTaxiVehicleMenu");
    }
});
API.onServerEventTrigger.connect((eventName, args) => {
    switch (eventName) {
        case "ShowRequestTaxiMenu":
            if (menu == null) {
                menu = API.createMenu("Taxi", "", 0, 0, 6);
                menu.OnItemSelect.connect(function (sender, item, index) {
                    if (index == 0) {
                        taxi_list = 2;
                        API.triggerServerEvent("RequestTaxi");
                    }
                    else if (index == 1) {
                        taxi_list = 1;
                        API.triggerServerEvent("DeleteTaxi");
                    }
                    menu.Visible = false;
                });
            }
            menu.Clear();
            var item = API.createMenuItem("Solicitar taxi", "");
            menu.AddItem(item);
            if (taxi_list > 1) {
                item = API.createMenuItem("Devolver taxi", "");
                menu.AddItem(item);
            }
            menu.Visible = true;
            break;
        case "RequestingTaxi":
            taxi_list = args[0];
            break;
    }
});
API.onUpdate.connect(() => {
    if (taxi_list > 0)
        API.displaySubtitle("Pulsa ~y~E ~w~para interactuar", 100);
});
