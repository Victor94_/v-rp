﻿using GTANetworkAPI;
using gtalife.src.Player.Utils;
using System;
using System.Collections.Generic;
using System.Timers;

namespace gtalife.src.Job.Taxi
{
    public class Taximeter
    {
        public bool Enabled { get; set; }

        public int Fare { get; set; }

        public Taximeter()
        {
            Fare = 10;
            Enabled = false;
        }
    }

    class Taxi : Script
    {
        Dictionary<Client, int> PlayerTaxiFare = new Dictionary<Client, int>();
        Dictionary<Client, Taximeter> PlayerTaximeter = new Dictionary<Client, Taximeter>();

        private void OnClientEventTrigger(Client sender, string eventName, params object[] arguments)
        {
            switch (eventName)
            {
                case "RequestTaxiMenu":
                    {
                        if (!sender.IsInVehicle) return;
                        else if ((VehicleHash)sender.Vehicle.Model != VehicleHash.Taxi) return;
                        else if (sender.GetJob() != (int)JobType.Taxi) return;

                        sender.TriggerEvent("ShowTaxiMenu");
                        break;
                    }
                case "ToggleTaximeter":
                    {
                        bool enabled = (bool)arguments[0];
                        PlayerTaximeter[sender].Enabled = enabled;
                        sender.sendChatAction(enabled ? "has activado el taxímetro." : "has apagado el taxímetro.");

                        if (enabled)
                        {
                            foreach (var passenger in NAPI.Pools.GetAllPlayers())
                            {
                                if (passenger.Vehicle != sender.Vehicle || sender == passenger) continue;

                                if (PlayerTaxiFare.ContainsKey(passenger)) PlayerTaxiFare[passenger] = 0;
                                else PlayerTaxiFare.Add(passenger, 0);
                            }
                        }
                        else
                        {
                            int total = 0;
                            var vehicle = sender.Vehicle;
                            foreach (var passenger in NAPI.Pools.GetAllPlayers())
                            {
                                if (!passenger.IsInVehicle) continue;
                                else if (passenger.Vehicle != vehicle) continue;
                                else if (passenger == sender) continue;
                                else if (!PlayerTaxiFare.ContainsKey(passenger)) continue;

                                var money = PlayerTaxiFare[passenger];
                                total += money;

                                if (money > 0)
                                {
                                    passenger.giveMoney(-money);
                                    passenger.SendNotification($"- ${money}");
                                    passenger.TriggerEvent("UpdateTaximeter", 0);
                                }

                                PlayerTaxiFare.Remove(passenger);
                            }

                            if (total > 0)
                            {
                                sender.giveMoney(total);
                                sender.SendNotification($"+ ${total}");
                                sender.TriggerEvent("UpdateTaximeter", 0);
                            }
                        }

                        break;
                    }
                case "TaximeterValue":
                    {
                        int value = Convert.ToInt32((string)arguments[0]);
                        // sender.sendChatAction(value > PlayerTaximeter[sender].Fare ? "aumentó el precio del taxímetro." : "bajó el precio del taxímetro.");
                        PlayerTaximeter[sender].Fare = value;
                        break;
                    }
            }
        }

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            Timer timer = new Timer();
            timer.Elapsed += new ElapsedEventHandler(OnUpdate);
            timer.Interval = 15000;
            timer.Enabled = true;
        }

        private void OnUpdate(object sender, ElapsedEventArgs e)
        {
            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                if (!player.IsInVehicle) continue;

                if (PlayerTaximeter.ContainsKey(player))
                {
                    int total = 0;
                    foreach (var passenger in NAPI.Pools.GetAllPlayers())
                    {
                        if (!passenger.IsInVehicle) continue;
                        else if (passenger.Vehicle != player.Vehicle) continue;
                        else if (passenger == player) continue;
                        else if (!PlayerTaxiFare.ContainsKey(passenger)) continue;

                        var money = PlayerTaxiFare[passenger];
                        total += money;
                    }
                    player.TriggerEvent("UpdateTaximeter", total);
                }
                else
                {
                    if (!PlayerTaxiFare.ContainsKey(player)) continue;

                    var driver = NAPI.Vehicle.GetVehicleDriver(player.Vehicle);
                    if (driver == null) continue;
                    else if (!PlayerTaximeter.ContainsKey(driver)) continue;

                    PlayerTaxiFare[player] += PlayerTaximeter[driver].Fare;
                    player.TriggerEvent("UpdateTaximeter", PlayerTaxiFare[player]);
                }
            }
        }

        [ServerEvent(Event.PlayerEnterVehicle)]
        public void OnPlayerEnterVehicle(Client player, Vehicle vehicle, sbyte seatID)
        {
            if ((VehicleHash)vehicle.Model != VehicleHash.Taxi) return;

            if (seatID != -1)
            {
                var driver = NAPI.Vehicle.GetVehicleDriver(vehicle);

                if (driver == null) return;
                else if (!PlayerTaximeter.ContainsKey(driver)) return;
                else if (!PlayerTaximeter[driver].Enabled) return;

                PlayerTaxiFare.Add(player, 0);
                player.TriggerEvent("UpdateTaximeter", PlayerTaxiFare[player]);
            }
            else if (player.GetJob() == (int)JobType.Taxi)
            {
                if (!PlayerTaximeter.ContainsKey(player)) PlayerTaximeter.Add(player, new Taximeter());
                if (!PlayerTaximeter[player].Enabled) player.SendNotification("Su taxímetro está apagado, pulsa ~y~B~w~.");
            }
        }

        [ServerEvent(Event.PlayerExitVehicle)]
        public void OnPlayerExitVehicle(Client player, Vehicle vehicle)
        {
            if ((VehicleHash)vehicle.Model != VehicleHash.Taxi) return;

            if (player.GetJob() == (int)JobType.Taxi)
            {
                if (!PlayerTaximeter.ContainsKey(player)) return;
                else if (!PlayerTaximeter[player].Enabled) return;

                int total = 0;
                foreach (var passenger in NAPI.Pools.GetAllPlayers())
                {
                    if (!passenger.IsInVehicle) continue;
                    else if (passenger.Vehicle != vehicle) continue;
                    else if (passenger == player) continue;
                    else if (!PlayerTaxiFare.ContainsKey(passenger)) continue;

                    var money = PlayerTaxiFare[passenger];
                    total += money;

                    if (money > 0)
                    {
                        passenger.giveMoney(-money);
                        passenger.SendNotification($"- ${money}");
                    }

                    passenger.TriggerEvent("UpdateTaximeter", 0);
                    PlayerTaxiFare.Remove(passenger);
                }

                if (total > 0)
                {
                    player.giveMoney(total);
                    player.SendNotification($"+ ${total}");
                }

                player.TriggerEvent("UpdateTaximeter", 0);
                PlayerTaximeter.Remove(player);
                player.TriggerEvent("ClearTaxiMenu");
            }
            else
            {
                var driver = NAPI.Vehicle.GetVehicleDriver(vehicle);

                if (driver == null || driver == player) return;
                else if (!PlayerTaxiFare.ContainsKey(player)) return;

                var money = PlayerTaxiFare[player];
                if (money > 0)
                {
                    player.SendNotification($"- ${money}");
                    driver.SendNotification($"+ ${money}");

                    player.giveMoney(-money);
                    driver.giveMoney(money);
                }

                player.TriggerEvent("UpdateTaximeter", 0);
                driver.TriggerEvent("UpdateTaximeter", 0);

                PlayerTaxiFare.Remove(player);
            }
        }
    }
}
