﻿using GTANetworkAPI;
using gtalife.src.Player.Utils;
using System.Collections.Generic;
using System.Linq;

namespace gtalife.src.Job.Taxi
{
    class TaxiCall : Script
    {
        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (player.HasData("CALLED_TAXI"))
            {
                player.ResetData("CALLED_TAXI");
                SendTaxiPlayersListToDrivers();
            }
        }

        [ServerEvent(Event.PlayerEnterVehicle)]
        public void OnPlayerEnterVehicle(Client player, Vehicle vehicle, sbyte seatID)
        {
            if (player.HasData("CALLED_TAXI"))
            {
                player.ResetData("CALLED_TAXI");
                player.SendChatMessage("~y~INFO: ~w~Cancelaste tu llamada.");
                SendTaxiPlayersListToDrivers();
            }
        }

        [Command("taxi")]
        public void CMD_CallTaxi(Client player)
        {
            if (player.IsInVehicle) player.SendChatMessage("~r~ERROR: ~w~No puedes llamar a un taxi dentro de un vehículo.");
            else if (player.HasData("CALLED_TAXI"))
            {
                player.ResetData("CALLED_TAXI");
                player.SendChatMessage("~y~INFO: ~w~Cancelaste tu llamada.");
                SendTaxiPlayersListToDrivers();
            }
            else
            {
                if (GetAllTaxiDrivers(new Client[] { player }) > 0)
                {
                    player.SetData("CALLED_TAXI", true);
                    player.SendChatMessage("~y~INFO: ~w~Llamó a un taxi, espera en tu posición.");
                    SendTaxiPlayersListToDrivers();
                    Main.SendChatMessage(JobType.Taxi, "~y~INFO: ~w~Una nueva llamada ha sido registrada.");
                }
                else player.SendChatMessage("~y~INFO: ~w~No hay taxistas online en este momento.");
            }
        }

        public static int GetAllTaxiDrivers(Client[] ignoredPlayers = null)
        {
            int i = 0;
            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                if (player.GetJob() != (int)JobType.Taxi) continue;
                else if (ignoredPlayers != null && ignoredPlayers.Contains(player)) continue;
                i++;
            }
            return i;
        }

        private void SendTaxiPlayersListToDrivers()
        {
            var players = NAPI.Pools.GetAllPlayers();
            var passengers = new List<Client>();

            foreach (var player in players)
            {
                if (player.HasData("CALLED_TAXI"))
                {
                    passengers.Add(player);
                }
            }

            foreach (var driver in NAPI.Pools.GetAllPlayers())
            {
                if (driver.GetJob() != (int)JobType.Taxi) continue;

                driver.TriggerEvent("ReceiveTaxiPlayers", passengers);
            }
        }
    }
}
