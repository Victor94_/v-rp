﻿using GTANetworkAPI;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Extensions;
using gtalife.src.Player.Utils;
using System;

namespace gtalife.src.Job.ArmsDealer
{
    class ArmsDealerService : Script
    {
        public static void FinishService(Client player)
        {
            player.TriggerEvent("ResetArmsDealerJob");
        }

        [RemoteEvent("OnTakeMaterialBox")]
        public void OnTakeMaterialBox(Client sender, object[] arguments)
        {
            int amount = 10;

            Random random = new Random();
            amount += random.Next(0, 5);

            sender.giveItem(ItemID.Material, amount);
            sender.SendChatMessage($"~g~ÉXITO: ~w~Recibiste {amount} materiales.");
        }

        [RemoteEvent("CREATE_WEAPON")]
        public void CreateWeapon(Client sender, object[] arguments)
        {
            WeaponHash weapon = (WeaponHash)arguments[0];
            int cost = (int)arguments[1];

            if (sender.getItemAmount(ItemID.Material) < cost)
            {
                sender.ResetData("CanMakeWeapon");
                sender.SendChatMessage("~r~ERROR: ~w~No tienes suficientes materiales.");
                return;
            }

            switch (weapon)
            {
                case WeaponHash.Pistol:
                    sender.giveItem(ItemID.Weapon_Pistol, 1);
                    break;
                case WeaponHash.Pistol50:
                    sender.giveItem(ItemID.Weapon_Pistol50, 1);
                    break;
                case WeaponHash.Revolver:
                    sender.giveItem(ItemID.Weapon_Revolver, 1);
                    break;
                case WeaponHash.MicroSMG:
                    sender.giveItem(ItemID.Weapon_MicroSMG, 1);
                    break;
                case WeaponHash.SMG:
                    sender.giveItem(ItemID.Weapon_SMG, 1);
                    break;
                case WeaponHash.Gusenberg:
                    sender.giveItem(ItemID.Weapon_Gusenberg, 1);
                    break;
                case WeaponHash.AssaultRifle:
                    sender.giveItem(ItemID.Weapon_AssaultRifle, 1);
                    break;
                case WeaponHash.BullpupRifle:
                    sender.giveItem(ItemID.Weapon_BullpupRifle, 1);
                    break;
                case WeaponHash.SniperRifle:
                    sender.giveItem(ItemID.Weapon_SniperRifle, 1);
                    break;
                case WeaponHash.PumpShotgun:
                    sender.giveItem(ItemID.Weapon_PumpShotgun, 1);
                    break;
                case WeaponHash.SawnOffShotgun:
                    sender.giveItem(ItemID.Weapon_SawnOffShotgun, 1);
                    break;
            }

            int idx = sender.getItem(ItemID.Material);
            sender.useItem(idx, cost);

            sender.ResetData("CanMakeWeapon");
        }

        [RemoteEvent("OnCloseWeaponCraftMenu")]
        public void OnCloseWeaponCraftMenu(Client sender, object[] arguments)
        {
            sender.ResetData("CanMakeWeapon");
        }

        [Command("material")]
        public void CMD_GetMaterialJob(Client sender)
        {
            if (sender.GetJob() == (int)JobType.ArmsDealer)
            {
                sender.TriggerEvent("StartArmsDealerJob");
            }
            else
            {
                sender.SendChatMessage("~r~ERROR: ~w~No eres traficante de armas.");
            }
        }

        [Command("vendermats")]
        public void CMD_SellMats(Client sender, int amount)
        {
            if (sender.GetJob() == (int)JobType.ArmsDealer)
            {
                if (sender.Position.DistanceTo(JobDefinitions.Jobs[(int)JobType.ArmsDealer].Position) < 2f)
                {
                    int materials = sender.getItemAmount(ItemID.Material);
                    if (amount > materials)
                    {
                        sender.SendChatMessage("~r~ERROR: ~w~No tienes tantos materiales.");
                        return;
                    }
                    else if (amount < 1)
                    {
                        sender.SendChatMessage("~r~ERROR: ~w~Cantidad invalida.");
                        return;
                    }

                    int cost = 2 * amount;
                    sender.giveMoney(cost);
                    sender.removeItem(sender.getItem(ItemID.Material), amount);
                    sender.SendChatMessage($"~g~ÉXITO: ~w~Vendiste ~g~{amount}~w~ materiales por ~g~${cost}~w~.");
                }
                else
                {
                    sender.SendChatMessage("~r~ERROR: ~w~No estás en el puesto del trabajo.");
                }
            }
            else
            {
                sender.SendChatMessage("~r~ERROR: ~w~No eres traficante de armas.");
            }
        }
    }
}
