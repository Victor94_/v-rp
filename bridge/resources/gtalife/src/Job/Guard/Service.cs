﻿using GTANetworkAPI;
using gtalife.src.Flags;
using gtalife.src.Player.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace gtalife.src.Job.Guard
{
    public class Service : Script
    {
        public static Dictionary<Client, int> PlayerJobProgress = new Dictionary<Client, int>();

        #region Methods
        public static void FinishService(Client player)
        {
            if (Main.PlayerJobVehicle.ContainsKey(player))
            {
                if (NAPI.Entity.DoesEntityExist(Main.PlayerJobVehicle[player].NetHandle)) NAPI.Entity.DeleteEntity(Main.PlayerJobVehicle[player].NetHandle);
                Main.PlayerJobVehicle.Remove(player);
            }

            if (player.HasData("SECURITY_JOB_OBJ"))
            {
                NAPI.Entity.DeleteEntity(player.GetData("SECURITY_JOB_OBJ"));
                player.ResetData("SECURITY_JOB_OBJ");
            }

            if (PlayerJobProgress.ContainsKey(player)) PlayerJobProgress.Remove(player);

            player.TriggerEvent("SecurityGuardDeleteEntities");
        }
        #endregion

        #region Events
        private void OnClientEvent(Client sender, string eventName, params object[] arguments)
        {
            switch (eventName)
            {
                case "SecurityGuardGetNextCheckpoint":
                    {
                        PlayerJobProgress[sender]++;
                        if (PlayerJobProgress[sender] < 5)
                        {
                            Random random = new Random();
                            var atms = Gameplay.Bank.Main.Banks.Where(x => x.Type != 0).ToList();
                            sender.SendNotification("Vaya a la siguiente dirección.");
                            sender.TriggerEvent("SecurityGuardGetNextCheckpoint", atms[random.Next(atms.Count)].Position);
                        }
                        else if (PlayerJobProgress[sender] == 5)
                        {
                            List<Vector3> positions = new List<Vector3>()
                            {
                                new Vector3(-115.6064, 6458.176, 31.46846)
                            };

                            Random rand = new Random();
                            sender.TriggerEvent("SecurityGuardGetNextCheckpoint", positions[rand.Next(positions.Count)], true);
                            sender.SendNotification("Vaya al banco.");
                        }
                        else
                        {
                            Random rand = new Random();
                            int payment = 600 + rand.Next(50);

                            sender.SendNotification($"Usted ha finalizado el servicio y ha recibido ~g~$~w~{payment}.");
                            sender.giveMoney(payment);
                            sender.TriggerEvent("SecurityGuardDeleteEntities");
                        }
                        break;
                    }
                case "SecurityGuardCarryAnimPlay":
                    {
                        var obj = NAPI.Object.CreateObject(289396019, sender.Position, new Vector3(0, 0, 0));
                        NAPI.Entity.AttachEntityToEntity(obj, sender, "SKEL_L_Hand", new Vector3(0.2, 0.05, 0.05), new Vector3(0, 0, 0));
                        sender.SetData("SECURITY_JOB_OBJ", obj);
                        sender.PlayAnimation("anim@heists@box_carry@", "idle", (int)(AnimationFlags.Loop | AnimationFlags.OnlyAnimateUpperBody | AnimationFlags.AllowPlayerControl));
                        break;
                    }
                case "SecurityGuardCarryAnimStop":
                    {
                        if (!sender.HasData("SECURITY_JOB_OBJ")) return;

                        var obj = sender.GetData("SECURITY_JOB_OBJ");
                        NAPI.Entity.DeleteEntity(obj);
                        sender.ResetData("SECURITY_JOB_OBJ");
                        sender.StopAnimation();
                        break;
                    }
            }
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (PlayerJobProgress.ContainsKey(player)) PlayerJobProgress.Remove(player);

            if (player.HasData("SECURITY_JOB_OBJ"))
            {
                NAPI.Entity.DeleteEntity(player.GetData("SECURITY_JOB_OBJ"));
                player.ResetData("SECURITY_JOB_OBJ");
            }
        }
        #endregion
    }
}
