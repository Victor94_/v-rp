﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var resX = API.getScreenResolutionMaintainRatio().Width;
var resY = API.getScreenResolutionMaintainRatio().Height;

API.onUpdate.connect(() => {
    if (!API.getHudVisible()) return;
    API.drawText("GTAlife Roleplay — ~b~Versión BETA ~w~— www.gtalife.es", resX - 30, resY - 30, 0.40, 255, 255, 255, 255, 7, 2, true, true, 0);
});