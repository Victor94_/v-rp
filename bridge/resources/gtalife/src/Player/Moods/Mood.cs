﻿using System;
using System.Linq;
using System.Collections.Generic;
using GTANetworkAPI;

namespace gtalife.src.Player.Moods
{
    public class Mood
    {
        public string Name;
        public string AnimName;

        public Mood(string name, string anim_name)
        {
            Name = name;
            AnimName = anim_name;
        }
    }

    public class Main : Script
    {
        // anim names taken from https://alexguirre.github.io/animations-list/
        List<Mood> PlayerMoods = new List<Mood>
        {
            new Mood("Normal", ""),
            new Mood("Apuntado", "mood_aiming_1"),
            new Mood("Enojado", "mood_angry_1"),
            new Mood("Borracho", "mood_drunk_1"),
            new Mood("Feliz", "mood_happy_1"),
            new Mood("Lesionado", "mood_injured_1"),
            new Mood("Estresado", "mood_stressed_1"),
            new Mood("Enfurruñado", "mood_sulk_1")
        };

        [ServerEvent(Event.EntityModelChange)]
        public void OnEntityModelChange(NetHandle entityHandle, uint oldModel)
        {
            Client player;

            if ((player = NAPI.Player.GetPlayerFromHandle(entityHandle)) != null)
            {
                if (player.HasSharedData("PlayerMood"))
                {
                    foreach (var p in NAPI.Pools.GetAllPlayers())
                    {
                        p.TriggerEvent("SetPlayerMood", player, player.GetSharedData("PlayerMood"));
                    }
                }
            }
        }

        [RemoteEvent("RequestMoods")]
        public void RequestMoods(Client player, object[] arguments)
        {
            player.TriggerEvent("ReceiveMoods", NAPI.Util.ToJson(PlayerMoods.Select(m => m.Name)));
        }

        [RemoteEvent("SetMood")]
        public void SetMood(Client player, object[] arguments)
        {
            if (arguments.Length < 1) return;

            int id = Convert.ToInt32(arguments[0]);
            if (id < 0 || id >= PlayerMoods.Count) return;

            if (id == 0) // reset
            {
                foreach (var p in NAPI.Pools.GetAllPlayers())
                {
                    p.TriggerEvent("ResetPlayerMood", player);
                }
                player.ResetSharedData("PlayerMood");
            }
            else
            {
                foreach (var p in NAPI.Pools.GetAllPlayers())
                {
                    p.TriggerEvent("SetPlayerMood", player, PlayerMoods[id].AnimName);
                }
                player.SetSharedData("PlayerMood", PlayerMoods[id].AnimName);
            }

            player.TriggerEvent("SetCurrentMoodIndex", id);
            player.SendChatMessage("~y~INFO: ~w~Estado de ánimo cambiado a: ~y~" + PlayerMoods[id].Name + ".");
        }
    }
}
