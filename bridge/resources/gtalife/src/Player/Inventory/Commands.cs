﻿using GTANetworkAPI;
using gtalife.src.Admin;
using gtalife.src.Managers;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Extensions;
using gtalife.src.Player.Utils;

namespace gtalife.src.Player.Inventory
{
    public class Commands : Script
    {
        [Command("daritem")]
        public void CMD_AddItem(Client player, string idOrName, ItemID item, int amount)
        {
            if (player.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    if (player != target) NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~{player.Name} te dio {amount} {item}.");
                    NAPI.Notification.SendNotificationToPlayer(player, $"~r~ADMIN: ~w~Usted dio {amount} {item} a {target.Name}.");

                    target.giveItem(item, amount);
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("retiraritems")]
        public void CMD_ClearItems(Client player, string idOrName)
        {
            if (player.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    if (player != target) NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~{player.Name} eliminó todos tus items.");
                    NAPI.Notification.SendNotificationToPlayer(player, $"~r~ADMIN: ~w~Usted eliminó todos items de {target.Name}.");

                    target.clearInventory();
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
        }        
    }
}
