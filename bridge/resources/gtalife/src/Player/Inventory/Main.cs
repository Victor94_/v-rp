﻿using System;
using System.IO;
using System.Timers;
using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Extensions;
using gtalife.src.Player.Inventory.Interfaces;
using gtalife.src.Player.Utils;

namespace gtalife.src.Player.Inventory
{
    public class Main : Script
    {
        public static Dictionary<NetHandle, List<InventoryItem>> Inventories = new Dictionary<NetHandle, List<InventoryItem>>();
        public Dictionary<Guid, DroppedItem> DroppedItems = new Dictionary<Guid, DroppedItem>();

        // these two are modified from meta.xml
        public static int MaxInventorySlots = 10;
        public static int DroppedItemLifetime = 5;

        public static string SaveLocation = "data/Inventories";
        public Timer DropRemover = null;

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            SaveLocation = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + "data" + Path.DirectorySeparatorChar + "Inventories";
            if (!Directory.Exists(SaveLocation)) Directory.CreateDirectory(SaveLocation);

            if (NAPI.Resource.HasSetting(this, "inventorySlots")) MaxInventorySlots = NAPI.Resource.GetSetting<int>(this, "inventorySlots");
            if (NAPI.Resource.HasSetting(this, "dropLifetime")) DroppedItemLifetime = NAPI.Resource.GetSetting<int>(this, "dropLifetime");

            NAPI.Util.ConsoleOutput($"Inventory Slots: {MaxInventorySlots}");
            NAPI.Util.ConsoleOutput($"Dropped Item Lifetime: {TimeSpan.FromMinutes(DroppedItemLifetime).ToString(@"hh\:mm\:ss")}");

            DropRemover = new Timer();
            DropRemover.Elapsed += new ElapsedEventHandler(OnDropRemover);
            DropRemover.Interval = 60000;
            DropRemover.Enabled = true;
        }

        private void OnDropRemover(object sender, ElapsedEventArgs e)
        {
            int count = 0;

            foreach (KeyValuePair<Guid, DroppedItem> item in DroppedItems.Where(kv => kv.Value.RemoveTime <= DateTime.Now).ToList())
            {
                item.Value.Delete();
                DroppedItems.Remove(item.Key);
                count++;
            }

            if (count > 0) NAPI.Util.ConsoleOutput($"Removed {count} items from the world.");
        }

        [RemoteEvent("RequestInventory")]
        public void RequestInventory(Client player, object[] arguments)
        {
            if (!Inventories.ContainsKey(player.Handle)) return;
            player.sendInventory();
        }

        [RemoteEvent("RequestNearbyPlayersInventory")]
        public void RequestNearbyPlayersInventory(Client player, object[] arguments)
        {
            if (arguments.Length < 2 || !Inventories.ContainsKey(player.Handle)) return;
            int idx = Convert.ToInt32(arguments[0]);
            int amount = Convert.ToInt32(arguments[1]);

            if (idx < 0 || idx >= Inventories[player.Handle].Count) return;

            List<string> players = new List<string>();

            foreach (var p in NAPI.Pools.GetAllPlayers())
            {
                if (p == player) continue;
                else if (p.Position.DistanceTo(player.Position) > 2f) continue;
                else players.Add(p.Name);
            }

            if (players.Count < 1) player.SendNotification("~r~No hay jugadores cercanos a usted.");
            else player.TriggerEvent("ReceiveNearbyPlayersInventory", idx, amount, NAPI.Util.ToJson(players));
        }

        [RemoteEvent("ConsumeItem")]
        public void ConsumeItem(Client player, object[] arguments)
        {
            if (arguments.Length < 1 || !Inventories.ContainsKey(player.Handle)) return;
            int idx = Convert.ToInt32(arguments[0]);
            player.useItem(idx);
        }

        [RemoteEvent("DropItem")]
        public void DropItem(Client player, object[] arguments)
        {
            if (arguments.Length < 5 || !Inventories.ContainsKey(player.Handle)) return;
            int idx = Convert.ToInt32(arguments[0]);
            int amount = Convert.ToInt32(arguments[1]);
            Vector3 pos = new Vector3((float)arguments[2], (float)arguments[3], (float)arguments[4]);

            if (idx < 0 || idx >= Inventories[player.Handle].Count) return;
            if (player.Position.DistanceTo(pos) >= 3.0) return;

            if (Inventories[player.Handle][idx].Item is IDroppable dropInfo)
            {
                Guid dropID;

                do
                {
                    dropID = Guid.NewGuid();
                } while (DroppedItems.ContainsKey(dropID));

                DroppedItems.Add(dropID, new DroppedItem(dropID, Inventories[player.Handle][idx].ID, dropInfo.DropModel, pos, amount));
                player.removeItem(idx, amount);
            }
        }

        [RemoteEvent("TradeItem")]
        public void TradeItem(Client player, object[] arguments)
        {
            if (arguments.Length < 3 || !Inventories.ContainsKey(player.Handle)) return;

            int idx = Convert.ToInt32(arguments[0]);
            int amount = Convert.ToInt32(arguments[1]);
            string playerName = Convert.ToString(arguments[2]);
            int price = Convert.ToInt32(arguments[3]);

            if (idx < 0 || idx >= Inventories[player.Handle].Count) return;

            var target = NAPI.Player.GetPlayerFromName(playerName);
            if (target == null) player.SendNotification("~r~ERROR: ~w~El jugador no está cerca.");
            else if (player.Position.DistanceTo(target.Position) > 2f) player.SendNotification("~r~ERROR: ~w~El jugador no está cerca.");
            else
            {
                target.SendChatMessage($"~y~INFO: ~w~{player.Name} te ofrece {amount} {Inventories[player.Handle][idx].Item.Name.ToLower()} por ${price}. (( /aceptaritem ))");
                player.SendChatMessage($"~y~INFO: ~w~Usted ofreció {amount} {Inventories[player.Handle][idx].Item.Name.ToLower()} por ${price} a {target.Name}.");

                target.SetData("ITEM_OFFER_IDX", idx);
                target.SetData("ITEM_OFFER_PLAYER", player);
                target.SetData("ITEM_OFFER_AMOUNT", amount);
                target.SetData("ITEM_OFFER_PRICE", price);
            }
        }

        [RemoteEvent("TakeItem")]
        public void TakeItem(Client player, object[] arguments)
        {
            if (arguments.Length < 1 || !Inventories.ContainsKey(player.Handle)) return;
            Guid dropID = new Guid(arguments[0].ToString());
            if (!DroppedItems.ContainsKey(dropID)) return;

            DroppedItem item = DroppedItems[dropID];
            int added = player.giveItem(item.ID, item.Quantity);

            if (added > 0)
            {
                item.Quantity -= added;

                if (item.Quantity < 1)
                {
                    item.Delete();
                    DroppedItems.Remove(dropID);
                }

                player.SendChatMessage($"~y~INFO: ~w~Tomó {item.Name} x{added}.");
            }
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            player.saveInventory();
            if (Inventories.ContainsKey(player.Handle)) Inventories.Remove(player.Handle);
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (Client player in NAPI.Pools.GetAllPlayers()) player.saveInventory();
            foreach (KeyValuePair<Guid, DroppedItem> item in DroppedItems) item.Value.Delete();
            Inventories.Clear();
            DroppedItems.Clear();
        }

        [Command("aceptaritem")]
        public void CMD_AcceptItem(Client player)
        {
            if (!player.HasData("ITEM_OFFER_PLAYER"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Nadie te ha ofrecido un item.");
                return;
            }

            Client target = player.GetData("ITEM_OFFER_PLAYER");
            int idx = player.GetData("ITEM_OFFER_IDX");
            int amount = player.GetData("ITEM_OFFER_AMOUNT");
            int price = player.GetData("ITEM_OFFER_PRICE");

            if (price > player.getMoney()) player.SendChatMessage("~r~ERROR: ~w~No tienes suficiente dinero.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está cerca.");
            else if (player.Position.DistanceTo(target.Position) > 2f) player.SendChatMessage("~r~ERROR: ~w~El jugador no está cerca.");
            else
            {
                target.SendNotification($"- {amount} {Inventories[target.Handle][idx].Item.Name.ToLower()}");
                player.SendNotification($"+ {amount} {Inventories[target.Handle][idx].Item.Name.ToLower()}");
                target.sendChatAction($"dio un item a {player.Name}.");

                player.giveItem(Inventories[target.Handle][idx].ID, amount);
                target.removeItem(idx, amount);

                target.giveMoney(price);
                player.giveMoney(-price);

                player.ResetData("ITEM_OFFER_IDX");
                player.ResetData("ITEM_OFFER_PLAYER");
                player.ResetData("ITEM_OFFER_AMOUNT");
                player.ResetData("ITEM_OFFER_PRICE");
            }
        }
        #endregion
    }
}
