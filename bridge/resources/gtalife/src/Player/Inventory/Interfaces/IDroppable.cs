﻿using gtalife.src.Player.Inventory.Classes;

namespace gtalife.src.Player.Inventory.Interfaces
{
    interface IDroppable
    {
        WorldModel DropModel { get; set; }
    }
}
