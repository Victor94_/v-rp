﻿using GTANetworkAPI;
using System.Collections.Generic;
using gtalife.src.Player.Inventory.ItemTypes;

namespace gtalife.src.Player.Inventory.Classes
{
    public enum ItemID
    {
        Armor_Tier1 = 0,
        Armor_Tier2,
        Armor_Tier3,
        Armor_Tier4,
        Armor_Tier5,

        FirstAidKit_Tier1 = 10,
        FirstAidKit_Tier2,
        FirstAidKit_Tier3,
        FirstAidKit_Tier4,
        FirstAidKit_Tier5,

        Weapon_StunGun = 50,
        Weapon_Pistol,
        Weapon_Pistol50,
        Weapon_Revolver,
        Weapon_MicroSMG,
        Weapon_SMG,
        Weapon_Gusenberg,
        Weapon_AssaultRifle,
        Weapon_BullpupRifle,
        Weapon_SniperRifle,
        Weapon_PumpShotgun,
        Weapon_SawnOffShotgun,
        Weapon_Nightstick,
        Weapon_Flashlight,

        Material = 100,
        Jerrycan,
        Boombox,

        Water = 150,
        Beer,
        Whisky,
        Vodka
    }

    public class ItemDefinitions
    {
        public static Dictionary<ItemID, BaseItem> ItemDictionary = new Dictionary<ItemID, BaseItem>
        {
            { ItemID.Armor_Tier1, new ArmorItem("Chaleco súper ligero", "+20 armor.", 3, 20, new WorldModel("prop_armour_pickup", new Vector3(0.0, 0.0, 0.1), new Vector3(-90.0, 0.0, 0.0))) },
            { ItemID.Armor_Tier2, new ArmorItem("Chaleco ligero", "+40 armor.", 2, 40, new WorldModel("prop_bodyarmour_02", new Vector3(0.0, 0.0, 0.125), new Vector3(-90.0, 0.0, 0.0))) },
            { ItemID.Armor_Tier3, new ArmorItem("Chaleco estándar", "+60 armor.", 2, 60, new WorldModel("prop_bodyarmour_03", new Vector3(0.0, 0.0, 0.125), new Vector3(-90.0, 0.0, 0.0))) },
            { ItemID.Armor_Tier4, new ArmorItem("Chaleco fuerte", "+80 armor.", 1, 80, new WorldModel("prop_bodyarmour_04", new Vector3(0.0, 0.0, 0.125), new Vector3(-90.0, 0.0, 0.0))) },
            { ItemID.Armor_Tier5, new ArmorItem("Chaleco súper fuerte", "+100 armor.", 1, 100, new WorldModel("prop_bodyarmour_05", new Vector3(0.0, 0.0, 0.125), new Vector3(-90.0, 0.0, 0.0))) },

            { ItemID.FirstAidKit_Tier1, new FirstAidKitItem("Botiquín pequeño", "+20 salud.", 3, 20, new WorldModel("prop_ld_health_pack", new Vector3(0.0, 0.0, 0.125), new Vector3(-90.0, 0.0, 0.0))) },
            { ItemID.FirstAidKit_Tier2, new FirstAidKitItem("Botiquín medio", "+40 salud.", 2, 40, new WorldModel("prop_ld_health_pack", new Vector3(0.0, 0.0, 0.125), new Vector3(-90.0, 0.0, 0.0))) },
            { ItemID.FirstAidKit_Tier3, new FirstAidKitItem("Botiquín estándar", "+60 salud.", 2, 60, new WorldModel("prop_ld_health_pack", new Vector3(0.0, 0.0, 0.125), new Vector3(-90.0, 0.0, 0.0))) },
            { ItemID.FirstAidKit_Tier4, new FirstAidKitItem("Botiquín grande", "+80 salud.", 1, 80, new WorldModel("prop_ld_health_pack", new Vector3(0.0, 0.0, 0.125), new Vector3(-90.0, 0.0, 0.0))) },
            { ItemID.FirstAidKit_Tier5, new FirstAidKitItem("Botiquín enorme", "+100 salud.", 1, 100, new WorldModel("prop_ld_health_pack", new Vector3(0.0, 0.0, 0.125), new Vector3(-90.0, 0.0, 0.0))) },

            { ItemID.Weapon_StunGun, new WeaponItem("Tazer", "Una pistola não letal.", 1, WeaponHash.StunGun, new WorldModel("prop_box_guncase_01a", new Vector3(0.0, 0.0, 0.1), new Vector3())) },
            { ItemID.Weapon_Pistol, new WeaponItem("Pistol", "Una pistola compacta, liviana y semiautomática.", 1, WeaponHash.Pistol, new WorldModel("prop_box_guncase_01a", new Vector3(0.0, 0.0, 0.1), new Vector3())) },
            { ItemID.Weapon_Pistol50, new WeaponItem("Pistol 50mm", "Una pistola compacta, liviana y semiautomática.", 1, WeaponHash.Pistol50, new WorldModel("prop_box_guncase_01a", new Vector3(0.0, 0.0, 0.1), new Vector3())) },
            { ItemID.Weapon_Revolver, new WeaponItem("Revolver", "Una pistola compacta, liviana y semiautomática.", 1, WeaponHash.Revolver, new WorldModel("prop_box_guncase_01a", new Vector3(0.0, 0.0, 0.1), new Vector3())) },
            { ItemID.Weapon_MicroSMG, new WeaponItem("MicroSMG", "Una metralhadora compacta, liviana y semiautomática.", 1, WeaponHash.MicroSMG, new WorldModel("prop_gun_case_01", new Vector3(0.0, 0.0, 0.15), new Vector3())) },
            { ItemID.Weapon_SMG, new WeaponItem("SMG", "Una metralhadora compacta, liviana y semiautomática.", 1, WeaponHash.SMG, new WorldModel("prop_gun_case_01", new Vector3(0.0, 0.0, 0.15), new Vector3())) },
            { ItemID.Weapon_Gusenberg, new WeaponItem("Gusenberg", "Una metralhadora compacta, liviana y semiautomática.", 1, WeaponHash.Gusenberg, new WorldModel("prop_gun_case_01", new Vector3(0.0, 0.0, 0.15), new Vector3())) },
            { ItemID.Weapon_AssaultRifle, new WeaponItem("Assault Rifle", "Una metralhadora fuerte.", 1, WeaponHash.AssaultRifle, new WorldModel("prop_gun_case_01", new Vector3(0.0, 0.0, 0.15), new Vector3())) },
            { ItemID.Weapon_BullpupRifle, new WeaponItem("Bullpup Rifle", "Una metralhadora fuerte.", 1, WeaponHash.BullpupRifle, new WorldModel("prop_gun_case_01", new Vector3(0.0, 0.0, 0.15), new Vector3())) },
            { ItemID.Weapon_SniperRifle, new WeaponItem("Sniper Rifle", "Rifle de francotirador estándar Ideal para situaciones que requieren precisión a largo plazo. Las limitaciones incluyen una velocidad de recarga lenta y una tasa de fuego muy baja.", 1, WeaponHash.SniperRifle, new WorldModel("prop_gun_case_01", new Vector3(0.0, 0.0, 0.15), new Vector3())) },
            { ItemID.Weapon_PumpShotgun, new WeaponItem("Pump Shotgun", "Escopeta estándar ideal para combate de corto alcance.", 1, WeaponHash.PumpShotgun, new WorldModel("prop_gun_case_01", new Vector3(0.0, 0.0, 0.15), new Vector3())) },
            { ItemID.Weapon_SawnOffShotgun, new WeaponItem("SawnOff Shotgun", "Escopeta serrada ideal para combate de corto alcance.", 1, WeaponHash.SawnOffShotgun, new WorldModel("prop_gun_case_01", new Vector3(0.0, 0.0, 0.15), new Vector3())) },
            { ItemID.Weapon_Nightstick, new WeaponItem("Porra", "Um bastão usado para controlar criminosos.", 1, WeaponHash.Nightstick, new WorldModel("prop_gun_case_01", new Vector3(0.0, 0.0, 0.15), new Vector3())) },
            { ItemID.Weapon_Flashlight, new WeaponItem("Linterna", "Un aparato portátil de iluminación que funciona mediante pilas o baterías eléctricas.", 1, WeaponHash.Flashlight, new WorldModel("prop_gun_case_01", new Vector3(0.0, 0.0, 0.15), new Vector3())) },
            
            { ItemID.Material, new MaterialItem("Material", "Usado para fabricar armas.", 50000, new WorldModel("prop_cs_cardbox_01", new Vector3(0.0, 0.0, 0.125), new Vector3(0.0, 0.0, 0.0))) },
            { ItemID.Jerrycan, new JerrycanItem("Bidón", "Un recipiente hermético utilizado para contener, transportar y almacenar líquidos.", 5, new WorldModel("prop_jerrycan_01a", new Vector3(0.0, 0.0, 0.125), new Vector3(0.0, 0.0, 0.0))) },
            { ItemID.Boombox, new BoomboxItem("Radio", "Un dispositivo electrónico que recibe ondas de radio y convierte la información que llevan en una forma utilizable.", 1, new WorldModel("prop_boombox_01", new Vector3(0.0, 0.0, 0.125), new Vector3(0.0, 0.0, 0.0))) },
            
            { ItemID.Water, new DrinkItem("Agua", "Un líquido incoloro, transparente, inodoro e insípido que forma los mares, lagos, ríos y la lluvia y es la base de los fluidos de los organismos vivos.", 1, new WorldModel("prop_ld_flow_bottle", new Vector3(0.0, 0.0, 0.125), new Vector3(0.0, 0.0, 0.0))) },
            { ItemID.Beer, new DrinkItem("Cerveza", "Bebida producida a partir de la fermentación de cereales, principalmente la cebada maltada. Se cree que fue una de las primeras bebidas alcohólicas que fueron creadas por el ser humano.", 1, new WorldModel("prop_amb_beer_bottle", new Vector3(0.0, 0.0, 0.125), new Vector3(0.0, 0.0, 0.0))) },
            { ItemID.Whisky, new DrinkItem("Whisky", "El whisky es una bebida alcohólica destilada de granos, a menudo incluyendo malta, que ha sido envejecida en barricas.", 1, new WorldModel("prop_amb_beer_bottle", new Vector3(0.0, 0.0, 0.125), new Vector3(0.0, 0.0, 0.0))) },
            { ItemID.Vodka, new DrinkItem("Vodka", "Una popular bebida destilada, incolora, casi sin sabor y con un grado alcohólico.", 1, new WorldModel("prop_amb_beer_bottle", new Vector3(0.0, 0.0, 0.125), new Vector3(0.0, 0.0, 0.0))) }
        };
    }
}
