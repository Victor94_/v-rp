﻿using System;
using GTANetworkAPI;

namespace gtalife.src.Player.Inventory.Classes
{
    public class DroppedItem
    {
        public ItemID ID { get; private set; }
        public string Name { get; private set; }
        public DateTime RemoveTime { get; private set; }

        public int Quantity
        {
            get
            {
                return _quantity;
            }

            set
            {
                _quantity = value;
                _itemLabel.Text = $"{Name} x{value}";
            }
        }

        private int _quantity;
        private GTANetworkAPI.Object _itemObject;
        private TextLabel _itemLabel;

        public DroppedItem(Guid dropID, ItemID itemID, WorldModel model, Vector3 position, int quantity)
        {
            ID = itemID;
            Name = ItemDefinitions.ItemDictionary[itemID].Name;
            _quantity = quantity;
            RemoveTime = DateTime.Now.AddMinutes(Main.DroppedItemLifetime);

            _itemObject = NAPI.Object.CreateObject(NAPI.Util.GetHashKey(model.ModelName), position + model.Offset, model.Rotation);
            _itemObject.SetSharedData("ItemDropID", dropID.ToString());

            _itemLabel = NAPI.TextLabel.CreateTextLabel($"{Name} x{quantity}", position + model.Offset + new Vector3(0.0, 0.0, 0.20), 10f, 0.5f, 1, new Color(255, 255, 255, 255));
        }

        public void Delete()
        {
            _itemObject.Delete();
            _itemLabel.Delete();
        }
    }
}
