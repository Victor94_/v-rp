﻿using GTANetworkAPI;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Interfaces;

namespace gtalife.src.Player.Inventory.ItemTypes
{
    public class MaterialItem : BaseItem, IDroppable
    {
        public WorldModel DropModel { get; set; }

        public MaterialItem(string name, string description, int stackSize, WorldModel dropModel) : base(name, description, stackSize)
        {
            DropModel = dropModel;
        }

        public override bool Use(Client player)
        {
            if (player.HasData("CanMakeWeapon"))
                return true;

            player.TriggerEvent("CloseInventoryMenus");
            player.SetData("CanMakeWeapon", true);
            player.TriggerEvent("ShowWeaponCraftMenu");
            return false;
        }
    }
}
