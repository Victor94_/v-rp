﻿using GTANetworkAPI;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Interfaces;

namespace gtalife.src.Player.Inventory.ItemTypes
{
    public class FirstAidKitItem : BaseItem, IDroppable
    {
        public int Value { get; set; }
        public WorldModel DropModel { get; set; }

        public FirstAidKitItem(string name, string description, int stackSize, int healthValue, WorldModel dropModel) : base(name, description, stackSize)
        {
            Value = healthValue;
            DropModel = dropModel;
        }

        public override bool Use(Client player)
        {
            if (player.Health == 100)
            {
                player.SendChatMessage("~r~ERROR: ~w~Tienes salud completa.");
                return false;
            }

            player.SendChatMessage($"~y~INFO: ~w~Botiquín de primeros auxilios usado. (+{Value})");
            player.Health = ((player.Health + Value > 100) ? 100 : player.Health + Value);
            return true;
        }
    }
}
