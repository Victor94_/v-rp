﻿using GTANetworkAPI;
using gtalife.src.Database.Models;
using gtalife.src.Player.Utils;

namespace gtalife.src.Player.Customization
{
    public class CustomizationModel : Script
    {
        public void InitializePedFace(NetHandle ent)
        {
            Client player = NAPI.Player.GetPlayerFromHandle(ent);

            if (player == null) return;
            else if (!player.IsLogged()) return;

            Character character = Data.Character[player];

            NAPI.Data.SetEntitySharedData(ent, "GTAO_HAS_CHARACTER_DATA", true);

            NAPI.Data.SetEntitySharedData(ent, "GTAO_SHAPE_FIRST_ID", character.Trait.FaceFirst);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_SHAPE_SECOND_ID", character.Trait.FaceSecond);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_SKIN_FIRST_ID", character.Trait.SkinFirst);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_SKIN_SECOND_ID", character.Trait.SkinSecond);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_SHAPE_MIX", character.Trait.FaceMix);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_SKIN_MIX", character.Trait.SkinMix);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_HAIR", character.Trait.HairType);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_HAIR_COLOR", character.Trait.HairColor);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_HAIR_HIGHLIGHT_COLOR", character.Trait.HairHighlight);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_EYE_COLOR", character.Trait.EyeColor);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_EYEBROWS", character.Trait.Eyebrows);

            if (character.Trait.Beard != null)
                NAPI.Data.SetEntitySharedData(ent, "GTAO_BEARD", character.Trait.Beard); // No beard by default.

            if (character.Trait.Makeup != null)
                NAPI.Data.SetEntitySharedData(ent, "GTAO_MAKEUP", character.Trait.Makeup); // No makeup by default.

            if (character.Trait.Lipstick != null)
                NAPI.Data.SetEntitySharedData(ent, "GTAO_LIPSTICK", character.Trait.Lipstick); // No lipstick by default.

            NAPI.Data.SetEntitySharedData(ent, "GTAO_BEARD_COLOR", character.Trait.BeardColor);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_BEARD_COLOR2", character.Trait.BeardColor);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_EYEBROWS_COLOR", character.Trait.EyebrowsColor1);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_EYEBROWS_COLOR2", character.Trait.EyebrowsColor2);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_LIPSTICK_COLOR", character.Trait.LipstickColor);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_LIPSTICK_COLOR2", character.Trait.LipstickColor);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_MAKEUP_COLOR", character.Trait.MakeupColor);
            NAPI.Data.SetEntitySharedData(ent, "GTAO_MAKEUP_COLOR2", character.Trait.MakeupColor);

            var list = new float[21];

            for (var i = 0; i < 21; i++)
            {
                list[i] = 0f;
            }

            NAPI.Data.SetEntitySharedData(ent, "GTAO_FACE_FEATURES_LIST", list);
        }

        public void RemovePedFace(NetHandle ent)
        {
            NAPI.Data.SetEntitySharedData(ent, "GTAO_HAS_CHARACTER_DATA", false);

            NAPI.Data.ResetEntitySharedData(ent, "GTAO_SHAPE_FIRST_ID");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_SHAPE_SECOND_ID");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_SKIN_FIRST_ID");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_SKIN_SECOND_ID");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_SHAPE_MIX");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_SKIN_MIX");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_HAIR");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_HAIR_COLOR");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_HAIR_HIGHLIGHT_COLOR");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_EYE_COLOR");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_EYEBROWS");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_BEARD");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_MAKEUP");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_LIPSTICK");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_BEARD_COLOR");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_BEARD_COLOR2");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_EYEBROWS_COLOR");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_EYEBROWS_COLOR2");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_MAKEUP_COLOR");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_MAKEUP_COLOR2");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_LIPSTICK_COLOR");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_LIPSTICK_COLOR2");
            NAPI.Data.ResetEntitySharedData(ent, "GTAO_FACE_FEATURES_LIST");
        }

        public bool IsPlayerFaceValid(NetHandle ent)
        {
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_SHAPE_FIRST_ID")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_SHAPE_SECOND_ID")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_SKIN_FIRST_ID")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_SKIN_SECOND_ID")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_SHAPE_MIX")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_SKIN_MIX")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_HAIR")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_HAIR_COLOR")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_HAIR_HIGHLIGHT_COLOR")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_EYE_COLOR")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_EYEBROWS")) return false;
            //if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_BEARD")) return false; // Player may have no beard
            //if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_MAKEUP")) return false; // Player may have no makeup
            //if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_LIPSTICK")) return false; // Player may have no lipstick
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_BEARD_COLOR")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_BEARD_COLOR2")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_EYEBROWS_COLOR")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_EYEBROWS_COLOR2")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_MAKEUP_COLOR")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_MAKEUP_COLOR2")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_LIPSTICK_COLOR")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_LIPSTICK_COLOR2")) return false;
            if (!NAPI.Data.HasEntitySharedData(ent, "GTAO_FACE_FEATURES_LIST")) return false;

            return true;
        }

        public void UpdatePlayerFace(NetHandle player)
        {
            NAPI.ClientEvent.TriggerClientEventForAll("UPDATE_CHARACTER", player);
        }
    }
}
