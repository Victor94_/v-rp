﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using Newtonsoft.Json;

namespace gtalife.src.Player.Award
{
    public class AwardEventArgs : EventArgs
    {
        public Client Player;

        public string Id;

        public string Name;
    }

    public class Main : Script
    {
        static string AWARDS_SAVE_DIR = "data/PlayerAwardData";
        static Dictionary<string, Award> AwardDict = new Dictionary<string, Award>();
        static Dictionary<NetHandle, List<PlayerAward>> PlayerAwardDict = new Dictionary<NetHandle, List<PlayerAward>>();
        public static event EventHandler<AwardEventArgs> PlayerUnlockedAward;

        public static void OnPlayerUnlockAward(Client player, string id, string name)
        {
            if (PlayerUnlockedAward != null)
                PlayerUnlockedAward(null, new AwardEventArgs() { Id = id, Name = name, Player = player });
        }

        [ServerEvent(Event.PlayerConnected)]
        public void OnPlayerConnected(Client player)
        {
            InitPlayer(player);
        }

        #region Exported Methods (Award)
        public bool CreateAward(string ID, string name, string description, string txd_lib, string txd_name, int txd_color, int required_progress)
        {
            if (AwardDict.ContainsKey(ID))
            {
                NAPI.Util.ConsoleOutput("AwardAPI: Can't create {0}, ID in use.", ID);
                return false;
            }

            AwardDict.Add(ID, new Award(name, description, txd_lib, txd_name, txd_color, required_progress));
            return true;
        }

        public bool IsIDInUse(string ID)
        {
            return (AwardDict.ContainsKey(ID));
        }

        public string[] GetAllAwardIDs()
        {
            return AwardDict.Keys.ToArray();
        }

        public string GetAwardName(string ID)
        {
            return ((AwardDict.ContainsKey(ID)) ? AwardDict[ID].Name : string.Empty);
        }

        public string GetAwardDescription(string ID)
        {
            return ((AwardDict.ContainsKey(ID)) ? AwardDict[ID].Description : string.Empty);
        }

        public int GetAwardRequiredProgress(string ID)
        {
            return ((AwardDict.ContainsKey(ID)) ? AwardDict[ID].RequiredProgress : 0);
        }
        #endregion

        #region Exported Methods (Player)
        public void InitPlayer(Client player)
        {
            string player_file = AWARDS_SAVE_DIR + Path.DirectorySeparatorChar + player.SocialClubName + ".json";

            if (File.Exists(player_file))
            {
                List<PlayerAward> data = JsonConvert.DeserializeObject<List<PlayerAward>>(File.ReadAllText(player_file));

                if (PlayerAwardDict.ContainsKey(player.Handle))
                {
                    PlayerAwardDict[player.Handle].Clear();
                    PlayerAwardDict[player.Handle] = data;
                }
                else
                {
                    PlayerAwardDict.Add(player.Handle, data);
                }
            }
            else
            {
                PlayerAwardDict.Add(player.Handle, new List<PlayerAward>());
            }
        }

        public static bool GiveAwardProgress(Client player, string ID, int progress)
        {
            if (!AwardDict.ContainsKey(ID) || !PlayerAwardDict.ContainsKey(player.Handle)) return false;

            PlayerAward award = PlayerAwardDict[player.Handle].FirstOrDefault(a => a.ID == ID);
            if (award == null)
            {
                PlayerAward new_award = null;
                bool unlocked = false;

                if (progress >= AwardDict[ID].RequiredProgress)
                {
                    new_award = new PlayerAward(ID, progress, true, DateTime.Now);
                    unlocked = true;
                }
                else
                {
                    new_award = new PlayerAward(ID, progress, false, null);
                }

                PlayerAwardDict[player.Handle].Add(new_award);

                if (unlocked)
                {
                    UnlockAward(player, ID);
                }
                else
                {
                    SavePlayer(player);
                }
            }
            else
            {
                award.Progress += progress;

                if (award.Progress >= AwardDict[ID].RequiredProgress)
                {
                    UnlockAward(player, ID);
                }
                else
                {
                    SavePlayer(player);
                }
            }

            return true;
        }

        public static bool SetAwardProgress(Client player, string ID, int progress)
        {
            if (!AwardDict.ContainsKey(ID) || !PlayerAwardDict.ContainsKey(player.Handle)) return false;

            PlayerAward award = PlayerAwardDict[player.Handle].FirstOrDefault(a => a.ID == ID);
            if (award == null)
            {
                PlayerAward new_award = null;
                bool unlocked = false;

                if (progress >= AwardDict[ID].RequiredProgress)
                {
                    new_award = new PlayerAward(ID, progress, true, DateTime.Now);
                    unlocked = true;
                }
                else
                {
                    new_award = new PlayerAward(ID, progress, false, null);
                }

                PlayerAwardDict[player.Handle].Add(new_award);

                if (unlocked)
                {
                    UnlockAward(player, ID);
                }
                else
                {
                    SavePlayer(player);
                }
            }
            else
            {
                award.Progress = progress;

                if (award.Progress >= AwardDict[ID].RequiredProgress)
                {
                    UnlockAward(player, ID);
                }
                else
                {
                    SavePlayer(player);
                }
            }

            return true;
        }

        public int GetAwardProgress(Client player, string ID)
        {
            if (!AwardDict.ContainsKey(ID) || !PlayerAwardDict.ContainsKey(player.Handle)) return 0;

            PlayerAward award = PlayerAwardDict[player.Handle].FirstOrDefault(a => a.ID == ID);
            if (award == null) return 0;

            return award.Progress;
        }

        public bool RemoveAward(Client player, string ID)
        {
            if (!AwardDict.ContainsKey(ID) || !PlayerAwardDict.ContainsKey(player.Handle)) return false;

            PlayerAward award = PlayerAwardDict[player.Handle].FirstOrDefault(a => a.ID == ID);
            if (award == null) return false;

            PlayerAwardDict[player.Handle].Remove(award);
            SavePlayer(player);
            return true;
        }

        public bool RemoveAllAwards(Client player)
        {
            if (!PlayerAwardDict.ContainsKey(player.Handle)) return false;

            PlayerAwardDict[player.Handle].Clear();
            SavePlayer(player);
            return true;
        }

        public static bool IsAwardUnlocked(Client player, string ID)
        {
            if (!AwardDict.ContainsKey(ID) || !PlayerAwardDict.ContainsKey(player.Handle)) return false;

            PlayerAward award = PlayerAwardDict[player.Handle].FirstOrDefault(a => a.ID == ID);
            if (award == null) return false;

            return award.Unlocked;
        }

        public DateTime? GetAwardUnlockDate(Client player, string ID)
        {
            if (!AwardDict.ContainsKey(ID) || !PlayerAwardDict.ContainsKey(player.Handle)) return null;

            PlayerAward award = PlayerAwardDict[player.Handle].FirstOrDefault(a => a.ID == ID);
            if (award == null) return null;

            return award.UnlockDate;
        }

        public bool LockAward(Client player, string ID)
        {
            if (!AwardDict.ContainsKey(ID) || !PlayerAwardDict.ContainsKey(player.Handle)) return false;

            PlayerAward award = PlayerAwardDict[player.Handle].FirstOrDefault(a => a.ID == ID);
            if (award == null) return false;

            award.Unlocked = false;
            award.UnlockDate = null;
            SavePlayer(player);
            return true;
        }

        public static bool UnlockAward(Client player, string ID)
        {
            if (!AwardDict.ContainsKey(ID) || !PlayerAwardDict.ContainsKey(player.Handle)) return false;

            PlayerAward award = PlayerAwardDict[player.Handle].FirstOrDefault(a => a.ID == ID);
            bool new_unlock = false;

            if (award == null)
            {
                PlayerAwardDict[player.Handle].Add(new PlayerAward(ID, AwardDict[ID].RequiredProgress, true, DateTime.Now));
                new_unlock = true;
            }
            else
            {
                if (!award.Unlocked)
                {
                    new_unlock = true;
                    award.Unlocked = true;
                    award.UnlockDate = DateTime.Now;
                }
            }

            if (new_unlock)
            {
                player.TriggerEvent("Award_Unlocked", NAPI.Util.ToJson(AwardDict[ID]));
                OnPlayerUnlockAward(player, ID, AwardDict[ID].Name);
            }

            SavePlayer(player);

            return true;
        }
        #endregion

        #region Methods
        public static void SavePlayer(Client player)
        {
            if (!PlayerAwardDict.ContainsKey(player.Handle)) return;

            string player_file = AWARDS_SAVE_DIR + Path.DirectorySeparatorChar + player.SocialClubName + ".json";
            File.WriteAllText(player_file, JsonConvert.SerializeObject(PlayerAwardDict[player.Handle], Formatting.Indented));
        }
        #endregion

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            AWARDS_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + AWARDS_SAVE_DIR;
            if (!Directory.Exists(AWARDS_SAVE_DIR)) Directory.CreateDirectory(AWARDS_SAVE_DIR);

            // Definitions
            CreateAward("new_car", "My new car!", "Buy a car for the first time!", "", "", 0, 1);
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (PlayerAwardDict.ContainsKey(player.Handle)) PlayerAwardDict.Remove(player.Handle);
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            PlayerAwardDict.Clear();
        }
        #endregion
    }
}
