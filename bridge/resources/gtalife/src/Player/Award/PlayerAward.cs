﻿using System;

namespace gtalife.src.Player.Award
{
    public class PlayerAward
    {
        public string ID { get; set; }
        public int Progress { get; set; }
        public bool Unlocked { get; set; }
        public DateTime? UnlockDate { get; set; }

        public PlayerAward(string ID, int progress, bool unlocked, DateTime? unlockdate)
        {
            this.ID = ID;
            Progress = progress;
            Unlocked = unlocked;
            UnlockDate = unlockdate;
        }
    }
}
