"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
API.onServerEventTrigger.connect((eventName, args) => {
    if (eventName == "DisplaySubtitle") {
        API.displaySubtitle(args[0], (args[1] > 0) ? args[1] : 3000);
    }
});
