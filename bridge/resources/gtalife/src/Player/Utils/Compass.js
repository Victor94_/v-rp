"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var text;
var zone;
var street;
var mapMarginLeft = API.getScreenResolutionMaintainRatio().Width / 64;
var mapMarginBottom = API.getScreenResolutionMaintainRatio().Height / 60;
var mapWidth = API.getScreenResolutionMaintainRatio().Width / 7.11;
var mapHeight = API.getScreenResolutionMaintainRatio().Height / 5.71;
var resX = mapMarginLeft + mapWidth + mapMarginLeft;
var resY = API.getScreenResolutionMaintainRatio().Height - mapHeight - mapMarginBottom;
var updateTimeoutInMilliseconds = 500;
var lastUpdateTickCount = 0;
function updateDirectionText() {
    var cameraDirection = API.getGameplayCamDir();
    if (0.3 < cameraDirection.X && 0.3 < cameraDirection.Y) {
        text = "NE";
    }
    else if (cameraDirection.X < -0.3 && 0.3 < cameraDirection.Y) {
        text = "NW";
    }
    else if (0.3 < cameraDirection.X && cameraDirection.Y < -0.3) {
        text = "SE";
    }
    else if (cameraDirection.X < -0.3 && cameraDirection.Y < -0.3) {
        text = "SW";
    }
    else if (-0.3 < cameraDirection.X && cameraDirection.X < 0.3 && cameraDirection.Y < -0.3) {
        text = "S";
    }
    else if (cameraDirection.X < -0.3 && -0.3 < cameraDirection.Y && cameraDirection.Y < 0.3) {
        text = "W";
    }
    else if (0.3 < cameraDirection.X && -0.3 < cameraDirection.Y && cameraDirection.Y < 0.3) {
        text = "E";
    }
    else if (-0.3 < cameraDirection.X && cameraDirection.X < 0.3 && cameraDirection.Y > 0.3) {
        text = "N";
    }
}
function updateValues() {
    var playerPosition = API.getEntityPosition(API.getLocalPlayer());
    zone = API.getZoneNameLabel(playerPosition);
    street = API.getStreetName(playerPosition);
    updateDirectionText();
}
API.onUpdate.connect(function () {
    if (!API.getHudVisible())
        return;
    var currentTimeInMilliseconds = new Date().getTime();
    if (currentTimeInMilliseconds - lastUpdateTickCount > updateTimeoutInMilliseconds) {
        lastUpdateTickCount = currentTimeInMilliseconds;
        updateValues();
    }
    // compass
    API.drawText(text, resX + 5, resY + 127, 1.05, 255, 255, 255, 200, 2, 1, false, true, 0);
    API.drawText("|", resX + 45, resY + 130, 1, 255, 255, 255, 200, 4, 1, false, true, 0);
    API.drawText(zone, resX + 125, resY + 133, 0.5, 255, 255, 255, 200, 4, 1, false, true, 0);
    API.drawText(street, resX + 125, resY + 160, 0.4, 255, 255, 255, 200, 4, 1, false, true, 0);
    // vehicle info
    var player = API.getLocalPlayer();
    var inVeh = API.isPlayerInAnyVehicle(player);
    if (!inVeh)
        return;
    var vehicle = API.getPlayerVehicle(player);
    var velocity = API.getEntityVelocity(vehicle);
    var speed = Math.sqrt(velocity.X * velocity.X + velocity.Y * velocity.Y + velocity.Z * velocity.Z) * 3.6;
    var fuel = API.getVehicleFuelLevel(vehicle);
    var rpm = API.getVehicleRPM(vehicle) * 10000;
    API.drawText(Math.round(speed).toString(), resX + 5, resY + 47, 1.05, 255, 255, 255, 200, 2, 1, false, true, 0);
    API.drawText("KMh", resX + 5, resY + 107, 0.3, 255, 255, 255, 200, 4, 1, false, true, 0);
    if (isVehicleABicycle(API.getEntityModel(vehicle)))
        return;
    API.drawText("|", resX + 45, resY + 50, 1, 255, 255, 255, 200, 4, 1, false, true, 0);
    API.drawText(`Gas: ${fuel}%`, resX + 125, resY + 53, 0.5, 255, 255, 255, 200, 4, 1, false, true, 0);
    API.drawText(`RPM: ${Math.round(rpm)}`, resX + 125, resY + 80, 0.4, 255, 255, 255, 200, 4, 1, false, true, 0);
});
function isVehicleABicycle(model) {
    if (model == 1131912276 || model == 448402357 || model == -836512833 || model == -186537451 || model == 1127861609 || model == -1233807380 || model == -400295096)
        return true;
    return false;
}
