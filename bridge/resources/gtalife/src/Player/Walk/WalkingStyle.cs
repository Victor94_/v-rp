﻿using System;
using System.Linq;
using System.Collections.Generic;
using GTANetworkAPI;

namespace gtalife.src.Player.Walk
{
    public class WalkingStyle
    {
        public string Name;
        public string AnimName;

        public WalkingStyle(string name, string anim_name)
        {
            Name = name;
            AnimName = anim_name;
        }
    }

    public class Main : Script
    {
        // names & style names taken from https://github.com/Guad/EnhancedInteractionMenu/blob/master/EnhancedInteractionMenu/pi_menu.cs
        List<WalkingStyle> WalkingStyles = new List<WalkingStyle>
        {
            new WalkingStyle("Normal", ""),
            new WalkingStyle("Valiente", "move_m@brave"),
            new WalkingStyle("Confidente", "move_m@confident"),
            new WalkingStyle("Borracho", "move_m@drunk@verydrunk"),
            new WalkingStyle("Gordo", "move_m@fat@a"),
            new WalkingStyle("Gangster", "move_m@shadyped@a"),
            new WalkingStyle("Prisa", "move_m@hurry@a"),
            new WalkingStyle("Lesionado", "move_m@injured"),
            new WalkingStyle("Intimidado", "move_m@intimidation@1h"),
            new WalkingStyle("Rápido", "move_m@quick"),
            new WalkingStyle("Triste", "move_m@sad@a"),
            new WalkingStyle("Malo", "move_m@tool_belt@a")
        };

        [ServerEvent(Event.EntityModelChange)]
        public void OnEntityModelChange(NetHandle entityHandle, uint oldModel)
        {
            Client player;

            if ((player = NAPI.Player.GetPlayerFromHandle(entityHandle)) != null)
            {
                if (player.HasSharedData("WalkingStyle")) NAPI.ClientEvent.TriggerClientEventForAll("SetPlayerWalkingStyle", player.Handle, player.GetSharedData("WalkingStyle"));
            }
        }

        [RemoteEvent("RequestWalkingStyles")]
        public void RequestWalkingStyles(Client player, object[] arguments)
        {
            player.TriggerEvent("ReceiveWalkingStyles", NAPI.Util.ToJson(WalkingStyles.Select(w => w.Name)));
        }

        [RemoteEvent("SetWalkingStyle")]
        public void SetWalkingStyle(Client player, object[] arguments)
        {
            if (arguments.Length < 1) return;

            int id = Convert.ToInt32(arguments[0]);
            if (id < 0 || id >= WalkingStyles.Count) return;

            if (id == 0) // reset
            {
                NAPI.ClientEvent.TriggerClientEventForAll("ResetPlayerWalkingStyle", player.Handle);
                player.ResetSharedData("WalkingStyle");
            }
            else
            {
                NAPI.ClientEvent.TriggerClientEventForAll("SetPlayerWalkingStyle", player.Handle, WalkingStyles[id].AnimName);
                player.SetSharedData("WalkingStyle", WalkingStyles[id].AnimName);
            }

            player.TriggerEvent("SetCurrentStyleIndex", id);
            player.SendChatMessage("~y~INFO: ~w~Estilo de caminar cambiado a: ~y~" + WalkingStyles[id].Name + ".");
        }
    }
}
