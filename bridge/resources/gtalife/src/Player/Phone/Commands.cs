﻿using GTANetworkAPI;
using gtalife.src.Database.Models;
using gtalife.src.Player.Utils;
using System.Linq;

namespace gtalife.src.Player.Phone
{
    class Commands : Script
    {
        [Command("ayudatelefono")]
        public void CommandsCommand(Client player)
        {
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Telefono ~~~~~~~~~~~");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /llamar - /añadiragenda - /borraragenda");
            NAPI.Chat.SendChatMessageToPlayer(player, "* Pulsa ~y~flecha arriba ~w~para abrir el teléfono y ~y~backspace ~w~para cerrar.");
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Telefono ~~~~~~~~~~~");
        }

        [Command("llamar", "~y~USO: ~w~/llamar [telefono]", Alias = "call")]
        public void CMD_Call(Client sender, int phone)
        {
            if (sender.getPhoneNumber() == null) sender.SendChatMessage("~r~ERROR: ~w~Usted no tiene un teléfono.");
            else Phone.Call(sender, phone);
        }

        [Command("añadiragenda", "~y~USO: ~w~/añadiragenda [telefono] [nombre]", Alias = "addcontact", GreedyArg = true)]
        public void CMD_AddContact(Client player, int phone, string name)
        {
            if (player.getPhoneNumber() == null) player.SendChatMessage("~r~ERROR: ~w~Usted no tiene un teléfono.");
            else
            {
                Data.Character[player].Contacts.Add(new CharacterContact { Character = Data.Character[player], Phone = phone, Name = name });
                player.SendChatMessage($"~g~ÉXITO: ~w~Agregaste a un contacto con el nombre ~y~{name} ~w~y al teléfono ~y~{phone}~w~.");
            }
        }

        [Command("borraragenda", "~y~USO: ~w~/borraragenda [nombre]", Alias = "removecontact", GreedyArg = true)]
        public void CMD_DeleteContact(Client player, string name)
        {
            if (player.getPhoneNumber() == null) player.SendChatMessage("~r~ERROR: ~w~Usted no tiene un teléfono.");
            else
            {
                var contact = Data.Character[player].Contacts.FirstOrDefault(x => x.Name == name && x.Character == Data.Character[player]);
                if (contact != null)
                {
                    Data.Character[player].Contacts.Remove(contact);
                    player.SendChatMessage($"~g~ÉXITO: ~w~Borraste el contacto ~y~{name}~w~.");
                }
                else player.SendChatMessage($"~r~ERROR: ~w~No se encontró contacto con el nombre ~y~{name}~w~.");
            }
        }
    }
}
