﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var browser = null;

API.onResourceStart.connect(() => {
    var res = API.getScreenResolution();
    browser = API.createCefBrowser(res.Width, res.Height);
    API.waitUntilCefBrowserInit(browser);
    API.setCefBrowserPosition(browser, 0, 0);
    API.loadPageCefBrowser(browser, "res/views/phone.html");
    API.setCefBrowserHeadless(browser, true);
});

API.onResourceStop.connect(() => {
    if (browser != null) {
        API.destroyCefBrowser(browser);
        browser = null;
    }
});

API.onKeyUp.connect(function (sender, e) {    
    if (API.isChatOpen() || API.isAnyMenuOpen() || !API.getHudVisible()) return;

    if (e.KeyCode === Keys.Up) {
        if (API.getCefBrowserHeadless(browser)) {
            API.triggerServerEvent("RequestPhone");            
        }
    }
    else if (e.KeyCode == Keys.Back) {
        if (!API.getCefBrowserHeadless(browser)) {
            API.showCursor(false);
            API.setCefBrowserHeadless(browser, true);
            API.playSoundFrontEnd("Put_Away", "Phone_SoundSet_Michael");
        }
    }
});

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) => {
    switch (eventName) {
        case "ShowPhone":
            API.showCursor(true);
            API.setCefBrowserHeadless(browser, false);
            API.playSoundFrontEnd("Put_Away", "Phone_SoundSet_Michael");
            API.triggerServerEvent("GetPhoneList");
            break;
        case "PopulatePhoneList":            
            browser.call("updateList", args[0]);
            break;
        case "IncomingCall":
            API.showCursor(true);
            API.setCefBrowserHeadless(browser, false);

            browser.call("incomingCall");
            break;
    }
});

function hangup() {
    API.triggerServerEvent("HangupPhone");
    API.playSoundFrontEnd("Hang_Up", "Phone_SoundSet_Michael");

    API.triggerServerEvent("GetPhoneList");
}

function answerCall() {
    API.triggerServerEvent("AnswerCall");
    API.playSoundFrontEnd("SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
}

function closePhone() {
    API.showCursor(false);
    API.setCefBrowserHeadless(browser, true);
    API.playSoundFrontEnd("Put_Away", "Phone_SoundSet_Michael");
}

function call(playerNumber: number) {
    API.triggerServerEvent("CallPlayer", playerNumber);
    API.playSoundFrontEnd("SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
}
