﻿using GTANetworkAPI;
using gtalife.src.Gameplay.Parkinglot;
using gtalife.src.Player.Utils;
using System;
using System.Linq;

namespace gtalife.src.Player.Vehicle
{
    class Vehicle : Script
    {
        const int DepositPrice = 100;

        [RemoteEvent("on_player_request_vehicle_list")]
        public void OnPlayerRequestVehicleList(Client player, object[] arguments)
        {
            if (!player.IsLogged()) return;

            var vehicles = (from v in Data.Character[player].Vehicles select new { v.Id, v.Model, v.PositionX, v.PositionY, v.PositionZ }).ToList();
            NAPI.ClientEvent.TriggerClientEvent(player, "retrieve_vehicle_list", NAPI.Util.ToJson(vehicles));
        }

        [RemoteEvent("on_player_select_owned_vehicle")]
        public void OnPlayerSelectOwnedVehicle(Client player, object[] arguments)
        {
            if (player.HasData("ParkinglotMarker_ID") && player.getMoney() < DepositPrice)
            {
                NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~s~No tienes dinero suficiente.");
                return;
            }

            int vehicle_id = (int)arguments[0];
            var vehicle = Data.Character[player].Vehicles.FirstOrDefault(x => x.Id == vehicle_id);
            if (vehicle == null) return;

            if (player.HasData("ParkinglotMarker_ID"))
            {
                Parkinglot parkinglot = Gameplay.Parkinglot.Main.Parkinglots.FirstOrDefault(h => h.ID == player.GetData("ParkinglotMarker_ID"));
                if (parkinglot == null) return;
                else if (parkinglot.Spawns.Count < 1)
                {
                    player.SendNotification("Este aparcamiento está cerrado.");
                    return;
                }

                var rand = new Random();
                var parkingSpace = parkinglot.Spawns[rand.Next(parkinglot.Spawns.Count)];

                NAPI.Entity.SetEntityPosition(vehicle.Handle, parkingSpace.Position);
                NAPI.Entity.SetEntityRotation(vehicle.Handle, parkingSpace.Rotation);

                player.giveMoney(-DepositPrice);
            }
            else if (player.Position.DistanceTo(NAPI.Entity.GetEntityPosition(vehicle.Handle)) < 40f)
            {
                bool isLocked = NAPI.Vehicle.GetVehicleLocked(vehicle.Handle);
                if (isLocked) player.sendChatAction((player.IsInVehicle) ? ((vehicle.Handle == player.Vehicle) ? "abrió la puerta de su vehículo." : "abrió la puerta de su vehículo con el control remoto.") : "abrió la puerta de su vehículo con el control remoto.");
                else player.sendChatAction((player.IsInVehicle) ? ((vehicle.Handle == player.Vehicle) ? "cerró la puerta de su vehículo." : "cerró la puerta de su vehículo con el control remoto.") : "cerró la puerta de su vehículo con el control remoto.");
                NAPI.Vehicle.SetVehicleLocked(vehicle.Handle, !isLocked);
            }
            else
            {
                Vector3 position = NAPI.Entity.GetEntityPosition(vehicle.Handle);
                NAPI.ClientEvent.TriggerClientEvent(player, "show_owned_vehicle_blip", position.X, position.Y, position.Z);
            }
        }

        [ServerEvent(Event.PlayerEnterVehicle)]
        public void OnPlayerEnterVehicle(Client player, GTANetworkAPI.Vehicle vehicle, sbyte seatID)
        {
            if (!Data.Character.ContainsKey(player)) return;

            foreach (var personalVehicle in Data.Character[player].Vehicles)
            {
                if (!vehicle.Equals(personalVehicle)) continue;

                NAPI.ClientEvent.TriggerClientEvent(player, "on_player_enter_owned_vehicle");
            }
        }
    }
}
