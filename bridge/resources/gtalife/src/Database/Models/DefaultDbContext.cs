﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.IO;

namespace gtalife.src.Database.Models
{
    class Config
    {
        public string Address { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
        public int Port { get; set; }
    }

    public class DefaultDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<CharacterTrait> CharacterTrait { get; set; }
        public DbSet<CharacterWeapon> CharacterWeapons { get; set; }
        public DbSet<CharacterClothes> CharacterClothes { get; set; }
        public DbSet<CharacterContact> CharacterContacts { get; set; }
        public DbSet<CharacterVehicle> CharacterVehicles { get; set; }
        public DbSet<Weather> Weather { get; set; }

        public DefaultDbContext()
        {

        }

        public DefaultDbContext(DbContextOptions<DefaultDbContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                Config config = new Config { Address = "127.0.0.1", Username = "root", Password = null, Database = "gtalife", Port = 3306 };

                string file = Environment.CurrentDirectory + Path.DirectorySeparatorChar +
                    "bridge" + Path.DirectorySeparatorChar + "resources" +
                    Path.DirectorySeparatorChar + "gtalife" + Path.DirectorySeparatorChar + "database.json";

                if (File.Exists(file)) config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(file));

                optionsBuilder.UseMySql($"Host={config.Address};Port={config.Port};Database={config.Database};Username={config.Username};Password={config.Password}");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Username).IsRequired();
                entity.HasMany(d => d.Characters)
                  .WithOne(p => p.User);
            });

            modelBuilder.Entity<Character>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.HasOne(d => d.User)
                  .WithMany(p => p.Characters);
                entity.HasMany(c => c.Clothes)
                    .WithOne(e => e.Character);
                entity.HasMany(c => c.Contacts)
                    .WithOne(e => e.Character);
                entity.HasOne(c => c.Trait)
                    .WithOne(e => e.Character)
                    .HasForeignKey<CharacterTrait>(b => b.CharacterId);
                entity.HasMany(c => c.Vehicles)
                    .WithOne(e => e.Character);
                entity.HasMany(c => c.Weapons)
                    .WithOne(e => e.Character);
            });

            modelBuilder.Entity<CharacterTrait>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasOne(d => d.Character)
                  .WithOne(p => p.Trait);
            });

            modelBuilder.Entity<CharacterWeapon>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasOne(d => d.Character)
                  .WithMany(p => p.Weapons);
            });

            modelBuilder.Entity<CharacterClothes>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasOne(d => d.Character)
                  .WithMany(p => p.Clothes);
            });

            modelBuilder.Entity<CharacterContact>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasOne(d => d.Character)
                  .WithMany(p => p.Contacts);
            });

            modelBuilder.Entity<CharacterVehicle>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasOne(d => d.Character)
                  .WithMany(p => p.Vehicles);
            });

            modelBuilder.Entity<Weather>(entity =>
            {
                entity.HasKey(e => e.Id);
            });
        }
    }
}
