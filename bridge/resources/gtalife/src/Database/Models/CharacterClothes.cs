﻿namespace gtalife.src.Database.Models
{
    public class CharacterClothes
    {
        public int Id { get; set; }

        public virtual Character Character { get; set; }

        public int Slot { get; set; }

        public int Drawable { get; set; }

        public int Texture { get; set; }

        public bool IsProp { get; set; }
    }
}
