﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.EventArgs;
using DSharpPlus.CommandsNext;
using GTANetworkAPI;
using Newtonsoft.Json;
using DSharpPlus.CommandsNext.Exceptions;
using DSharpPlus.Entities;

namespace gtalife.src.Discord
{
    class Main : Script
    {
        public CommandsNextModule Commands { get; set; }

        public static DiscordClient Client { get; set; }
        public static DiscordMessage PlayerListMessage = null;

        public static ulong GuildId { get; set; }
        public static ulong PlayerListChannelId { get; set; }
        public static ulong AdvertisementChannelId { get; set; }

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            var prog = new Main();
            prog.RunBotAsync().GetAwaiter().GetResult();
        }

        public async Task RunBotAsync()
        {
            if (!File.Exists("bridge/resources/gtalife/discordbot.json"))
            {
                NAPI.Util.ConsoleOutput("[WARNING] discordbot.json not found!");
                return;
            }

            var json = "";
            using (var fs = File.OpenRead("bridge/resources/gtalife/discordbot.json"))
            using (var sr = new StreamReader(fs, new UTF8Encoding(false)))
                json = await sr.ReadToEndAsync();

            var cfgjson = JsonConvert.DeserializeObject<ConfigJson>(json);
            var cfg = new DiscordConfiguration
            {
                Token = cfgjson.Token,
                TokenType = TokenType.Bot,

                AutoReconnect = true,
                LogLevel = LogLevel.Debug,
                UseInternalLogHandler = true
            };

            GuildId = cfgjson.GuildId;
            PlayerListChannelId = cfgjson.PlayerListChannelId;
            AdvertisementChannelId = cfgjson.AdvertisementChannelId;

            Client = new DiscordClient(cfg);

            Client.Ready += this.Client_Ready;
            Client.GuildAvailable += this.Client_GuildAvailable;
            Client.ClientErrored += this.Client_ClientError;

            var ccfg = new CommandsNextConfiguration
            {
                StringPrefix = cfgjson.CommandPrefix,

                EnableDms = true,

                EnableMentionPrefix = true
            };

            this.Commands = Client.UseCommandsNext(ccfg);

            this.Commands.CommandExecuted += this.Commands_CommandExecuted;
            this.Commands.CommandErrored += this.Commands_CommandErrored;

            this.Commands.RegisterCommands<Commands>();

            await Client.ConnectAsync();

            await Task.Delay(-1);
        }

        private Task Client_Ready(ReadyEventArgs e)
        {
            e.Client.DebugLogger.LogMessage(LogLevel.Info, "DiscordBot", "Client is ready to process events.", DateTime.Now);

            return Task.FromResult(0);
        }

        private async Task Client_GuildAvailable(GuildCreateEventArgs e)
        {
            e.Client.DebugLogger.LogMessage(LogLevel.Info, "DiscordBot", $"Guild available: {e.Guild.Name}", DateTime.Now);

            if (e.Guild.Id == GuildId)
            {
                var channel = e.Guild.GetChannel(PlayerListChannelId);
                var emoji = DiscordEmoji.FromName(Client, ":on:");

                var embed = new DiscordEmbedBuilder
                {
                    Title = "V Roleplay | Bot del servidor",
                    Description = $"El servidor se encuentra en linea",
                    Color = new DiscordColor(0x72C245)
                };

                ClearMessages();
                await channel.SendMessageAsync("", embed: embed);

                embed = new DiscordEmbedBuilder
                {
                    Title = "Jugadores en linea",
                    Description = $"**Cargando...**",
                    Color = new DiscordColor(0x888888)
                };

                PlayerListMessage = await channel.SendMessageAsync(embed: embed);
            }
        }

        private Task Client_ClientError(ClientErrorEventArgs e)
        {
            e.Client.DebugLogger.LogMessage(LogLevel.Error, "DiscordBot", $"Exception occured: {e.Exception.GetType()}: {e.Exception.Message}", DateTime.Now);

            return Task.FromResult(0);
        }

        private Task Commands_CommandExecuted(CommandExecutionEventArgs e)
        {
            e.Context.Client.DebugLogger.LogMessage(LogLevel.Info, "DiscordBot", $"{e.Context.User.Username} successfully executed '{e.Command.QualifiedName}'", DateTime.Now);

            return Task.FromResult(0);
        }

        private async Task Commands_CommandErrored(CommandErrorEventArgs e)
        {
            e.Context.Client.DebugLogger.LogMessage(LogLevel.Error, "DiscordBot", $"{e.Context.User.Username} tried executing '{e.Command?.QualifiedName ?? "<unknown command>"}' but it errored: {e.Exception.GetType()}: {e.Exception.Message ?? "<no message>"}", DateTime.Now);

            if (e.Exception is ChecksFailedException ex)
            {
                var emoji = DiscordEmoji.FromName(e.Context.Client, ":no_entry:");

                var embed = new DiscordEmbedBuilder
                {
                    Title = "Access denied",
                    Description = $"{emoji} You do not have the permissions required to execute this command.",
                    Color = new DiscordColor(0xFF0000)
                };
                await e.Context.RespondAsync("", embed: embed);
            }
        }

        public static async void SendDiscordBotMessage(ulong channel, string message, DiscordEmbed embed = null)
        {
            await Client.Guilds[GuildId].GetChannel(channel).SendMessageAsync(message, embed: embed);
        }

        public async void ClearMessages()
        {
            var guild = Client.Guilds[GuildId];
            var channel = guild.GetChannel(PlayerListChannelId);
            var messages = await channel.GetMessagesAsync();

            if (messages.Count(x => x.Author == Client.CurrentUser) > 0)
                await channel.DeleteMessagesAsync(messages.Where(x => x.Author == Client.CurrentUser));
        }
    }

    public struct ConfigJson
    {
        [JsonProperty("guild")]
        public ulong GuildId { get; private set; }

        [JsonProperty("playerlistchannel")]
        public ulong PlayerListChannelId { get; private set; }

        [JsonProperty("advertisementchannel")]
        public ulong AdvertisementChannelId { get; private set; }

        [JsonProperty("token")]
        public string Token { get; private set; }

        [JsonProperty("prefix")]
        public string CommandPrefix { get; private set; }
    }
}
